#########################
### TERRAFORMING COST ###
#########################

@easy_cost = 2000			#on-class terraforming
@medium_cost = 4000			#adjacent terraforming
@hard_cost = 8000			#tomb world, barren/hothouse, exotic, and 2x adjacent
@difficult_cost = 12000		#gaia world, machine world, toxic, frozen, molten
@very_difficult_cost = 16000#uninhabitable to machine

#########################
### TERRAFORMING TIME ###
#########################
							#Base Colony Dev Speed = 1800 (5yr)
@easy_time = 1800 			#(5yr)
@medium_time = 2700 		#(7.5yr)
@hard_time = 5400 			#(10yr)
@difficult_time = 7200 		#(15yr)
@very_difficult_time = 9900 #(17.5yr)


#RW -> RW Machine
terraform_link = {
	from = "pc_ringworld_habitable"
	to = "pc_ringworld_habitable_machine"
	
	energy = @difficult_cost
	duration = @difficult_time
	
	potential = {
		has_ascension_perk = ap_orbital_engineering_mac
		has_ascension_perk = ap_hyper_engineering
	}

	ai_weight = {
		weight = 20
		#modifier = {
		#	factor = 0
		#	NOR = {
		#		is_mechanical_empire = yes
		#		is_cyborg_empire = yes
		#		has_authority = auth_machine_intelligence
		#	}
		#}
	}
}

#RW Machine -> RW
terraform_link = {
	from = "pc_ringworld_habitable_machine"
	to = "pc_ringworld_habitable"
	
	energy = @difficult_cost
	duration = @difficult_time
	
	potential = {
		has_ascension_perk = ap_hyper_engineering
	}

	ai_weight = {
		weight = 10
		modifier = {
			factor = 0
			OR = {
				is_mechanical_empire = yes
				is_cyborg_empire = yes
				has_authority = auth_machine_intelligence
				has_technology = tech_synthetic_workers
			}
		}
		modifier = {
			factor = 1
			has_technology = tech_synthetic_workers
		}
	}
}
