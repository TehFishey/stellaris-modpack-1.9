Defines merge & Overwrites
Scripted Triggers Merge & Overwrites
	Notably: Fallen Empire, Contingency, Prethorian ship spawn counts
Ascension perks merge, rebalance, and AI upgrades (Exoverhaul)
Combined vanilla building overwrites (MSmerge, EST, & Others)

Psionic civic mod
Vanilla civic modifications

Country types merge (NSC and FEE, and FE starting tech changes)