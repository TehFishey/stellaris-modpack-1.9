#ccs_while_count
#ccs_portal_defend_fleet_multiplier
#allowed factor 2.8
#ccs_big_unbidden_fleet_multiplier
#allowed factor 6.8
#ccs_small_unbidden_fleet_multiplier
#allowed factor 13.5
#


#
#game limitations
#while auto break after 100 loops without count despite the fact that limit = {} is still true
#create_fleet stops adding ships at 500 and logs an error message

spawn_portal_defend_fleet = {
	create_ccs_global_event = yes #if not yet done
	if = {
		limit = {is_country_type = extradimensional}
		event_target:ccs_global_event = {
			set_variable = {which = ccs_while_count value = 36}#vanilla base value
			multiply_variable = {which = ccs_while_count value = ccs_portal_defend_fleet_multiplier}#change number of ships
			change_variable = {which = ccs_while_count value = -1}#because one ship spwans before the while loop
			log = "(36*[This.ccs_portal_defend_fleet_multiplier])-1=[This.ccs_while_count]"
		}
		create_fleet = {
			effect = {
				set_owner = PREV
				create_ship = {
					name = random
					design = "NAME_Revenant"
					graphical_culture = "extra_dimensional_01"
				}
				assign_leader = last_created_leader
				event_target:ccs_global_event = {
					while = {
						count = 99 
						# Which factor is possible so that create_fleet doesn't add more than 500 ships to the fleet
						# ((35+1)+56+86)*x < 500
						# => x=2.8
						# 35*2.8 = 99
						# This means that this while loop should never exceed 99
						limit = {event_target:ccs_global_event = {check_variable = {which = ccs_while_count value > 0.999}}}
						PREV = {
							create_ship = {
								name = random
								design = "NAME_Revenant"
								graphical_culture = "extra_dimensional_01"
							}
						}
						log = "LOOP:[This.ccs_while_count]"
						change_variable = {which = ccs_while_count value = -1}
					}
					set_variable = {which = ccs_while_count value = 56}#vanilla base value
					multiply_variable = {which = ccs_while_count value = ccs_portal_defend_fleet_multiplier}#change number of ships
					log = "56*[This.ccs_portal_defend_fleet_multiplier]=[This.ccs_while_count]"
					while = {
						count = 156
						limit = {check_variable = {which = ccs_while_count value > 0.999}}
						PREV = {
							create_ship = {
								name = random
								design = "NAME_Phantom"
								graphical_culture = "extra_dimensional_01"
							}
						}
						log = "LOOP:[This.ccs_while_count]"
						change_variable = {which = ccs_while_count value = -1}
					}
					set_variable = {which = ccs_while_count value = 86}#vanilla base value
					multiply_variable = {which = ccs_while_count value = ccs_portal_defend_fleet_multiplier}#change number of ships
					log = "86*[This.ccs_portal_defend_fleet_multiplier]=[This.ccs_while_count]"
					while = {
						count = 240
						limit = {check_variable = {which = ccs_while_count value > 0.999}}
						PREV = {
							create_ship = {
								name = random
								design = "NAME_Wraith"
								graphical_culture = "extra_dimensional_01"
							}	
						}
						log = "LOOP:[This.ccs_while_count]"
						change_variable = {which = ccs_while_count value = -1}
					}
				}
				set_location = {
					target = event_target:dimensional_portal
					distance = 5
					angle = random
				}
				set_fleet_stance = aggressive
				set_aggro_range = 500
				set_aggro_range_measure_from = self
			}
		}
	}
	if = {
		limit = {is_country_type = extradimensional_2}
		event_target:ccs_global_event = {
			set_variable = {which = ccs_while_count value = 36}#vanilla base value
			multiply_variable = {which = ccs_while_count value = ccs_portal_defend_fleet_multiplier}#change number of ships
			change_variable = {which = ccs_while_count value = -1}#because one ship spwans before the while loop
		}
		create_fleet = {
			effect = {
				set_owner = PREV
				create_ship = {
					name = random
					design = "NAME_Huntress"
					graphical_culture = "extra_dimensional_02"
				}
				assign_leader = last_created_leader
				event_target:ccs_global_event = {
					while = {
						count = 99
						limit = {check_variable = {which = ccs_while_count value > 0.999}}
						PREV = {
							create_ship = {
								name = random
								design = "NAME_Huntress"
								graphical_culture = "extra_dimensional_02"
							}
						}
						change_variable = {which = ccs_while_count value = -1}
					}				
					set_variable = {which = ccs_while_count value = 56}#vanilla base value
					multiply_variable = {which = ccs_while_count value = ccs_portal_defend_fleet_multiplier}#change number of ships				
					while = {
						count = 156
						limit = {check_variable = {which = ccs_while_count value > 0.999}}
						PREV = {
							create_ship = {
								name = random
								design = "NAME_Assassin"
								graphical_culture = "extra_dimensional_02"
							}
						}
						change_variable = {which = ccs_while_count value = -1}
					}				
					set_variable = {which = ccs_while_count value = 86}#vanilla base value
					multiply_variable = {which = ccs_while_count value = ccs_portal_defend_fleet_multiplier}#change number of ships
					while = {
						count = 240
						limit = {check_variable = {which = ccs_while_count value > 0.999}}
						PREV = {
							create_ship = {
								name = random
								design = "NAME_Predator"
								graphical_culture = "extra_dimensional_02"
							}	
						}
						change_variable = {which = ccs_while_count value = -1}
					}
				}
				set_location = {
					target = event_target:dimensional_portal
					distance = 5
					angle = random
				}
				set_fleet_stance = aggressive
				set_aggro_range = 500
				set_aggro_range_measure_from = self
			}
		}
	}
	if = {
		limit = {is_country_type = extradimensional_3}
		event_target:ccs_global_event = {
			set_variable = {which = ccs_while_count value = 36}#vanilla base value
			multiply_variable = {which = ccs_while_count value = ccs_portal_defend_fleet_multiplier}#change number of ships
			change_variable = {which = ccs_while_count value = -1}#because one ship spwans before the while loop
		}
		create_fleet = {
			effect = {
				set_owner = PREV
				create_ship = {
					name = random
					design = "NAME_Eradicator"
					graphical_culture = "extra_dimensional_03"
				}
				assign_leader = last_created_leader
				event_target:ccs_global_event = {
					while = {
						count = 99
						limit = {check_variable = {which = ccs_while_count value > 0.999}}
						PREV = {
							create_ship = {
								name = random
								design = "NAME_Eradicator"
								graphical_culture = "extra_dimensional_03"
							}
						}
						change_variable = {which = ccs_while_count value = -1}
					}
					set_variable = {which = ccs_while_count value = 56}#vanilla base value
					multiply_variable = {which = ccs_while_count value = ccs_portal_defend_fleet_multiplier}#change number of ships
					while = {
						count = 156
						limit = {check_variable = {which = ccs_while_count value > 0.999}}
						PREV = {
							create_ship = {
								name = random
								design = "NAME_Annihilator"
								graphical_culture = "extra_dimensional_03"
							}
						}
						change_variable = {which = ccs_while_count value = -1}
					}
					set_variable = {which = ccs_while_count value = 86}#vanilla base value
					multiply_variable = {which = ccs_while_count value = ccs_portal_defend_fleet_multiplier}#change number of ships
					while = {
						count = 240
						limit = {check_variable = {which = ccs_while_count value > 0.999}}
						PREV = {
							create_ship = {
								name = random
								design = "NAME_Obliterator"
								graphical_culture = "extra_dimensional_03"
							}	
						}
						change_variable = {which = ccs_while_count value = -1}
					}
				}
				set_location = {
					target = event_target:dimensional_portal
					distance = 5
					angle = random
				}
				set_fleet_stance = aggressive
				set_aggro_range = 500
				set_aggro_range_measure_from = self
			}
		}
	}
}

spawn_big_unbidden_fleet = {
	create_ccs_global_event = yes #if not yet done
	if = {
		limit = {is_country_type = extradimensional}
		event_target:ccs_global_event = {
			set_variable = {which = ccs_while_count value = 15}#vanilla base value
			multiply_variable = {which = ccs_while_count value = ccs_big_unbidden_fleet_multiplier}#change number of ships
			change_variable = {which = ccs_while_count value = -1}#because one ship spwans before the while loop
			log = "(15*[This.ccs_big_unbidden_fleet_multiplier])-1=[This.ccs_while_count]"
		}
		create_fleet = {
			effect = {
				set_owner = PREV
				create_ship = {
					name = random
					design = "NAME_Revenant"
					graphical_culture = "extra_dimensional_01"
				}
				assign_leader = last_created_leader
				event_target:ccs_global_event = {
					while = {
						count = 101 
						limit = {check_variable = {which = ccs_while_count value > 0.999}}
						PREV = {
							create_ship = {
								name = random
								design = "NAME_Revenant"
								graphical_culture = "extra_dimensional_01"
							}
						}
						log = "LOOP:[This.ccs_while_count]"
						change_variable = {which = ccs_while_count value = -1}
					}
					set_variable = {which = ccs_while_count value = 20}#vanilla base value
					multiply_variable = {which = ccs_while_count value = ccs_big_unbidden_fleet_multiplier}#change number of ships
					log = "20*[This.ccs_big_unbidden_fleet_multiplier]=[This.ccs_while_count]"
					while = {
						count = 136 
						limit = {check_variable = {which = ccs_while_count value > 0.999}}
						PREV = {
							create_ship = {
								name = random
								design = "NAME_Phantom"
								graphical_culture = "extra_dimensional_01"
							}
						}
						log = "LOOP:[This.ccs_while_count]"
						change_variable = {which = ccs_while_count value = -1}
					}
					set_variable = {which = ccs_while_count value = 38}#vanilla base value
					multiply_variable = {which = ccs_while_count value = ccs_big_unbidden_fleet_multiplier}#change number of ships
					log = "38*[This.ccs_big_unbidden_fleet_multiplier]=[This.ccs_while_count]"
					while = {
						count = 258 
						limit = {check_variable = {which = ccs_while_count value > 0.999}}
						PREV = {
							create_ship = {
								name = random
								design = "NAME_Wraith"
								graphical_culture = "extra_dimensional_01"
							}
						}
						log = "LOOP:[This.ccs_while_count]"
						change_variable = {which = ccs_while_count value = -1}					
					}
				}
				set_location = {
					target = ROOT
					distance = 5
					angle = random
				}
				set_fleet_stance = aggressive
				set_aggro_range = 500
				set_aggro_range_measure_from = self
			}
		}
	}
	if = {
		limit = {is_country_type = extradimensional_2}
		event_target:ccs_global_event = {
			set_variable = {which = ccs_while_count value = 15}#vanilla base value
			multiply_variable = {which = ccs_while_count value = ccs_big_unbidden_fleet_multiplier}#change number of ships
			change_variable = {which = ccs_while_count value = -1}#because one ship spwans before the while loop
		}
		create_fleet = {
			effect = {
				set_owner = PREV
				create_ship = {
					name = random
					design = "NAME_Huntress"
					graphical_culture = "extra_dimensional_02"
				}
				assign_leader = last_created_leader
				event_target:ccs_global_event = {
					while = {
						count = 101 
						limit = {check_variable = {which = ccs_while_count value > 0.999}}
						PREV = {
							create_ship = {
								name = random
								design = "NAME_Huntress"
								graphical_culture = "extra_dimensional_02"
							}
						}
						change_variable = {which = ccs_while_count value = -1}
					}
					set_variable = {which = ccs_while_count value = 20}#vanilla base value
					multiply_variable = {which = ccs_while_count value = ccs_big_unbidden_fleet_multiplier}#change number of ships
					while = {
						count = 136 
						limit = {check_variable = {which = ccs_while_count value > 0.999}}
						PREV = {
							create_ship = {
								name = random
								design = "NAME_Assassin"
								graphical_culture = "extra_dimensional_02"
							}
						}
						change_variable = {which = ccs_while_count value = -1}
					}
					set_variable = {which = ccs_while_count value = 38}#vanilla base value
					multiply_variable = {which = ccs_while_count value = ccs_big_unbidden_fleet_multiplier}#change number of ships
					while = {
						count = 258 
						limit = {check_variable = {which = ccs_while_count value > 0.999}}
						PREV = {
							create_ship = {
								name = random
								design = "NAME_Predator"
								graphical_culture = "extra_dimensional_02"
							}	
						}
						change_variable = {which = ccs_while_count value = -1}
					}
				}
				set_location = {
					target = ROOT
					distance = 5
					angle = random
				}
				set_fleet_stance = aggressive
				set_aggro_range = 500
				set_aggro_range_measure_from = self
			}
		}
	}
	if = {
		limit = {is_country_type = extradimensional_3}
		event_target:ccs_global_event = {
			set_variable = {which = ccs_while_count value = 15}#vanilla base value
			multiply_variable = {which = ccs_while_count value = ccs_big_unbidden_fleet_multiplier}#change number of ships
			change_variable = {which = ccs_while_count value = -1}#because one ship spwans before the while loop
		}
		create_fleet = {
			effect = {
				set_owner = PREV
				create_ship = {
					name = random
					design = "NAME_Eradicator"
					graphical_culture = "extra_dimensional_03"
				}
				assign_leader = last_created_leader
				event_target:ccs_global_event = {
					while = {
						count = 101 
						limit = {check_variable = {which = ccs_while_count value > 0.999}}
						PREV = {
							create_ship = {
								name = random
								design = "NAME_Eradicator"
								graphical_culture = "extra_dimensional_03"
							}
						}
						change_variable = {which = ccs_while_count value = -1}
					}
					set_variable = {which = ccs_while_count value = 20}#vanilla base value
					multiply_variable = {which = ccs_while_count value = ccs_big_unbidden_fleet_multiplier}#change number of ships
					while = {
						count = 136 
						limit = {check_variable = {which = ccs_while_count value > 0.999}}
						PREV = {
							create_ship = {
								name = random
								design = "NAME_Annihilator"
								graphical_culture = "extra_dimensional_03"
							}
						}
						change_variable = {which = ccs_while_count value = -1}
					}
					set_variable = {which = ccs_while_count value = 38}#vanilla base value
					multiply_variable = {which = ccs_while_count value = ccs_big_unbidden_fleet_multiplier}#change number of ships
					while = {
						count = 258 
						limit = {check_variable = {which = ccs_while_count value > 0.999}}
						PREV = {
							create_ship = {
								name = random
								design = "NAME_Obliterator"
								graphical_culture = "extra_dimensional_03"
							}	
						}
						change_variable = {which = ccs_while_count value = -1}
					}
				}
				set_location = {
					target = ROOT
					distance = 5
					angle = random
				}
				set_fleet_stance = aggressive
				set_aggro_range = 500
				set_aggro_range_measure_from = self
			}
		}
	}
}

spawn_small_unbidden_fleet = {
	create_ccs_global_event = yes #if not yet done
	if = {
		limit = {is_country_type = extradimensional}
		event_target:ccs_global_event = {
			set_variable = {which = ccs_while_count value = 8}#vanilla base value
			multiply_variable = {which = ccs_while_count value = ccs_small_unbidden_fleet_multiplier}#change number of ships
			change_variable = {which = ccs_while_count value = -1}#because one ship spwans before the while loop
		}
		create_fleet = {
			effect = {
				set_owner = PREV
				create_ship = {
					name = random
					design = "NAME_Revenant"
					graphical_culture = "extra_dimensional_01"
				}
				assign_leader = last_created_leader
				event_target:ccs_global_event = {
					while = {
						count = 107 
						limit = {check_variable = {which = ccs_while_count value > 0.999}}
						PREV = {
							create_ship = {
								name = random
								design = "NAME_Revenant"
								graphical_culture = "extra_dimensional_01"
							}
						}
						change_variable = {which = ccs_while_count value = -1}
					}
					set_variable = {which = ccs_while_count value = 10}#vanilla base value
					multiply_variable = {which = ccs_while_count value = ccs_small_unbidden_fleet_multiplier}#change number of ships
					while = {
						count = 135 
						limit = {check_variable = {which = ccs_while_count value > 0.999}}
						PREV = {
							create_ship = {
								name = random
								design = "NAME_Phantom"
								graphical_culture = "extra_dimensional_01"
							}
						}
						change_variable = {which = ccs_while_count value = -1}
					}
					set_variable = {which = ccs_while_count value = 19}#vanilla base value
					multiply_variable = {which = ccs_while_count value = ccs_small_unbidden_fleet_multiplier}#change number of ships
					while = {
						count = 256 
						limit = {check_variable = {which = ccs_while_count value > 0.999}}
						PREV = {
							create_ship = {
								name = random
								design = "NAME_Wraith"
								graphical_culture = "extra_dimensional_01"
							}
						}
						change_variable = {which = ccs_while_count value = -1}					
					}
				}
				set_location = {
					target = event_target:dimensional_portal
					distance = 5
					angle = random
				}
				set_fleet_stance = aggressive
				set_aggro_range = 500
				set_aggro_range_measure_from = self
			}
		}
	}
	if = {
		limit = {is_country_type = extradimensional_2}
		event_target:ccs_global_event = {
			set_variable = {which = ccs_while_count value = 8}#vanilla base value
			multiply_variable = {which = ccs_while_count value = ccs_small_unbidden_fleet_multiplier}#change number of ships
			change_variable = {which = ccs_while_count value = -1}#because one ship spwans before the while loop
		}
		create_fleet = {
			effect = {
				set_owner = PREV
				create_ship = {
					name = random
					design = "NAME_Huntress"
					graphical_culture = "extra_dimensional_02"
				}
				assign_leader = last_created_leader
				event_target:ccs_global_event = {
					while = {
						count = 107 
						limit = {check_variable = {which = ccs_while_count value > 0.999}}
						PREV = {
							create_ship = {
								name = random
								design = "NAME_Huntress"
								graphical_culture = "extra_dimensional_02"
							}
						}
						change_variable = {which = ccs_while_count value = -1}
					}
					set_variable = {which = ccs_while_count value = 10}#vanilla base value
					multiply_variable = {which = ccs_while_count value = ccs_small_unbidden_fleet_multiplier}#change number of ships
					while = {
						count = 135 
						limit = {check_variable = {which = ccs_while_count value > 0.999}}
						PREV = {
							create_ship = {
								name = random
								design = "NAME_Assassin"
								graphical_culture = "extra_dimensional_02"
							}
						}
						change_variable = {which = ccs_while_count value = -1}
					}
					set_variable = {which = ccs_while_count value = 20}#vanilla base value
					multiply_variable = {which = ccs_while_count value = ccs_small_unbidden_fleet_multiplier}#change number of ships
					while = {
						count = 256
						limit = {check_variable = {which = ccs_while_count value > 0.999}}
						PREV = {
							create_ship = {
								name = random
								design = "NAME_Predator"
								graphical_culture = "extra_dimensional_02"
							}	
						}
						change_variable = {which = ccs_while_count value = -1}
					}
				}
				set_location = {
					target = event_target:dimensional_portal
					distance = 5
					angle = random
				}
				set_fleet_stance = aggressive
				set_aggro_range = 500
				set_aggro_range_measure_from = self
			}
		}
	}
	if = {
		limit = {is_country_type = extradimensional_3}
		event_target:ccs_global_event = {
			set_variable = {which = ccs_while_count value = 8}#vanilla base value
			multiply_variable = {which = ccs_while_count value = ccs_small_unbidden_fleet_multiplier}#change number of ships
			change_variable = {which = ccs_while_count value = -1}#because one ship spwans before the while loop
		}
		create_fleet = {
			effect = {
				set_owner = PREV
				create_ship = {
					name = random
					design = "NAME_Eradicator"
					graphical_culture = "extra_dimensional_03"
				}
				assign_leader = last_created_leader
				event_target:ccs_global_event = {
					while = {
						count = 107 
						limit = {check_variable = {which = ccs_while_count value > 0.999}}
						PREV = {
							create_ship = {
								name = random
								design = "NAME_Eradicator"
								graphical_culture = "extra_dimensional_03"
							}
						}
						change_variable = {which = ccs_while_count value = -1}
					}
					set_variable = {which = ccs_while_count value = 10}#vanilla base value
					multiply_variable = {which = ccs_while_count value = ccs_small_unbidden_fleet_multiplier}#change number of ships
					while = {
						count = 135 
						limit = {check_variable = {which = ccs_while_count value > 0.999}}
						PREV = {
							create_ship = {
								name = random
								design = "NAME_Annihilator"
								graphical_culture = "extra_dimensional_03"
							}
						}
						change_variable = {which = ccs_while_count value = -1}
					}
					set_variable = {which = ccs_while_count value = 20}#vanilla base value
					multiply_variable = {which = ccs_while_count value = ccs_small_unbidden_fleet_multiplier}#change number of ships
					while = {
						count = 256 
						limit = {check_variable = {which = ccs_while_count value > 0.999}}
						PREV = {
							create_ship = {
								name = random
								design = "NAME_Obliterator"
								graphical_culture = "extra_dimensional_03"
							}	
						}
						change_variable = {which = ccs_while_count value = -1}
					}
				}
				set_location = {
					target = event_target:dimensional_portal
					distance = 5
					angle = random
				}
				set_fleet_stance = aggressive
				set_aggro_range = 500
				set_aggro_range_measure_from = self
			}
		}
	}
}