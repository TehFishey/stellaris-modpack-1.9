on_game_start = {
	events = {
		mzilli_distant_origin.1
	}
}

# A ship has surveyed a planet.
# Scope = Ship
# From = Planet
on_survey = {
	events = {
		mzilli_distant_origin.2
	}
}

# Executed right after country has established communications with another country
# This = Country which established the communications
# From = Country which communications were established with
on_post_communications_established = {
	events = {
		mzilli_distant_origin.8
	}
}

# A planet has begun the colonization process.
# Scope = Planet
on_colonization_started = {
	events = {
		mzilli_distant_origin.12
	}
}

# A war has been won
# Root = Winner Warleader
# From = Loser Warleader
# FromFrom = War
on_war_won = {
	events = {
		mzilli_distant_origin.13
	}
}