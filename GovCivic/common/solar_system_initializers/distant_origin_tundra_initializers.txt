@base_moon_distance = 10

distant_origin_homeworld_tundra_01 = {
	class = "rl_standard_stars"
	asteroids_distance = 85
	
	flags = { distant_origin_homeworld_tundra }
		
	planet = {
		count = 1
		class = star
		orbit_distance = 0
		orbit_angle = 1
		size = { min = 20 max = 30 }
		has_ring = no
	}
	
	change_orbit = 40
	
	planet = {
		count = 1
		orbit_distance = 20
		
		orbit_angle = { min = 90 max = 270 }
		change_orbit = @base_moon_distance

		moon = {
			count = { min = 0 max = 1 }
			orbit_angle = { min = 90 max = 270 }
			orbit_distance = 5
		}
	}
	
	change_orbit = 25
	
	planet = {
		count = { min = 1 max = 2 }
		class = random_asteroid
		orbit_distance = 0
		orbit_angle = { min = 120 max = 300 }
	}
	
	planet = {
		count = 1
		orbit_distance = 25
		orbit_angle = { min = 90 max = 270 }
		change_orbit = @base_moon_distance

		moon = {
			count = { min = 0 max = 1 }
			orbit_angle = { min = 90 max = 270 }
			orbit_distance = 5
		}
	}
	
	planet = {
		count = 1
		orbit_distance = 25
		class = pc_gas_giant
		orbit_angle = { min = 90 max = 270 }
		size = 25
		
		change_orbit = @base_moon_distance
		
		moon = {
			count = 1
			orbit_angle = { min = 90 max = 270 }
			orbit_distance = 4
		}
		
		moon = {
			count = 1
			class = pc_tundra
			orbit_distance = 10
			orbit_angle = { min = 90 max = 270 }
			size = { min = 16 max = 20 }
			modifiers = none
			flags = { distant_origin_homeworld_tundra }

			init_effect = {
				prevent_anomaly = yes
				random_list = {
					60 = {
						random_tile = {
							limit = { has_blocker = no }
							set_blocker = "tb_city_ruins"
						}
						random_tile = {
							limit = { has_blocker = no }
							set_blocker = "tb_city_ruins"
						}
					}
					20 = {
						change_pc = pc_nuked
						reroll_blockers = yes
					}
					10 = {
						change_pc = pc_gaia
						reroll_blockers = yes
						random_tile = {
							limit = { has_blocker = no }
							set_blocker = "tb_city_ruins"
						}
						random_tile = {
							limit = { has_blocker = no }
							set_blocker = "tb_city_ruins"
						}
					}
					5 = {
						change_pc = pc_barren
						add_modifier = { modifier = "terraforming_candidate" days = -1 }
					}
					5 = {
						change_pc = pc_barren_cold
						add_modifier = { modifier = "terraforming_candidate" days = -1 }
					}
				}
				if = {
					limit = { has_modifier = tidal_locked }
					remove_modifier = tidal_locked
				}
				if = {
					limit = { has_modifier = low_gravity }
					remove_modifier = low_gravity
				}
				if = {
					limit = { has_modifier = high_gravity }
					remove_modifier = high_gravity
				}
			}
		}
	}
	
	planet = {
		count = { min = 1 max = 3 }
		orbit_distance = 25
		orbit_angle = { min = 90 max = 270 }
	}
	
	planet = {
		count = { min = 1 max = 2 }
		orbit_distance = 20
		class = pc_gas_giant
		orbit_angle = { min = 90 max = 270 }
		
		change_orbit = @base_moon_distance

		moon = {
			count = { min = 0 max = 3 }
			orbit_angle = { min = 90 max = 270 }
			orbit_distance = 5
		}
	}
	
	planet = {
		count = { min = 0 max = 1 }
		orbit_distance = 20
		
		orbit_angle = { min = 90 max = 270 }
	}

	init_effect = {
		every_system_planet = {
			limit = {
				is_planet_class = pc_barren
				has_ring = no
				NOT = { has_planet_flag = distant_origin_homeworld_tundra }
			}
			random_list = {
				90 = { }
				10 = { set_planet_entity = { entity = "barren_planet_01_destroyed_entity" } }
			}
		}
		random_list = {
			80 = { }
			20 = {
				create_ambient_object = {
					type = "large_debris_object"
					location = solar_system
				}
				create_ambient_object = {
					type = "large_debris_object"
					location = solar_system
				}
				create_ambient_object = {
					type = "large_debris_object"
					location = solar_system
				}
				create_ambient_object = {
					type = "medium_debris_01_object"
					location = solar_system
				}
				create_ambient_object = {
					type = "medium_debris_02_object"
					location = solar_system
				}
				create_ambient_object = {
					type = "medium_debris_01_object"
					location = solar_system
				}
				create_ambient_object = {
					type = "medium_debris_02_object"
					location = solar_system
				}
				create_ambient_object = {
					type = "small_debris_object"
					location = solar_system
				}
			}
		}
	}
}

distant_origin_homeworld_tundra_02 = {
	class = "rl_standard_stars"	

	flags = { distant_origin_homeworld_tundra }
		
	planet = {
		count = 1
		class = star
		orbit_distance = 0
		orbit_angle = 1
		size = { min = 20 max = 30 }
		has_ring = no
	}
	
	change_orbit = 35
	
	planet = {
		count = { min = 1 max = 3 }
		orbit_distance = 15
		orbit_angle = { min = 90 max = 270 }
	}
	
	planet = {
		count = 1
		orbit_distance = 20
		class = pc_tundra
		orbit_angle = { min = 90 max = 270 }
		size = { min = 16 max = 20 }
		modifiers = none
		flags = { distant_origin_homeworld_tundra }

		init_effect = {
			prevent_anomaly = yes
			random_list = {
				60 = {
					random_tile = {
						limit = { has_blocker = no }
						set_blocker = "tb_city_ruins"
					}
					random_tile = {
						limit = { has_blocker = no }
						set_blocker = "tb_city_ruins"
					}
				}
				20 = {
					change_pc = pc_nuked
					reroll_blockers = yes
				}
				10 = {
					change_pc = pc_gaia
					reroll_blockers = yes
					random_tile = {
						limit = { has_blocker = no }
						set_blocker = "tb_city_ruins"
					}
					random_tile = {
						limit = { has_blocker = no }
						set_blocker = "tb_city_ruins"
					}
				}
				5 = {
					change_pc = pc_barren
					add_modifier = { modifier = "terraforming_candidate" days = -1 }
				}
				5 = {
					change_pc = pc_barren_cold
					add_modifier = { modifier = "terraforming_candidate" days = -1 }
				}
			}
			if = {
				limit = { has_modifier = tidal_locked }
				remove_modifier = tidal_locked
			}
			if = {
				limit = { has_modifier = low_gravity }
				remove_modifier = low_gravity
			}
			if = {
				limit = { has_modifier = high_gravity }
				remove_modifier = high_gravity
			}
		}

		change_orbit = @base_moon_distance

		moon = {
			count = { min = 0 max = 1 }
			orbit_angle = { min = 90 max = 270 }
			orbit_distance = 5
		}
	}
	
	planet = {
		count = { min = 1 max = 3 }
		orbit_distance = 25
		
		orbit_angle = { min = 90 max = 270 }
	}
	
	planet = {
		count = { min = 1 max = 2 }
		orbit_distance = 20
		class = pc_gas_giant
		orbit_angle = { min = 90 max = 270 }
		
		change_orbit = @base_moon_distance

		moon = {
			count = { min = 1 max = 3 }
			orbit_angle = { min = 90 max = 270 }
			orbit_distance = 5
		}
	}
	
	planet = {
		count = { min = 0 max = 1 }
		orbit_distance = 20
		orbit_angle = { min = 90 max = 270 }
	}

	init_effect = {
		every_system_planet = {
			limit = {
				is_planet_class = pc_barren
				has_ring = no
				NOT = { has_planet_flag = distant_origin_homeworld_tundra }
			}
			random_list = {
				90 = { }
				10 = { set_planet_entity = { entity = "barren_planet_01_destroyed_entity" } }
			}
		}
		random_list = {
			80 = { }
			20 = {
				create_ambient_object = {
					type = "large_debris_object"
					location = solar_system
				}
				create_ambient_object = {
					type = "large_debris_object"
					location = solar_system
				}
				create_ambient_object = {
					type = "large_debris_object"
					location = solar_system
				}
				create_ambient_object = {
					type = "medium_debris_01_object"
					location = solar_system
				}
				create_ambient_object = {
					type = "medium_debris_02_object"
					location = solar_system
				}
				create_ambient_object = {
					type = "medium_debris_01_object"
					location = solar_system
				}
				create_ambient_object = {
					type = "medium_debris_02_object"
					location = solar_system
				}
				create_ambient_object = {
					type = "small_debris_object"
					location = solar_system
				}
			}
		}
	}
}