@base_moon_distance = 10

distant_origin_xarmiri = {
	class = "sc_m"
	asteroids_distance = 85

	name = "New Xir"

	planet = {
		count = 1
		class = star
		orbit_distance = 0
		orbit_angle = 1
		size = { min = 20 max = 30 }
		has_ring = no
	}
	
	change_orbit = 40
	
	planet = {
		count = 1
		orbit_distance = 20
		class = random_non_colonizable
		orbit_angle = { min = 90 max = 270 }
		change_orbit = @base_moon_distance

		moon = {
			count = { min = 0 max = 1 }
			class = random_non_colonizable
			orbit_angle = { min = 90 max = 270 }
			orbit_distance = 5
		}
	}
	
	change_orbit = 25
	
	planet = {
		count = { min = 1 max = 2 }
		class = random_asteroid
		orbit_distance = 0
		orbit_angle = { min = 120 max = 300 }
	}
	
	planet = {
		count = 1
		orbit_distance = 25
		class = random_non_colonizable
		orbit_angle = { min = 90 max = 270 }
		change_orbit = @base_moon_distance

		moon = {
			count = { min = 0 max = 1 }
			class = random_non_colonizable
			orbit_angle = { min = 90 max = 270 }
			orbit_distance = 5
		}
	}
	
	planet = {
		count = 1
		orbit_distance = 25
		class = pc_barren
		orbit_angle = { min = 90 max = 270 }
		size = 25
		entity = "barren_planet_01_destroyed_entity"
		has_ring = no

		init_effect = {
			prevent_anomaly = yes
		}
		
		change_orbit = @base_moon_distance
		
		moon = {
			count = 1
			class = pc_ocean
			name = "New Xarmir"
			orbit_distance = 5
			orbit_angle = { min = 90 max = 270 }
			size = { min = 16 max = 20 }
			modifiers = none
			flags = { new_xarmir }

			init_effect = {
				prevent_anomaly = yes
			}
			init_effect = {
	
				every_tile = {

					limit = { has_blocker = yes }
					remove_blocker = yes
				}

				generate_start_buildings_and_blockers = yes
				create_species = {
					name = "Xarmiri"
					plural="Xarmiri"
					adjective="Xarmiri"
					class = MAM
					portrait = mam4
					traits = {
						trait="trait_pc_ocean_preference"
						trait="trait_nomadic"
						trait="trait_charismatic"
						trait="trait_conservational"
						trait="trait_slow_breeders"
					}
				}
				create_country = {

					name = "Resurgent Xarmiri Nations"
					adjective="Xarmiri"
					authority="auth_oligarchic"
					civics = {

						civic = civic_distant_origin
						civic = civic_citizen_service

					}

					species = last_created
					name_list = "MAM2"
					ship_prefix = "RXNS"
					ethos = {

						ethic = "ethic_xenophobe"

						ethic = "ethic_militarist"
						ethic = "ethic_egalitarian"
					}

					flag = {

						icon = {

							category = "spherical"

							file = "flag_spherical_11.dds"

						}

						background = {

							category = "backgrounds"

							file = "circle.dds"

						}

						colors = {

							"burgundy"

							"light_orange"

							"null"
							"null"

						}

					}

					room="personality_migrating_flock_room"
					type = default
				}

				while = {

					count = 8

					random_tile = {

						limit = {
							has_blocker = no

							has_grown_pop = no

							has_growing_pop = no

							has_building = yes

						}
						create_pop = { species = last_created }

					}

				}
				set_owner = last_created_country

				create_spaceport = {

					owner = last_created_country

					initial_module = "missile_weapon"

				}

				last_created_country = { add_minerals = 300 }

			}
		}
	}
	
	planet = {
		count = { min = 1 max = 3 }
		class = random_non_colonizable
		orbit_distance = 25
		orbit_angle = { min = 90 max = 270 }
	}
	
	planet = {
		count = { min = 1 max = 2 }
		orbit_distance = 20
		class = pc_gas_giant
		orbit_angle = { min = 90 max = 270 }
		
		change_orbit = @base_moon_distance

		moon = {
			count = { min = 0 max = 3 }
			class = random_non_colonizable
			orbit_angle = { min = 90 max = 270 }
			orbit_distance = 5
		}
	}
	
	planet = {
		count = { min = 0 max = 1 }
		orbit_distance = 20
		class = random_non_colonizable
		orbit_angle = { min = 90 max = 270 }
	}

	init_effect = {
		random_system_planet = {
			limit = { has_planet_flag = new_xarmir }
			owner = {
				set_country_flag = xarmiri
				create_fleet = {
					effect = {
						set_owner = prev

						create_ship = {
							name = random
							random_existing_design = science
						}

						set_fleet_stance = evasive				
						set_location = prev.capital_scope
						
						owner = {
							create_leader = {
								type = scientist
								sub_type = survey
								name = random
								species = owner_main_species
							}
						}
						set_leader = last_created_leader
					}			
				}
					
				#constructor
				create_fleet = {
					effect = {
						set_owner = prev
						
						create_ship = {
							name = random
							random_existing_design = constructor
						}
						
						set_fleet_stance = evasive
						set_location = prev.capital_scope
					}
				}

				#military
				create_fleet = {
					set_take_point = yes
					effect = {
						set_owner = prev
						
						while = {
							count = 3
							create_ship = {
								name = random
								random_existing_design = corvette
							}
						}
						
						set_location = prev.capital_scope
					}
				}
			}
		}
	}
}

distant_origin_pasceal = {
	class = "sc_k"

	name = "Xir"
		
	planet = {
		count = 1
		class = star
		orbit_distance = 0
		orbit_angle = 1
		size = { min = 20 max = 30 }
		has_ring = no
	}
	
	change_orbit = 35
	
	planet = {
		count = { min = 1 max = 3 }
		class = random_non_colonizable
		orbit_distance = 15
		orbit_angle = { min = 90 max = 270 }
	}
	
	planet = {
		count = 1
		orbit_distance = 20
		class = pc_ocean
		name = "Xarmir"
		orbit_angle = { min = 90 max = 270 }
		size = { min = 16 max = 20 }
		modifiers = none
		has_ring = yes
		flags = { xarmir }

		init_effect = {
			prevent_anomaly = yes
		}

		init_effect = {

			every_tile = {

				limit = { has_blocker = yes }
				remove_blocker = yes
			}

			generate_start_buildings_and_blockers = yes
			create_species = {
				name = "Pasceal"
				class = MOL
				portrait = mol15
				traits = {
					trait="trait_pc_ocean_preference"
					trait="trait_agrarian"
					trait="trait_natural_sociologists"
					trait="trait_communal"
					trait="trait_sedentary"
					trait="trait_slow_learners"
				}
				homeworld = this
			}
			create_country = {

				name = "Sacrosanct Imperium"
				adjective="Pasceal"
				authority="auth_imperial"
				civics = {

					civic = civic_homeworld_occupiers
					civic = civic_environmentalist
				}

				species = last_created
				name_list = "MOL1"
				ship_prefix = "ISV"
				ethos = {

					ethic = "ethic_fanatic_xenophobe"

					ethic = "ethic_spiritualist"
				}

				flag = {

					icon = {

						category = "pointy"

						file = "flag_pointy_6.dds"

					}

					background = {

						category = "backgrounds"

						file = "new_dawn.dds"

					}

					colors = {

						"light_green"

						"green"

						"null"
						"null"

					}

				}

				type = default
			}

			while = {

				count = 8

				random_tile = {

					limit = {
						has_blocker = no

						has_grown_pop = no

						has_growing_pop = no

						has_building = yes

					}
					create_pop = { species = last_created }

				}

			}
			set_owner = last_created_country

			create_spaceport = {

				owner = last_created_country

				initial_module = "laser_weapon"

			}

			last_created_country = { add_minerals = 300 }

		}
	}
	
	planet = {
		count = { min = 1 max = 3 }
		orbit_distance = 25
		class = random_non_colonizable
		orbit_angle = { min = 90 max = 270 }
	}
	
	planet = {
		count = { min = 1 max = 2 }
		orbit_distance = 20
		class = pc_gas_giant
		orbit_angle = { min = 90 max = 270 }
		
		change_orbit = @base_moon_distance

		moon = {
			count = { min = 1 max = 3 }
			class = random_non_colonizable
			orbit_angle = { min = 90 max = 270 }
			orbit_distance = 5
		}
	}
	
	planet = {
		count = { min = 0 max = 1 }
		class = random_non_colonizable
		orbit_distance = 20
		orbit_angle = { min = 90 max = 270 }
	}

	init_effect = {
		random_system_planet = {
			limit = { has_planet_flag = xarmir }
			owner = {
				set_country_flag = pasceal
				create_fleet = {
					effect = {
						set_owner = prev

						create_ship = {
							name = random
							random_existing_design = science
						}

						set_fleet_stance = evasive				
						set_location = prev.capital_scope
						
						owner = {
							create_leader = {
								type = scientist
								sub_type = survey
								name = random
								species = owner_main_species
							}
						}
						set_leader = last_created_leader
					}			
				}
					
				#constructor
				create_fleet = {
					effect = {
						set_owner = prev
						
						create_ship = {
							name = random
							random_existing_design = constructor
						}
						
						set_fleet_stance = evasive
						set_location = prev.capital_scope
					}
				}

				#military
				create_fleet = {
					set_take_point = yes
					effect = {
						set_owner = prev
						
						while = {
							count = 3
							create_ship = {
								name = random
								random_existing_design = corvette
							}
						}
						
						set_location = prev.capital_scope
					}
				}
			}
		}
		random_list = {
			50 = { }
			50 = {
				create_ambient_object = {
					type = "large_debris_object"
					location = solar_system
				}
				create_ambient_object = {
					type = "large_debris_object"
					location = solar_system
				}
				create_ambient_object = {
					type = "large_debris_object"
					location = solar_system
				}
				create_ambient_object = {
					type = "medium_debris_01_object"
					location = solar_system
				}
				create_ambient_object = {
					type = "medium_debris_02_object"
					location = solar_system
				}
				create_ambient_object = {
					type = "medium_debris_01_object"
					location = solar_system
				}
				create_ambient_object = {
					type = "medium_debris_02_object"
					location = solar_system
				}
				create_ambient_object = {
					type = "small_debris_object"
					location = solar_system
				}
			}
		}
	}
}