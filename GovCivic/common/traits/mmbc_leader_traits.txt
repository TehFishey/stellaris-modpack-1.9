trait_ruler_philosopher_king = {
	cost = 1
	modification = no
	icon = "gfx/interface/icons/traits/leader_traits/leader_trait_philosopher_king.dds"
	self_modifier = {
		leader_age = 30
		species_leader_exp_gain = +25%
	}
	leader_trait = yes
	leader_class = { ruler }
	initial = no
	randomized = no
}