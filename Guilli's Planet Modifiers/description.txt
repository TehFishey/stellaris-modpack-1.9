[img]https://i.imgur.com/v0c7ibh.png[/img]
[h1]Guilli's Planet Modifiers[/h1]
For version 1.8+ of the game. 
Compatible with all DLC. 
DLCs not required.
Compatible with virtually all mods.


[h1]What does this mod do?[/h1]
This mod adds 180+ new planet modifiers as well as increase the maximum planet modifier amount to 4. Adds new tile blockers and new armies. Tons of mini events around the modifiers. It also makes planet modifiers a bit stronger so planets become a true strategic choice to have! Furthermore, terraforming planets will remove bad modifiers and has a chance to give a good modifier or two!. It also adds planet modifiers to Barren worlds, Frozen worlds, Molten worlds, Toxic worlds and Gas Giants so terraforming mods can make full use out of these planets! Some modifiers will spawn tile blockers unique to those modifiers and some of them might require a tech to get rid of them. And so much more, see list below!



[h1]In Short:[/h1]
[list]
[*]180+ new Planet modifiers with unique icons.
[*]16 new planet tile blockers that show up with some of the new modifiers. Some techs to remove the tile blockers.
[*]3 new armies based on modifiers. Can only be built on planets with those modifiers.
[*]5 new exploration events. Surveying celestial object has a chance of discovering a modifier + resources.
[*]33 new "discovery!" events. Small event telling you discovered something interesting!
[*]40+ new colony events. There's a chance of a special event when owning a planet with a new planet modifier.
[*]Planet modifiers appear more frequently and in larger numbers.
[*]Up to 3 (rarely 4+) planet modifiers per planet.
[*]Planet modifiers can be strong and provide a strategic choice when it comes to planets. 
[*]Secret and rare precursor planet modifiers.
[*]3 New planet modifier borders! (grey, blue, purple)
[*]Planet modifiers also appear on Barren, Frozen, Toxic and Molten worlds and Gas Giants. As well as asteroids, suns, and other celestial bodies.
[*]Terraforming cleans up planet modifiers and has a chance to roll new good ones (see below for details)
[*]Ringworlds and Habitats have a chance to roll a unique modifier to indicate how well construction has gone. There are also unique ringworld and habitat modifiers to make them feel alive and unique.
[*] Unique modifiers for Ringworlds, Habitats, Fallen Empire Worlds.
[*] Tomb worlds hold ancient secrets. Precursor modifiers can be found more frequently here.
[*] New planet interactions with the crisis events. Bombarding infested worlds or worlds bombarded by the unbidden can turn into molten/frozen/barren/nuked worlds. Each can then roll new planet modifiers fitting them. 
[/list]


[img]https://i.imgur.com/Gsvkr4W.png[/img]
[h1]Wondrous Planets Update (new with 1.8)[/h1]
[b]How it works:[/b]
At the start of the game, one planet of each planet type will be chosen as a wondrous planet. This planet will have a unique tile blocker on it that provides very large adjacency bonuses as well as a planet 20% happiness bonus! Once you have surveyed and discovered a planet like this (or the AI has), the planet name will change to a blue colour and it will have a unique icon in front of it making it stand out in the vastness of space. You want to fight for these planets, especially early game. 


[img]https://i.imgur.com/utWsOZ1.png[/img]
[h1]Discovery! Events [/h1]
[b]How it works:[/b]
When your science ships survey a planet and discover a special modifier on that planet an event will pop up telling you about it. This only happens the first time you discover this modifier. Some of these "discovery" events will reward you with a bonus to something, others can reward you with resources. 
All discovery events have unique images.
It is possible to get multiple discovery events from the same planet but that should be fairly rare. You'll just get more event windows to read and click trough. 


[img]https://i.imgur.com/fvCRIT6.png[/img]
[h1]Colony Events [/h1]
[b]How it works:[/b]
Over 40 mini colony events have been added that can trigger after you have colonised a world with my modifiers. These events have a decent chance of showing up but it can happen that it takes a long time or not happen at all. Replayability is key here :). There are plans for a lot more mini colony events as well as event chains. If you have an idea for a cool event revolving around the planet modifiers let me know! 


[img]https://i.imgur.com/46pcbZf.png[/img]
[h1]Valuable World Modifier[/h1]
[b]How it works:[/b]
Some dead worlds (barren, frozen, etc) that have specific modifiers will also have a 'valuable world' modifier. This modifier allows terraforming of this world with the right technologies. 


[img]https://i.imgur.com/87GwSej.png[/img]
[h1]Terraforming Interaction[/h1]
[b]How it works:[/b]
When you terraform a planet the following will happen:
[list]
[*] Most bad negative modifiers will be removed (where it makes sense)
[*] Precursor modifiers will NOT be removed
[*] Planet wonders will be removed. 
[*] The newly terraformed planet will always gain a flavour or good modifier if it has less than 4 modifiers.
[*] The newly terraformed planet has an extra chance to roll new flavour or good modifiers (up to a maximum of 3 in total).
[*] Turning a planet into a Machine World will remove most modifiers and roll unique machine world modifiers.
[/list]
[i]It didn't make sense when terraforming a world with a bad surface that it would still have that bad surface. So now terraforming doesn't just fix that, it has a chance to make your planet even better! [/i]


[img]https://i.imgur.com/B7VzHEm.png[/img]
[h1]Crisis Interaction[/h1]
[b]How it works:[/b]
Crisis that purge or change planets will remove and/or roll new modifiers
[list]
[*]Prethoryn Swarm: Planets that get infested have most biological modifiers removed and roll some fitting modifiers
[*]Prethoryn Swarm: Cleansing infested worlds now turn the world into either a barren, molten, frozen or nuked  world. Modifiers are wiped and some fitting ones can be rolled.
[*] Unbidden: When the unbidden bombard a planet it now turns the world into either a barren, molten, frozen or nuked  world. Modifiers are wiped and some fitting ones can be rolled.
[/list]


[img]https://i.imgur.com/AdVvhFJ.png[/img]

[h1]Achievements?[/h1]
Does not work with achievements due to new files.


[h1]I feel overwhelmed! There's too many modifiers![/h1]
[b]This might help:[/b]
Green/Blue/Purple/Orange Border modifiers = Only good bonuses
Yellow Border Modifiers = Both good and bad bonuses.
Red Border Modifiers = Only bad bonuses
Grey border = Story text, no bonuses.


[h1]Want to help?[/h1]
I want to expand this mod with more planet modifiers. Feel free to leave suggestions in the comments or the suggestion post! Thank you very much!
Also looking for ideas for events and event chains. 
Also looking for translations!

Some more information about the mod can be found in the discussion thread here:
http://steamcommunity.com/workshop/filedetails/discussion/865040033/1495615865211530073/

[h1]Please rate and let me know what you think, Enjoy[/h1]


[h1]Do you like the mod and wish you could do something to help in some way?[/h1]
If you really like my mod and wish to support me, feel free to drop something. It would help a lot! Thank you very much!

[url=https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=2YNBVCV78USXN] [img]http://i.imgur.com/DR3G8dK.png[/img] [/url] 







--------------------------------------------------------------------------------------------


[img]https://i.imgur.com/uC2ksiI.png[/img]
[h1]Planet Modifiers[/h1]
[b]How it works:[/b]
At the start of the game, every planet in the galaxy rolls between 0 and 3 planet modifiers that fit it's planet class. 
[list]
[*]There is a chance the world rolls an ancient precursor modifier. These are extremely rare and powerful!
[*]Fallen Empire worlds start with unique, very powerful, "utopian" planet modifiers. They have a pink-purple border. These modifiers show off how advanced these worlds are. 
[*]Machine Worlds roll their own special and unique planet modifiers. They have an orange border colour. 
[*]Ringworlds and habitats have their own unique planet modifiers which can show up when you finish constructing them.
[*]Tarraforming a world can roll new modifiers (see terraforming section below).
[/list]



[h1]Vanilla Files that are overwritten[/h1]
This might help:
[b]Does not overwrite vanilla files except:[/b]
[list]
[*]gfx\interface\icons\planet_modifiers\modifier_frames.dds
[*]planet modifier VALUES from the common\static_modifiers folder. Does not overwrite the files themselves.
[*]The Ascendancy Perk "Mastery of Nature" to allow new tileblocker tech from being unlocked by it.
[*]common\terraform\02_special_terraform_links.txt this to make custom terraforming mechanic work with vanilla planets.
[*]Two specific events from the unbidden and scourge crisis. (crisis.200 and crisis.1011)
[/list]


Compatible with virtually all mods.
[b]Built in Compatibility for [/b] [url=https://steamcommunity.com/sharedfiles/filedetails/?id=819148835] Planetary Diversity [/url] The new planet types will also have these modifiers. 
[b]Built in Compatibility for [/b] [url=https://steamcommunity.com/sharedfiles/filedetails/?id=822305739] More Star Classes [/url] The new star types will also have these modifiers. 
[b]Built in Compatibility for [/b] [url=https://steamcommunity.com/sharedfiles/filedetails/?id=937289339] Real Space [/url] Graphical Objects that come with modifiers are scaled accordingly
[b]Built in Compatibility for [/b] [url=https://steamcommunity.com/sharedfiles/filedetails/?id=947379037] (Star Trek) New Worlds [/url] The new planet types will also have these modifiers.
[b]Built in Compatibility for [/b] [url=https://steamcommunity.com/sharedfiles/filedetails/?id=688086068] Star Trek: New Horizons [/url] The new planet types will also have these modifiers. Removed modifiers that no not fit the mod or star trek universe
[b]Compatibility with [/b] [url=https://steamcommunity.com/sharedfiles/filedetails/?id=1085097357] Immersive Galaxy - Planet Variety [/url]
[b]Compatibility with [/b] [url=https://steamcommunity.com/sharedfiles/filedetails/?id=948359179] Planetary Shields [/url]


[img]https://i.imgur.com/PEmGB7x.png[/img]
[h1]Planetary Modifier Border Colours [/h1]
[b]How it works:[/b]
Planet modifiers come in 6 border colours!
Green = Only positive bonuses
Yellow = positive and negative bonuses (mixed)
Red = only bad bonuses
Grey = no bonuses, just flavour/text/story
Blue = precursor bonuses (really good!)
Bright Orange = Machine World Modifiers
Purple = special modifiers that allow you to do extra things like build unique armies, ships(maybe soon), or modifiers that are the result of an event (usually also give a good bonus somewhere). 


[h1]List of Planet Modifiers added[/h1]
[b]Modifiers range from 5% all the way up to 200% for some very rare modifiers![/b]
See the discussion thread below for a overall list. If you don't want to spoil yourself, don't look at it :)


[h1]For modders:[/h1]
My events use pm_ and c_ preffixes
Most of my files use pm_ preffixes
To check for my mod on gamestart use global flag “has_guillis_planet_modifiers_mod”


[h1]Debugging commands:[/h1]
You can debug planet modifiers (add/remove them) using the following console command:
event pm_debug.0
- 0 to 184 depending on what you need.
0 = wipe all planet modifiers
1 = reroll planet modifiers
2 to 184 = add/remove specific planet modifiers (apply to add a modifier, apply same command again to remove it)
See the list in the discussions on the steam page for a full list of planet modifiers and their matching debug numbers.


[h1]Localisations[/h1]
Mod has the following localisations:
[list]
[*] English
[*] German ( Special thanks to p6kocka for the German Translations! )
[*] Spanish ( Special thanks to Alguerath for the Spanish Translations! )
[/list]



[h1]Special thanks to the following people for suggestions and feedback![/h1]
[b]A huge and special thanks to Heather H. for proofreading all the English localisation and providing corrections and improvements. Thank you very much! [/b]
-Alguerath for Spanish translations
-p6kocka for the German translations
-PBG
-Cybrxkhan
-HFY
-Sportsmaster
-Jerev
-Krebsig
-MrMarbles
-Hapchazzard
-jrr101
-Birdy
-Waesche for the cool colour name trick
Special thanks to the original creators of all the art. 

