name="Guilli's Planet Modifiers [1.9.*]"
path="mod/Guilli's Planet Modifiers"
tags={
	"Events"
	"Galaxy Generation"
	"Gameplay"
}
picture="Thumbnail.png"
supported_version="1.9.*"
