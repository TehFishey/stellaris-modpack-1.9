
namespace = pm_test


### colour planet name
planet_event = {
	id = pm_test.1
	hide_window = yes
	is_triggered_only = yes
	
	immediate = {
		log = "--- PM --- TEST EVENT 1 EXECUTED"
		print_scope_effect = yes
		set_name = "color_precursor"
	}
}
### colour system name
planet_event = {
	id = pm_test.2
	hide_window = yes
	is_triggered_only = yes
	
	immediate = {
		log = "--- PM --- TEST EVENT 2 EXECUTED"
		print_scope_effect = yes
		solar_system = {
			set_name = "color_precursor_col"
			set_star_flag = pm_is_star_precursor
			set_star_flag = pm_is_star_wondrous
		}
	}
}