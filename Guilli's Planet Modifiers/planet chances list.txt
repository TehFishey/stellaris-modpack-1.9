# pc_asteroid_accretion
# pc_orbital_megastructure

		modifier = {
			add = 10
			OR = {
				# Suns, Blackholes and such
				# is_planet_class = pc_b_star
				# is_planet_class = pc_a_star
				# is_planet_class = pc_f_star
				# is_planet_class = pc_g_star
				# is_planet_class = pc_k_star
				# is_planet_class = pc_m_star
				# is_planet_class = pc_neutron_star
				# is_planet_class = pc_pulsar
				# is_planet_class = pc_black_hole
				# is_planet_class = pc_gas_giant
			}
		}
		modifier = {
			add = 10
			OR = {
				# Asteroid
				# is_planet_class = pc_asteroid				
			}
		}
		modifier = {
			add = 10
			OR = {
				# Artificial
				is_planet_class = pc_nuked
				# is_planet_class = pc_ringworld_habitable
				# is_planet_class = pc_ringworld_habitable_damaged
				# is_planet_class = pc_ringworld_tech
				# is_planet_class = pc_ringworld_tech_damaged
				# is_planet_class = pc_ringworld_seam
				# is_planet_class = pc_ringworld_seam_damaged
			}
		}
		modifier = {
			add = 10
			OR = {
				# Atmosphere / Life
				is_planet_class = pc_gaia
				is_planet_class = pc_continental
				is_planet_class = pc_alpine
				is_planet_class = pc_tropical
				is_planet_class = pc_ocean
				is_planet_class = pc_savannah
				is_planet_class = pc_toxic
				is_planet_class = pc_hothouse
				is_planet_class = pc_desertislands
				is_planet_class = pc_cascadian
				is_planet_class = pc_swamp
				is_planet_class = pc_oasis
				is_planet_class = pc_steppe
				is_planet_class = pc_mangrove
				is_planet_class = pc_hajungle
			}
		}
		modifier = {
			add = 10
			OR = {
				# No Atmosphere / Dead
				is_planet_class = pc_molten
				is_planet_class = pc_barren
				is_planet_class = pc_barren_cold
				is_planet_class = pc_frozen
				is_planet_class = pc_arctic
				is_planet_class = pc_tundra
				is_planet_class = pc_arid
				is_planet_class = pc_desert
				is_planet_class = pc_ammonia
				is_planet_class = pc_methane
				is_planet_class = pc_postgarden
				is_planet_class = pc_geothermal
				is_planet_class = pc_hadesert
				is_planet_class = pc_frozen_desert
				is_planet_class = pc_mesa
				is_planet_class = pc_subarctic
				is_planet_class = pc_sandsea
				is_planet_class = pc_glacial
				is_planet_class = pc_antarctic
			}
		}
		
##################################
# Vanilla and PD factor sections #
##################################
	
		# Vanilla habitible factor		
		modifier = {
			factor = @vanilla_factor
			OR = {
				is_planet_class = pc_gaia
				is_planet_class = pc_continental
				is_planet_class = pc_alpine
				is_planet_class = pc_tropical
				is_planet_class = pc_ocean
				is_planet_class = pc_savannah
				is_planet_class = pc_arid
				is_planet_class = pc_desert
				is_planet_class = pc_arctic
				is_planet_class = pc_tundra
			}
		}
		# Planetary Div habitible factor		
		modifier = {
			factor = @pd_factor
			OR = {
				is_planet_class = pc_hothouse
				is_planet_class = pc_desertislands
				is_planet_class = pc_cascadian
				is_planet_class = pc_swamp
				is_planet_class = pc_oasis
				is_planet_class = pc_steppe
				is_planet_class = pc_postgarden
				is_planet_class = pc_geothermal
				is_planet_class = pc_hadesert
				is_planet_class = pc_frozen_desert
				is_planet_class = pc_mesa
				is_planet_class = pc_subarctic
				is_planet_class = pc_sandsea
				is_planet_class = pc_glacial
				is_planet_class = pc_antarctic
				is_planet_class = pc_mangrove
				is_planet_class = pc_hajungle
			}
		}
##################################
# Vanilla and PD factor sections #
##################################