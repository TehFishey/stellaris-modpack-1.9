admiral_quirk_defensive = {
	icon = "gfx/interface/icons/traits/admiral_quirk_defensive.dds"
	cost = 0
	opposites = {
	#Generic Leader Flaws
		"leader_trait_stubborn"
	#LO Generic Leader Flaws
		"leader_flaw_greedy"
		"leader_flaw_terminal"
	#LO Generic Leader Quirks
		"leader_quirk_enduring"
		"leader_quirk_sickly"
		"leader_quirk_smart"
	#LO Admiral Flaws
		"admiral_flaw_hesitant"
		"admiral_flaw_poor_maintainer"
		"admiral_flaw_proud"
		"admiral_flaw_sitting_duck"
		"admiral_flaw_slacker"
	#LO Admiral Quirks
		"admiral_quirk_defensive"
		"admiral_quirk_evasive"
		"admiral_quirk_offensive"
		"admiral_quirk_reinforced"
		"admiral_quirk_scrappy"
	}
	modifier = {
		ship_hitpoints_mult = 0.05
		ship_evasion_mult = -0.05
	}
	leader_trait = yes
	leader_class = { admiral }
	modification = no
}

admiral_quirk_evasive = {
	icon = "gfx/interface/icons/traits/admiral_quirk_evasive.dds"
	cost = 0
	opposites = {
	#Generic Leader Flaws
		"leader_trait_stubborn"
	#LO Generic Leader Flaws
		"leader_flaw_greedy"
		"leader_flaw_terminal"
	#LO Generic Leader Quirks
		"leader_quirk_enduring"
		"leader_quirk_sickly"
		"leader_quirk_smart"
	#LO Admiral Flaws
		"admiral_flaw_hesitant"
		"admiral_flaw_poor_maintainer"
		"admiral_flaw_proud"
		"admiral_flaw_sitting_duck"
		"admiral_flaw_slacker"
	#LO Admiral Quirks
		"admiral_quirk_defensive"
		"admiral_quirk_evasive"
		"admiral_quirk_offensive"
		"admiral_quirk_reinforced"
		"admiral_quirk_scrappy"
	}
	modifier = {
		ship_evasion_mult = 0.05
	 	ship_fire_rate_mult = -0.04
	}
	leader_trait = yes
	leader_class = { admiral }
	modification = no
}

admiral_quirk_offensive = {
	icon = "gfx/interface/icons/traits/admiral_quirk_offensive.dds"
	cost = 0
	opposites = {
	#Generic Leader Flaws
		"leader_trait_stubborn"
	#LO Generic Leader Flaws
		"leader_flaw_greedy"
		"leader_flaw_terminal"
	#LO Generic Leader Quirks
		"leader_quirk_enduring"
		"leader_quirk_sickly"
		"leader_quirk_smart"
	#LO Admiral Flaws
		"admiral_flaw_hesitant"
		"admiral_flaw_poor_maintainer"
		"admiral_flaw_proud"
		"admiral_flaw_sitting_duck"
		"admiral_flaw_slacker"
	#LO Admiral Quirks
		"admiral_quirk_defensive"
		"admiral_quirk_evasive"
		"admiral_quirk_offensive"
		"admiral_quirk_reinforced"
		"admiral_quirk_scrappy"
	}
	modifier = {
	 	ship_fire_rate_mult = 0.04
		ship_hitpoints_mult = -0.05
	}
	leader_trait = yes
	leader_class = { admiral }
	modification = no
}

admiral_quirk_reinforced = {
	icon = "gfx/interface/icons/traits/admiral_quirk_reinforced.dds"
	cost = 0
	opposites = {
	#Generic Leader Flaws
		"leader_trait_stubborn"
	#LO Generic Leader Flaws
		"leader_flaw_greedy"
		"leader_flaw_terminal"
	#LO Generic Leader Quirks
		"leader_quirk_enduring"
		"leader_quirk_sickly"
		"leader_quirk_smart"
	#LO Admiral Flaws
		"admiral_flaw_hesitant"
		"admiral_flaw_poor_maintainer"
		"admiral_flaw_proud"
		"admiral_flaw_sitting_duck"
		"admiral_flaw_slacker"
	#LO Admiral Quirks
		"admiral_quirk_defensive"
		"admiral_quirk_evasive"
		"admiral_quirk_offensive"
		"admiral_quirk_reinforced"
		"admiral_quirk_scrappy"
	}
	modifier = {
		ship_evasion_mult = -0.03
		ship_speed_mult = -0.12
		ship_combat_speed_mult = -0.12
		ship_hitpoints_mult = 0.06
	}
	leader_trait = yes
	leader_class = { admiral }
	modification = no
}

admiral_quirk_scrappy = {
	icon = "gfx/interface/icons/traits/admiral_quirk_scrappy.dds"
	cost = 0
	opposites = {
	#Generic Leader Flaws
		"leader_trait_stubborn"
	#LO Generic Leader Flaws
		"leader_flaw_greedy"
		"leader_flaw_terminal"
	#LO Generic Leader Quirks
		"leader_quirk_enduring"
		"leader_quirk_sickly"
		"leader_quirk_smart"
	#LO Admiral Flaws
		"admiral_flaw_hesitant"
		"admiral_flaw_poor_maintainer"
		"admiral_flaw_proud"
		"admiral_flaw_sitting_duck"
		"admiral_flaw_slacker"
	#LO Admiral Quirks
		"admiral_quirk_defensive"
		"admiral_quirk_evasive"
		"admiral_quirk_offensive"
		"admiral_quirk_reinforced"
		"admiral_quirk_scrappy"
	}
	modifier = {
		ship_weapon_range_mult = -0.10
	 	ship_hitpoints_mult = 0.05
	}
	leader_trait = yes
	leader_class = { admiral }
	modification = no
}