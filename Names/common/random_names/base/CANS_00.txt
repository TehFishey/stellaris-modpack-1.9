asteroid_prefix = {
	XW3-
	LV8-
	ANM-
	LUL-
	NPN-
	A5N-
	OK-
	X22-
	P57-
	APP-
	HL1-
	F1K-
	YT9-
	VGVV-
	M03- #
	L0K- 
	JP- 
	CN- 
	VN- 
	K9- 
	O15- 
	A66P- 
	ABX- 
	DDK- 
	NMJ- 
	Z8- 
	D2- 
	F7- 
	UT- 
	YH- 
	JK- 
	BR8- 
	V3- 
	FZ1- 
	X3-
	IU- #
	518P-
	22Y-
	DIX4-
	LJK-
	W0T-
	H3H-
	U-
	X-
	Q1-
	Q2-
	L1U-
	MA0P-
	RCT-
	T35-
	5KY-
	DOUB-
	PI9X-
	JAX-
}

asteroid_postfix = {
	1453
	1227
	666
	51-X
	9-P
	0RZ
	22-I
	3-H
	MU-2
	QP-44
	220
	1421
	1337
	1066
	867
	768
	1519
	1945
	1888
	1905
	1914
	4100
	37A9O
	KS-85 #
	GK-1206
	CLE0
	NARM
	L1-P0
	X1-SH1
	LVB
	I776
	MVR-1CA
	D3-SV
	PHO #
	M31-M31
	IV
	V
	R1M
	M8
	5
	2
	8
	1992
	1066W
	1227GK
	666H
	I
	X
	Q-2
	OO-P
	W0-T
	HAX
	P2-9
	K2-L
}


#Where I put all my references unfortunately

nebula_names = {	
	"Cursed Garden of Wupakk"
	"Pihoc Nebula"
	"Eye of Fear"
	"Eye of Despair"
	"The Red Eye"
	"Eye of Endless Night"
	"Eye of Eternal Death"
	"Hyouk Nebula"
	"Orek Dust Clouds"
	"Eru Dust Clouds"
	"Satosh Dust Clouds"
	"Miyak Dust Clouds"	
	"Igarar Nebula"
	"Igatih Nebula"
	"Imoyok Nebula"
	"Nerak Nebula"
	"Ihikust Nebula"
	"Urabnak Nebula"
	"Awakenah Nebula"
	"Iguo Veil"
	"Ihcados Shroud"
	"Ubonish Veil"
	"Emem Shroud"	
	"Ikiak Badlands"	
	"Iunegak Nebula"	
	"Igustoy Nebula"	
	"Ioyam Nebula"	
	"Ukognes Nebula"
	"Jupong Expanse"
	"Amulli Expanse"
	"Jo-tjen Garden Nebula"
	"Gan-Hun Maixogi Patch"
	"Cuchox Garden Nebula"
	"Evtirpedi Patch"
	"Abkaval Patch"
	"Addhowex Nebula"
	"Emimeodiwe Nebula"
	"Gunerzepin Nebula"
	"Aguanmaz Nebula"
	"Profane Nightmare of Nonnameenak"
	"Nightmare of Gangsinhekh"
	"Fields of Madness"
	"Fields of Nancald"
	"Fields of Hoor"
	"Fields of Kaohyu"
	"Pit of Curses"
	"Hell of Olebbom"
	"Fallen Labyrinth"
	"Circle of Shadows"
	"Circle of Jiafbayhusi"
	"Circle of Motkoi"
	"Charred Citadel"
	"The Malevolent Realm"
	"Tower of Maleficent"
	"Grave of the Forsaken"
	"Grave of Dutenser"
	"Grave of Schjab"
	"Prison of the Infernal"
	"Prison of Zhulaginge"
	"Prison of Ishix"
	"Storm of Rakhapotakit"
	"Storm of Jamscaikuar"
	"Storm of Humsum"
	"Baleful Chambers Nebula"
	"Gauntlet of Darkness Nebula"
	"Dark Vaults Nebula"
	"Titanic Fields Nebula"
	"Shadowfall Nebula"
	"Wounded Night Nebula"
	"Grim Shade Nebula"
	"Imperfect Harmony Nebula"
	"Crimson Sky Nebula"
	"Arcane Wrath Nebula"
	"Lost Pit Nebula"
	"Aonar'kowk Patch"
	"Canomi'kom Patch"
	"Dyogref'oga Miasma"
	"Sdogai Miasma"
	"Inatkh Shroud"
	"Black Delve Nebula"
	"Wormbarrow Nebula"
	"Onyx Sanctum Nebula"
	"Silver Plains Nebula"
	"Akaksi Dust Clouds"
	"Honyacchi Dust Clouds"
	"Confluence of Death"
	"Confluence of Zarnlew Trafk"
	"Confluence of Utysio"
	"Confluence of Nuj-Amufkuya"
	"Confluence of Mai'Zishimiok"
	"Nameless Shadow"
	"Black Pit"
	"Oothar Veil"
	"Tacahnid Veil"
	"Void of Carnage" #
	"Charcoal Mist"
	"Great Blood Mist"
	"Endless Mist"
	"Cursed Mist"
	"Nehzegh's Mist"
	"Lenrovi Adicodan's Mist"
	"Mist of Icotriva Narhoven"
	"Eijim Neton's Mist"
	"Oblivion Vaults"
	"Lifeless Temple"
	"Fiery Shore"
	"Molten Shore"
	"Crumbling Shore"
	"Reef of Twilight"
	"Reef of Illusions"
	"Scarlet Reef"
	"Desolate Reef"
	"Reef of Haunts"
	"Broken Mist"
	"Cobalt Labyrinth"
	"Infernal Labyrinth"
	"Labyrinth of the Scourge"
	"Screaming Fragment"
	"Ruined Fragment"
	"Midnight's Gloom"
	"Ashen Nightfall"
	"Scarlet Nightfall"
	"Ruby Gloom"
	"Amaranth's Doom"
	"Vermillion Gloom"
	"Deathless Nightfall"
	"Ebon Shade"
	"Frosted Shadow"
	"Hallowed Shade"
	"Astral Shade"
	"Frozen Gateway"
	"Soulless Spirit"
	"Golden Spirit"
	"Voiceless Nightmare"
	"Thundering Blood"
	"Sapphire Ruin"
	"Grim Immortal"
	"Mausoleum of Blood"
	"Copper Necropolis"
	"Tomb of the Shadows"
	"Catacombs of the Condemned"
	"Grail of Dhowadex"
	"Obelisk of Despair"
	"Cursed Obelisk"
	"Eternal Pillar"
	"Bottomless Pillar"
	"Obelisk of 'slo Negsale"
	"Obelisk of Foricialna"
	"Lacos' Pillar"
	"Pillar of Cafpici"
	"Sorrowful Redoubt"
	"Stillscream Redoubt"
	"Codaap's Redoubt"
	"Reboubt of Eifaf-memse-alor-eto"
	"Fortress of Oslee-laad Feit Fome"
	"Nibosh Fortress"
	"Kuyi'kai Fortress"
	"Frost of Ngoyukishunni"
	"Yearning Cavern"
	"Mirrored Caverns"
	"Ghostly Caverns"
	"Desolate Glen"
	"Cerulean Glen"
	"Lilac Sea"
	"Azure Sea"
	"Boundless Sea"
	"Sea of Stars"
	"Tyrian Hope"
	"Void of Corpses" #
	"Golden Mansion"
	"Silver Estate"
	"House of Bronze"
	"Stability of the Endless"
	"Break of the Cosmos"
	"Fog of the Ancients"
	"Maze of the Wanderers"
	"Haze of the Deathless"
	"Graveyard of the Fireflies"
	"Grave of the Fireflies"
	"Lord of Hope"
	"Lady of Light"
	"Vision of Sparkling Gems"
	"Hope of Eternal Dreams"
	"Light of Hundred Souls"
	"Might of Hundred Stars"
	"Joy of Light"
	"Glory of Hope"
	"Pillar of the Mighty"
	"Pillar of the Blessed"
	"Pillar of Abundance"
	"Pillar of Prosperity"
	"Pillar of Mirth"
	"Pillar of Euphoria"
	"Pillar of Night"
	"Everlasting Flame"
	"Noble Granite"
	"Titantic Dreams"
	"Victorious Night"
	"Eye of the Gods"
	"Eye of the Heavens"
	"Eye of the Ancients"
	"Eye of the Forefathers"
	"Eye of the Ancestors"
	"House of the Ancestors"
	"House of the Predecessors"
	"Lingering Doubt"
	"Persistent Midnight"
	"Glory Nebula"
	"Holy Nebula"
	"Ancestral Nebula"
	"Elder Nebula"
	"Secret Nebula"
	"Enigma Nebula"
	"Haven of Hope"
	"Haven of Light"
	"Haven of Marble"
	"Haven of Red"
	"Haven of Innocence"
	"Haven of Illumination"
	"Shelter of the Blessed"
	"Shelter of the Divine"
	"Shelter of Happiness"
	"Shelter of Fortune"
	"Shelter of Harmony"
	"Shelter of Devotion"
	"Auspicious Origin"
	"Enigmatic Origin"
	"Harmonious Origin"
	"Blessed Origin"
	"Wall of Magnificence"
	"Wall of Darkness"
	"Wall of the Far-Sighted"
	"Wall of Devastation"
	"Wall of Obliteration"
	"Wall of Ruin"
	"Wall of Light"
	"Wall of Dangers"
	"Wall of the Ancestors"
	"Endless Stream"
	"Astral Stream"
	"Toxic Stream"
	"Ancestral Stream"
	"Life's Stream"
	"Death's Stream"
	"Haven of Love"
}
