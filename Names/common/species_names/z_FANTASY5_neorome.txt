# Generated species derive their names from here
# Rule of thumb: at least one of each culture for each species, except mammalian and humanoid which gets at least ~4 per each

### MAMMALIANS

MAM = {
	ramuadin = {
		name = Ramuadian
		plural = Ramuadians
		home_planet = Ramuadin
		home_system = Celluppayan
		name_list = "z_FANTASY5"
	}
	nadyipu = {
		name = Nadyipese
		plural = Nadyipese
		home_planet = "Id Nadyipu"
		home_system = Meaitayi
		name_list = "z_FANTASY5"
	}
	temyetirayump = {
		name = Tirayan
		plural = Tirayans
		home_planet = Temyetirayump
		home_system = Ceypirayup
		name_list = "z_FANTASY5"
	}
	ittagaya = {
		name = Ittagayan
		plural = Ittagayans
		home_planet = "Cye Iap Ittagaya"
		home_system = Paymisaa
		name_list = "z_FANTASY5"
	}
}

### FUNGOID

FUN = {
	etayu = {
		name = Etayoid
		plural = Etayoids
		home_planet = Etayu
		home_system = Ecpilansaa 
		name_list = "z_FANTASY5"
	}
	hiyamia = {
		name = Hiyamian
		plural = Hiyamians
		home_planet = Hiyamia
		home_system = "Tiye Ap Um" 
		name_list = "z_FANTASY5"
	}
	abundantrealm = {
		name = "Itta Runnusaa"
		plural = "Itta Runnusaa"
		home_planet = "Abundant Realm"
		home_system = "Deathless Fortune" 
		name_list = "z_FANTASY5"
	}
	iapmil = {
		name = Iapmilid
		plural = Iapmilids
		home_planet = Iapmil
		home_system = Yetirunnulan 
		name_list = "z_FANTASY5"
	}
}

### ARTHROPOID

ART = {
	pupay = {
		name = Pupoid
		plural = Pupoids
		home_planet = Pupay
		home_system = Raypupay
		name_list = "z_FANTASY5"
	}
	scatpirunnap = {
		name = Runnapoid
		plural = Runnapoids
		home_planet = Scatpi-Runnap
		home_system = Npidipun
		name_list = "z_FANTASY5"
	}
	tyil = {
		name = Tyil
		plural = Tyil
		home_planet = Tyil
		home_system = Raymaye
		name_list = "z_FANTASY5"
	}
	iapar = {
		name = Iap-Aroid
		plural = Iap-Aroids
		home_planet = "Iap Ar"
		home_system = Mampualaysaa
		name_list = "z_FANTASY5"
	}
}

### REPTILIAN

REP = {
	timalan = {
		name = Timalese  
		plural = Timalese
		home_planet = Timalan
		home_system = Edudipsaa
		name_list = "z_FANTASY5"
	}
	sayetusaa = {
		name = Sayetusan  
		plural = Sayetusans
		home_planet = Sayetusaa
		home_system = "I Cay-Nuyri"
		name_list = "z_FANTASY5"
	}
	tebmerayul = {
		name = Tebmerayan  
		plural = Tebmerayans
		home_planet = Tebmerayul
		home_system = Nagiyamp
		name_list = "z_FANTASY5"
	}
	autuadil = {
		name = Autuadilan  
		plural = Autuadilans
		home_planet = Autuadil
		home_system = Filansaa
		name_list = "z_FANTASY5"
	}
}

### AVIAN 

AVI = {
	henpansaa = {
		name = Henpian 
		plural = Henpians
		home_planet = Henpansaa
		home_system = Tebupsaa
		name_list = "z_FANTASY5"
	}
	dayyu = {
		name = Dayyid 
		plural = Dayyids
		home_planet = Dayyu
		home_system = Ciyiyan
		name_list = "z_FANTASY5"
	}
	dayucaya = {
		name = Dayucayan 
		plural = Dayucayans
		home_planet = Dayucaya
		home_system = Pyirupsaa
		name_list = "z_FANTASY5"
	}
	assipisaa = {
		name = Assipisan 
		plural = Assipisans
		home_planet = Assipisaa
		home_system = Cye-Assipisaa
		name_list = "z_FANTASY5"
	}
}

### MOLLUSCOID

MOL = {
	tuayalan = {
		name = Tuayalanese
		plural = Tuayalanese
		home_planet = Tuayalan
		home_system = Cyialunne
		name_list = "z_FANTASY5"
	}
	salai = {
		name = Saloid
		plural = Saloids
		home_planet = Salai
		home_system = Yicugasaa
		name_list = "z_FANTASY5"
	}
	hollowcastle = {
		name = Teyyupish
		plural = Teyyupish
		home_planet = "Hollow Castle"
		home_system = "Forged in Sky"
		name_list = "z_FANTASY5"
	}
	elumi = {
		name = Elumian
		plural = Elumians
		home_planet = Elumi
		home_system = Luyiyay
		name_list = "z_FANTASY5"
	}
}

### PLANTOID

PLANT = {
	gatup = {
		name = Gatupoid
		plural = Gatupoids
		home_planet = Gatup
		home_system = Umude
		name_list = "z_FANTASY5"
	}
	iasaalan = {
		name = Iasaalanoid
		plural = Iasaalanoids
		home_planet = Iasaalan
		home_system = Nadlumunpyiyasaa-Ga-Um
		name_list = "z_FANTASY5"
	}
}

### HUMANOIDS

HUM = {
	annanney = {
		name = Annannese
		plural = Annannese
		home_planet = Annanney
		home_system = Pyehudaimp
		name_list = "z_FANTASY5"
	}
	yipe = {
		name = Yipean
		plural = Yipeans
		home_planet = Yipe
		home_system = Iccayyimp
		name_list = "z_FANTASY5"
	}
	irui = {
		name = Irui
		plural = Irui
		home_planet = "Tal Irui"
		home_system = Rabmidun
		name_list = "z_FANTASY5"
	}
	riveroffire = {
		name = Arpish
		plural = Arpish
		home_planet = "River of Fire"
		home_system = "Sea of Ghosts"
		name_list = "z_FANTASY5"
	}
	ernimbaa = {
		name = Ernimban
		plural = Ernimbans
		home_planet = Ernimbaa
		home_system = Eyyiru
		name_list = "z_FANTASY5"
	}
}