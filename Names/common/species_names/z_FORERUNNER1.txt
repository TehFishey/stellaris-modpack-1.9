# Generated species derive their names from here
# Rule of thumb: at least one of each culture for each species, except mammalian which gets at least ~4 per each

### MAMMALIANS

MAM = {
	florix = {
		name = Florixian
		plural = Florixians
		home_planet = "Light of Florix"
		home_system = Florix
		name_list = "z_FORERUNNER1"
	}
	cuglara = {
		name = Cuglaran
		plural = Cuglarans
		home_planet = Cuglara
		home_system = Cuglara
		name_list = "z_FORERUNNER1"
	}
	eprei = {
		name = Epreian
		plural = Epreians
		home_planet = Eprei
		home_system = "Oiter Zrai"
		name_list = "z_FORERUNNER1"
	}
	ewum = {
		name = Ewumite
		plural = Ewumites
		home_planet = Ewum
		home_system = Gruft
		name_list = "z_FORERUNNER1"
	}
}

### FUNGOID

FUN = {
	iamia = {
		name = Iamian
		plural = Iamians
		home_planet = Iamia
		home_system = Iamia 
		name_list = "z_FORERUNNER1"
	}
	flumetera = {
		name = Flumeteran
		plural = Flumeterans
		home_planet = Flumetera
		home_system = Flunadar 
		name_list = "z_FORERUNNER1"
	}
	huaruta = {
		name = Huarutan
		plural = Huarutans
		home_planet = Huaruta
		home_system = "Isuayo Deh"
		name_list = "z_FORERUNNER1"
	}
	foscurn = {
		name = Foscurnid
		plural = Foscurnids
		home_planet = Foscurn
		home_system = Sloagso 
		name_list = "z_FORERUNNER1"
	}
}

### ARTHROPOID

ART = {
	oliv = {
		name = Olivan
		plural = Olivans
		home_planet = Oliv-Aju
		home_system = Oliv 
		name_list = "z_FORERUNNER1"
	}
	mixisog = {
		name = Mixisog
		plural = Mixisog
		home_planet = Mixisog
		home_system = Mixisog
		name_list = "z_FORERUNNER1"
	}
	axuab = {
		name = Axuab
		plural = Axuab
		home_planet = Axuab-yix-Daor
		home_system = Qublorix
		name_list = "z_FORERUNNER1"
	}
	glaf = {
		name = Glafoid
		plural = Glafoids
		home_planet = Glaf
		home_system = Uicarro
		name_list = "z_FORERUNNER1"
	}
}

### REPTILIAN

REP = {
	oelara = {
		name = Oelaran  
		plural = Oelarans
		home_planet = Oelara
		home_system = Oelara
		name_list = "z_FORERUNNER1"
	}
	izram = {
		name = Izramian
		plural = Izramians
		home_planet = Izram
		home_system = "Izram Peucler"
		name_list = "z_FORERUNNER1"
	}
	swora = {
		name = S'worian  
		plural = S'worians
		home_planet = S'wora
		home_system = Chaltos
		name_list = "z_FORERUNNER1"
	}
	ewoxul = {
		name = Ewoxulan  
		plural = Ewoxulans
		home_planet = Ewoxul
		home_system = Chupumia
		name_list = "z_FORERUNNER1"
	}
}

### AVIAN 

AVI = {
	kuria = {
		name = Kurian 
		plural = Kurians
		home_planet = Kuria
		home_system = Kuria
		name_list = "z_FORERUNNER1"
	}
	ocheria = {
		name = Ocherian
		plural = Ocherians
		home_planet = Ocheria
		home_system = Ocheria
		name_list = "z_FORERUNNER1"
	}
	peiria = {
		name = Peirian
		plural = Peirians
		home_planet = Peiria
		home_system = "Agraa Kli"
		name_list = "z_FORERUNNER1"
	}
	zroil = {
		name = Zroil
		plural = Zroil
		home_planet = Zroil
		home_system = Theyuria
		name_list = "z_FORERUNNER1"
	}
}

### MOLLUSCOID

MOL = {
	bafrolla = {
		name = Bafrollan
		plural = Bafrollans
		home_planet = Bafrolla
		home_system = Asalfrolla
		name_list = "z_FORERUNNER1"
	}
	estrion = {
		name = Estrionan
		plural = Estrionans
		home_planet = Estrion
		home_system = Asharth
		name_list = "z_FORERUNNER1"
	}
	mucliri = {
		name = Mucliroid
		plural = Mucliroids
		home_planet = Mucliri
		home_system = "Brip'Vrog Veyyama"
		name_list = "z_FORERUNNER1"
	}
	oxuovis = {
		name = Oxuovite
		plural = Oxuovites
		home_planet = O-xuovis
		home_system = O-plumdoy
		name_list = "z_FORERUNNER1"
	}
}

### PLANTOID

PLANT = {
	poulara = {
		name = Poularan
		plural = Poularans
		home_planet = Poulara
		home_system = Toubos
		name_list = "z_FORERUNNER1"
	}
	iotania = {
		name = Iotanian
		plural = Iotanians
		home_planet = "Iotania Raenia"
		home_system = Plucurus
		name_list = "z_FORERUNNER1"
	}
	vaks = {
		name = Vakoid
		plural = Vakoids
		home_planet = "Slorth lud Vaks"
		home_system = Locratania
		name_list = "z_FORERUNNER1"
	}
	praavlax = {
		name = Praavlax
		plural = Praavlax
		home_planet = Praavlax
		home_system = Se'trosie
		name_list = "z_FORERUNNER1"
	}
}



### HUMANOIDS

HUM = {
	branek = {
		name = Branekian
		plural = Branekians
		home_planet = Branek
		home_system = Noamdul
		name_list = "z_FORERUNNER1"
	}
	comdos = {
		name = Comdish
		plural = Comdish
		home_planet = Comdos
		home_system = Ixis
		name_list = "z_FORERUNNER1"
	}
	izragei = {
		name = Izrageian
		plural = Izrageians
		home_planet = Izragei
		home_system = "Prua Hilt"
		name_list = "z_FORERUNNER1"
	}
	taense = {
		name = Taensid
		plural = Taensids
		home_planet = Yo'Taense
		home_system = Yo'Prua
		name_list = "z_FORERUNNER1"
	}
	ichol = {
		name = Icholite
		plural = Icholites
		home_planet = "Ichol Ux'Waans"
		home_system = "Vluploi Ux'Kakduh"
		name_list = "z_FORERUNNER1"
	}
	plore = {
		name = Plorean
		plural = Ploreans
		home_planet = Plore
		home_system = "Hascov Pubrore"
		name_list = "z_FORERUNNER1"
	}
	vreb = {
		name = Vreb
		plural = Vreb
		home_planet = Vrebaflu
		home_system = Zlaph
		name_list = "z_FORERUNNER1"
	}
	izuyaoms = {
		name = Izuyaomese
		plural = Izuyaomese
		home_planet = Izuyaoms
		home_system = Iayphus
		name_list = "z_FORERUNNER1"
	}
}


### MACHINES

MACHINE = {
	selus1 = {
		name = Selucon
		plural = Selucons
		home_planet = Selus-1
		home_system = Selus-0
		name_list = "z_FORERUNNER1"
	}
}