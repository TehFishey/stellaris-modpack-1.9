# Generated species derive their names from here
# Rule of thumb: at least one of each culture for each species, except mammalian and humanoid which gets at least ~4 per each

### MAMMALIANS

MAM = {
	toyotsuda = {
		name = Toyo
		plural = Toyos
		home_planet = Toyotsuda
		home_system = "Tenba Yonaka"
		name_list = "zz_THEME2"
	}
	kamotsuki = {
		name = Kamokese
		plural = Kamokese
		home_planet = Kamotsuki
		home_system = Saitakata
		name_list = "zz_THEME2"
	}
	naganei = {
		name = Naganeian
		plural = Naganeians
		home_planet = Kushi-Naganei
		home_system = Kushi-Miyoda
		name_list = "zz_THEME2"
	}
	shinohekata = {
		name = Waji
		plural = Waji
		home_planet = "Shinohe Kata"
		home_system = Hachigasakurano
		name_list = "zz_THEME2"
	}
	sakurayama = {
		name = Sakuran
		plural = Sakurans
		home_planet = Sakurayama
		home_system = Sakurayama
		name_list = "zz_THEME2"
	}
}

### FUNGOID

FUN = {
	anabae = {
		name = Anabite
		plural = Anabites
		home_planet = Anabae
		home_system = Komaibari 
		name_list = "zz_THEME2"
	}
	itotorida = {
		name = "Maki Ata"
		plural = "Maki Ata"
		home_planet = "Ito Torida"
		home_system = "Ito Chibukai"		
		name_list = "zz_THEME2"
	}
	nisshima = {
		name = Nisshimoid
		plural = Nisshimoids
		home_planet = Nisshima
		home_system = Yokakyushu 
		name_list = "zz_THEME2"
	}
	usukawa = {
		name = Usukawan
		plural = Usukawans
		home_planet = Usukawa
		home_system = Kikugo 
		name_list = "zz_THEME2"
	}
}

### ARTHROPOID

ART = {
	dazawachi = {
		name = Hoku
		plural = Hoku
		home_planet = Dazawachi
		home_system = Kamisatte
		name_list = "zz_THEME2"
	}
	misayashikai = {
		name = Misa-Shina
		plural = Misa-Shina
		home_planet = "Misa Yashikai"
		home_system = "Misa Kyotan"
		name_list = "zz_THEME2"
	}
	onosu = {
		name = Onosese
		plural = Onosese
		home_planet = Onosu
		home_system = Higashima
		name_list = "zz_THEME2"
	}
}

### REPTILIAN

REP = {
	toshikawa = {
		name = Nan  
		plural = Nans
		home_planet = Toshikawa
		home_system = Nahamakushi
		name_list = "zz_THEME2"
	}
	muraiyama = {
		name = Muraiese  
		plural = Muraiese
		home_planet = Murai-Yama
		home_system = Otano-Tami-Murai
		name_list = "zz_THEME2"
	}
	fukuyamato = {
		name = Kosu-Chi  
		plural = Kosu-Chi
		home_planet = Fukuyamato
		home_system = Asagawata
		name_list = "zz_THEME2"
	}
	namiima = {
		name = Imian  
		plural = Imians
		home_planet = "Nami Ima"
		home_system = "Nari Hatsushi"
		name_list = "zz_THEME2"
	}
}

### AVIAN 

AVI = {
	fukuchi = {
		name = Fukuchian 
		plural = Fukuchians
		home_planet = Fukuchi
		home_system = "Karide Kawa"
		name_list = "zz_THEME2"
	}
	igashiure = {
		name = Igashian 
		plural = Igashians
		home_planet = Igashi-Ure
		home_system = Igashi-Ryu
		name_list = "zz_THEME2"
	}
	kagaruto = {
		name = Kagarese 
		plural = Kagarese
		home_planet = Kagaruto
		home_system = Nahasuda
		name_list = "zz_THEME2"
	}
}

### MOLLUSCOID

MOL = {
	mikyushusato = {
		name = Mikyoid
		plural = Mikyoids
		home_planet = "Mikyushu Sato"
		home_system = "Tsumoi Sato"
		name_list = "zz_THEME2"
	}
	sakatsuku = {
		name = "Oka Minami"
		plural = "Oka Minami"
		home_planet = Sakatsuku
		home_system = Sakatsuku
		name_list = "zz_THEME2"
	}
	toyoshi = {
		name = Toyoshite
		plural = Toyoshites
		home_planet = Toyoshi
		home_system = Nakasugi
		name_list = "zz_THEME2"
	}
}

### PLANTOID

PLANT = {
	kyobunjiito = {
		name = Higa
		plural = Higas
		home_planet = "Kyo Bunji-Ito"
		home_system = "Tose Sago"
		name_list = "zz_THEME2"
	}
	shinohara = {
		name = Shinoid
		plural = Shinoids
		home_planet = Shinohara
		home_system = Shimoto
		name_list = "zz_THEME2"
	}
	tokaimi = {
		name = Tokaimese
		plural = Tokaimese
		home_planet = Tokaimi
		home_system = Neyama
		name_list = "zz_THEME2"
	}
}

### HUMANOIDS

HUM = {
	matsuno = {
		name = Matsunite
		plural = Matsunites
		home_planet = "Saki Matsuno"
		home_system = "Saki Kizurume"
		name_list = "zz_THEME2"
	}
	kamizu = {
		name = "Yabe Ari"
		plural = "Yabe Ari"
		home_planet = Kamizu
		home_system = Hakomagori
		name_list = "zz_THEME2"
	}
	matsu = {
		name = Shi
		plural = Shi
		home_planet = Matsu
		home_system = Sakaho
		name_list = "zz_THEME2"
	}
	nirasakaho = {
		name = Sakahese
		plural = Sakahese
		home_planet = Nira-Sakaho
		home_system = Kasahama
		name_list = "zz_THEME2"
	}
	otsuru = {
		name = Otsuran
		plural = Otsurans
		home_planet = Otsuru
		home_system = Obihiroi
		name_list = "zz_THEME2"
	}
}