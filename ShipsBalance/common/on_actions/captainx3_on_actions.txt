on_game_start = {
    events = {
        nscflaglite.1 #Installs "has_nsc" country flag.
		nscflaglite.2 #Installs "NSC Control Player" country flag.
		nscflaglite.12 #Gives all human players the Drydock technology so AI will not use.
    }
}

on_monthly_pulse = {
	events = {
		nscflaglite.3 #Checks to see if the player activated NSC mid game, and if so, installs all appropriate flags.
		nscflaglite.4 #Checks if any "new countries" have arisen in an NSC Stellaris campaign and gives them the "has NSC" flag.
		nscflaglite.10 #Checks for newly formed countries and gives them NSC Component block technologies if NSC Control Player activates NSC Components.
		nscflaglite.11 #Checks for newly formed countries and gives them NSC block tech for needed reactors if NSC Control Player declines NSC Components.
		nsc_edicts.4 #Leads to individual counts of NSC planet production edicts to apply proper costs for said edicts.
	}
}

on_press_begin = {
	events = {
		NSC_features.2 #For Playable Guardian Standalone compatibility, will only trigger if said mod is active.
		nscflaglite.5 #Checks which Component Mods (BOSP, ESC, Crystallis) are active and determines if Player gets Component Feature.
		nscflaglite.7 #Leads to event that will give NSC Control Player option of NSC Components
	}
}

on_ship_built = {
	events = {
		nsc_flagship_check.1 #Tells Player Flagship is built.
		nsc_flagship_warning.1 #Tells Player HQ Station is built.
	}
}

on_border_change_station_lost = { events = { nsc_hq.303 } } #Makes sure that if the player loses the HQ Station to a "boarder change", it are automatically deleted.

on_fleet_disbanded = { events = { nsc_flagship_check.301 } } #Tells Player Flagship has been destroyed.

on_ship_destroyed_victim = { events = { nsc_flagship_check.302 } } #Tells Player Flagship has been destroyed.

empire_init_create_ships = { events = { NSC_game_start.1 } } #Gives the player 5 more corvettes on game start.