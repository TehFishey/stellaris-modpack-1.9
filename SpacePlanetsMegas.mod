name="*CST* SpacePlanetsMegas MERGE [1.9.*]"
path="mod/SpacePlanetsMegas"
tags={
	"Galaxy Generation"
	"Fallen Empire"
	"Megastructures"
}
picture="realspace.png"
supported_version="1.9.*"
