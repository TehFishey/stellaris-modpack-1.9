anomaly_category = {
	key = "koeleothan_world_category"
	desc = "koeleothan_world_category_desc"
	picture = GFX_evt_mining_station
	level = 1
	
	spawn_chance = {		
		modifier = {
			add = 100
			has_planet_flag = pf_koeleothan_world
		}
	}

	on_spawn = {
	}
	
	on_success = {
	}

	on_fail = {
	}

	on_critical_fail = {
	}
}

anomaly_category = {
	key = "xenograss_world_category"
	desc = "xenograss_world_category_desc"
	picture = GFX_evt_mining_station
	level = 1
	
	spawn_chance = {		
		modifier = {
			add = 100
			has_planet_flag = pf_xenograss_world
		}
	}

	on_spawn = {
	}
	
	on_success = {
	}

	on_fail = {
	}

	on_critical_fail = {
	}
}

anomaly_category = {
	key = "quantumpearl_world_category"
	desc = "quantumpearl_world_category_desc"
	picture = GFX_evt_mining_station
	level = 1
	
	spawn_chance = {		
		modifier = {
			add = 100
			has_planet_flag = pf_quantumpearl_world
		}
	}

	on_spawn = {
	}
	
	on_success = {
	}

	on_fail = {
	}

	on_critical_fail = {
	}
}

anomaly_category = {
	key = "woorskyr_world_category"
	desc = "woorskyr_world_category_desc"
	picture = GFX_evt_mining_station
	level = 1
	
	spawn_chance = {		
		modifier = {
			add = 100
			has_planet_flag = pf_woorskyr_world
		}
	}

	on_spawn = {
	}
	
	on_success = {
	}

	on_fail = {
	}

	on_critical_fail = {
	}
}

anomaly_category = {
	key = "beetle_world_category"
	desc = "beetle_world_category_desc"
	picture = GFX_evt_mining_station
	level = 1
	
	spawn_chance = {		
		modifier = {
			add = 100
			has_planet_flag = pf_beetle_world
		}
	}

	on_spawn = {
	}
	
	on_success = {
	}

	on_fail = {
	}

	on_critical_fail = {
	}
}