#--
#--		--------------------------- --------------------------- --------------------------- ---------------------------
#--		Fallen Empire Expanded - Initializer for Fallen Empires
#--		--------------------------- --------------------------- --------------------------- ---------------------------
#--
#--		PREFACE
#--		•	This file handles the creation of randomly generated fallen empires.
#--		•	This file provides the framework to enable the following FE homeworld system initializers.
#--
#-- 	MASTER FILES
#--		>	common/solar_system_initializer
#--
#-- 	SLAVE FILES
#--		>	common/solar_system_initializer
#--		>	common/global_ship_designs
#--		>	common/governments
#--		>	common/name_lists
#--		>	common/global_ship_designs
#--
#--		FALLEN EMPIRE HOMEWORLDS
#--		•	"Ethereal Bastion" 				->	Xenophile
#--		•	"Empyral Sanctuary" 			->	Spiritualist
#--		•	"Eternal Citadel" 				->	Xenophobe
#--		•	"Celestial Dynamo" 				->	Materialist
#--		•	"Sol Prime" 					->	Xenophile  			[EVENT] - See events/FEE_initializer_events.txt
#--		•	"Terran Wasteland" 				->	Xenophobe 			[EVENT] - See events/FEE_initializer_events.txt
#--		•	"Undying Fortress" 				->	Militarist
#--		>	"Calescent Nest" 				->	Hive Mind    
#--
#--		--------------------------- --------------------------- --------------------------- ---------------------------
#--





#	---------------------------
#	Fanatic Materialist -> "Celestial Dynamo"
#	---------------------------

fallen_empire_5 = {
	graphical_culture = "fallen_empire_02"
	initializer = "FEE_fallen_5"

	create_country_effect = {
		create_species = {
			name = random
			class = random_non_machine
			portrait = random
			traits = random
			extra_trait_points = 3
			allow_negative_traits = no
		}
		create_country = {
			name = random
			type = fallen_empire
			ignore_initial_colony_error = yes
			authority = auth_imperial
			civics = {
				civic = civic_lethargic_leadership
				civic = civic_empire_in_decline
			}
			species = last_created
			ethos = {
				ethic = ethic_fanatic_materialist
			}
			flag = random
			effect = {
				set_country_flag = fallen_empire_1	
				add_minerals = 10000
				add_energy = 10000	
				add_food = 1000
				add_influence = 500
				# must initialize global designs here
				add_global_ship_design = "NAME_Savant"
				add_global_ship_design = "NAME_Scholar"
				add_global_ship_design = "NAME_Sage"
				add_global_ship_design = "NAME_Obscurer"
				add_global_ship_design = "NAME_Cloaker"
				add_global_ship_design = "NAME_Librarian"
				add_global_ship_design = "NAME_Seeker"
				modify_species = {
					species = this
					add_trait = trait_cybernetic
				}
			}
		}
	}
}





#	---------------------------
#	Fanatic Spiritualist -> "Empyrean Sanctuary"
#	---------------------------
fallen_empire_6 = {
	graphical_culture = "fallen_empire_01"
	initializer = "FEE_fallen_6"

	create_country_effect = {
		create_species = {
			name = random
			class = random_non_machine
			portrait = random
			traits = random
			extra_trait_points = 3
			allow_negative_traits = no
		}
		create_country = {
			name = random
			type = fallen_empire
			ignore_initial_colony_error = yes
			authority = auth_imperial
			civics = {
				civic = civic_lethargic_leadership
				civic = civic_empire_in_decline
			}
			species = last_created
			ethos = {
				ethic = ethic_fanatic_spiritualist
			}
			flag = random
			effect = {
				set_country_flag = fallen_empire_2
				add_minerals = 10000
				add_energy = 10000	
				add_food = 1000
				add_influence = 500
				# must initialize global designs here
				add_global_ship_design = "NAME_Eternal"
				add_global_ship_design = "NAME_Avatar"
				add_global_ship_design = "NAME_Zealot"
				add_global_ship_design = "NAME_Penitent"
				add_global_ship_design = "NAME_Bulwark"
				add_global_ship_design = "NAME_Faith"
				add_global_ship_design = "NAME_Pilgrim"
				modify_species = {
					species = this
					add_trait = trait_psionic
				}
			}
		}
	}
}





#	---------------------------
#	Fanatic Xenophile -> "Ethereal Bastion"
#	---------------------------
fallen_empire_7 = {
	graphical_culture = "fallen_empire_03"
	initializer = "FEE_fallen_7"

	create_country_effect = {
		create_species = {
			name = random
			class = random_non_machine
			portrait = random
			traits = random
			extra_trait_points = 5
			allow_negative_traits = no
		}
		create_country = {
			name = random
			type = fallen_empire
			ignore_initial_colony_error = yes
			authority = auth_imperial
			civics = {
				civic = civic_lethargic_leadership
				civic = civic_empire_in_decline
			}
			species = last_created
			ethos = {
				ethic = ethic_fanatic_xenophile
			}
			flag = random
			effect = {
				set_country_flag = fallen_empire_3
				add_minerals = 10000
				add_energy = 10000	
				add_food = 1000
				add_influence = 500	
				# must initialize global designs here
				add_global_ship_design = "NAME_Keeper"
				add_global_ship_design = "NAME_Custodian"
				add_global_ship_design = "NAME_Warden"
				add_global_ship_design = "NAME_Watcher" # small station
				add_global_ship_design = "NAME_Sentinel" # large station
				add_global_ship_design = "NAME_Seeder"
				add_global_ship_design = "NAME_Builder"
			}
		}
	}
}





#	---------------------------
#	Fanatic Xenophobe -> "Eternal Citadel"
#	---------------------------
fallen_empire_8 = {
	graphical_culture = "fallen_empire_04"
	initializer = "FEE_fallen_8"

	create_country_effect = {
		create_species = {
			name = random
			class = random_non_machine
			portrait = random
			traits = random
			extra_trait_points = 5
			allow_negative_traits = no
		}
		create_country = {
			name = random
			type = fallen_empire
			ignore_initial_colony_error = yes
			authority = auth_imperial
			civics = {
				civic = civic_lethargic_leadership
				civic = civic_empire_in_decline
			}
			species = last_created
			ethos = {
				ethic = ethic_fanatic_xenophobe
			}
			flag = random
			effect = {
				set_country_flag = fallen_empire_4
				add_minerals = 10000
				add_energy = 10000	
				add_food = 1000
				add_influence = 500	
				# must initialize global designs here
				add_global_ship_design = "NAME_Imperium"
				add_global_ship_design = "NAME_Supremacy"
				add_global_ship_design = "NAME_Glory"
				add_global_ship_design = "NAME_Reaper"
				add_global_ship_design = "NAME_Devastator"
				add_global_ship_design = "NAME_Servitor"
				add_global_ship_design = "NAME_Destiny"
				#add_modifier = {
				#	modifier = "FEE_stagnated_borders"
				#	days = -1
				#}
			}
		}
	}
}


#	---------------------------
#	Fanatic Militarist -> "Undying Fortress"
#	---------------------------
fallen_empire_militarist = {
	graphical_culture = "fallen_empire_04"
	initializer = "FEE_fallen_militarist"

	create_country_effect = {
		create_species = {
			name = random
			class = random_non_machine
			portrait = random
			traits = random
			extra_trait_points = 5
			allow_negative_traits = no
		}
		create_country = {
			name = random
			type = fallen_empire
			ignore_initial_colony_error = yes
			authority = auth_imperial
			civics = {
				civic = civic_lethargic_leadership
				civic = civic_empire_in_decline
			}
			species = last_created
			ethos = {
				ethic = ethic_fanatic_militarist
			}
			flag = random
			effect = {
				set_country_flag = fallen_empire_militarist
				add_minerals = 10000
				add_energy = 10000	
				add_food = 1000
				add_influence = 500	
				# must initialize global designs here
				add_global_ship_design = "NAME_Imperium"
				add_global_ship_design = "NAME_Supremacy"
				add_global_ship_design = "NAME_Glory"
				add_global_ship_design = "NAME_Reaper"
				add_global_ship_design = "NAME_Devastator"
				add_global_ship_design = "NAME_Servitor"
				add_global_ship_design = "NAME_Destiny"
			}
		}
	}
}

#	---------------------------
#	Hive Mind -> "Calescent Nest"
#	---------------------------
fallen_empire_hivemind = {
	graphical_culture = "swarm_01"
	initializer = "FEE_fallen_hivemind"
	
	possible = {
		host_has_dlc = "Utopia"
	}

	create_country_effect = {
		create_species = {
			name = "Azalthyn"
			class = SWARM
			portrait = random
			traits = {
				trait = trait_hive_mind
				trait = trait_specialized_adaption
				trait = trait_natural_farmers
				trait = trait_masters_of_industry
				trait = trait_entrepreneural
				trait = trait_very_strong
				trait = trait_rapid_breeders
			}
			extra_trait_points = 6
			allow_negative_traits = no
			immortal = yes
		}
		create_country = {
			name = "Azalthyn Hive"
			type = fallen_empire
			ignore_initial_colony_error = yes
			authority = auth_hive_mind
			civics = {
				civic = civic_gestalt_torpor
				civic = civic_passive_drones
			}
			species = last_created
			name_list = "HIVE"
			ethos = {
				ethic = ethic_gestalt_consciousness
			}
			flag = {
				icon={
					category="zoological"
					file="flag_zoological_3.dds"
				}
				background={
					category="backgrounds"
					file="vertical.dds"
				}
				colors={
					"orange"
					"black"
					"null"
					"null"
				}
			}
			effect = {
				set_country_flag = FE_hivemind
				add_minerals = 10000
				add_energy = 10000	
				add_food = 1000
				add_influence = 500	
				# must initialize global designs here
				add_global_ship_design = "NAME_Progenitor"
				add_global_ship_design = "NAME_Overlord"
				add_global_ship_design = "NAME_Drone"
				add_global_ship_design = "NAME_Hive"
				add_global_ship_design = "NAME_Beholder"
				add_global_ship_design = "NAME_Drone-Constructor"
				add_global_ship_design = "NAME_Drone-Seeder"
				# add some extra border range
				add_modifier = {
					modifier = fallen_machine_empire
					days = -1
				}
			}
		}
	}
}