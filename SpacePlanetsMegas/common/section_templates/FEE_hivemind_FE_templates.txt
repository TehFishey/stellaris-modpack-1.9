#
#	--------------------------- --------------------------- --------------------------- ---------------------------
#	Fallen Empire Expanded - "common/section_templates/FEE_hivemind_FE_templates.txt"
#	--------------------------- --------------------------- --------------------------- ---------------------------
#
#	FILE INFO
#		•	Handles component placement on ships
#		•	Hivemind FE Ship Assets
#
#	FILE COUPLING
#		•	gfx/models/ships
#		•	common/ship_sizes
#		•	common/section_templates
#		•	common/global_ship_designs
#
#	--------------------------- --------------------------- --------------------------- ---------------------------
#


#	--------------------------- --------------------------- ---------------------------
#	Hivemind FE - Battlecruiser
#	--------------------------- --------------------------- ---------------------------

ship_section_template = {
	key = "FEE_hivemind_FE_battlecruiser_key"
	ship_size = FEE_hivemind_FE_battlecruiser
	fits_on_slot = mid
	entity = "warship_large_entity"
	icon = "GFX_ship_part_core_mid"

	#component_slot = {
	#	name = "EXTRA_LARGE_01"
	#	slot_size = extra_large
	#	slot_type = weapon
	#	locatorname = "root"
	#}
	#component_slot = {
	#	name = "EXTRA_LARGE_02"
	#	slot_size = extra_large
	#	slot_type = weapon
	#	locatorname = "root"
	#}
	component_slot = {
		name = "LARGE_GUN_01"
		slot_size = large
		slot_type = weapon
		locatorname = "root"
	}
	component_slot = {
		name = "LARGE_GUN_02"
		slot_size = large
		slot_type = weapon
		locatorname = "root"
	}
	component_slot = {
		name = "MEDIUM_GUN_01"
		slot_size = medium
		slot_type = weapon
		locatorname = "large_gun_01"
		is_side_slot = yes
		rotation = 90
	}
	component_slot = {
		name = "MEDIUM_GUN_02"
		slot_size = medium
		slot_type = weapon
		locatorname = "large_gun_02"
		is_side_slot = yes
		rotation = 90
	}
	component_slot = {
		name = "MEDIUM_GUN_03"
		slot_size = medium
		slot_type = weapon
		locatorname = "large_gun_03"
		is_side_slot = yes
		rotation = 90
	}
	component_slot = {
		name = "MEDIUM_GUN_04"
		slot_size = medium
		slot_type = weapon
		locatorname = "large_gun_01"
		is_side_slot = yes
		rotation = 90
	}
	component_slot = {
		name = "MEDIUM_GUN_05"
		slot_size = medium
		slot_type = weapon
		locatorname = "large_gun_02"
		is_side_slot = yes
		rotation = 90
	}
	component_slot = {
		name = "MEDIUM_GUN_06"
		slot_size = medium
		slot_type = weapon
		locatorname = "large_gun_03"
		is_side_slot = yes
		rotation = 90
	}

	large_utility_slots = 8
	aux_utility_slots = 5
}

#	--------------------------- --------------------------- ---------------------------
#	Hivemind FE - Titan
#	--------------------------- --------------------------- ---------------------------

ship_section_template = {
	key = "FEE_hivemind_FE_titan_key"
	ship_size = FEE_hivemind_FE_titan
	fits_on_slot = mid
	entity = "warship_large_entity"
	icon = "GFX_ship_part_core_mid"

	component_slot = {
		name = "EXTRA_LARGE_01"
		slot_size = extra_large
		slot_type = weapon
		locatorname = "root"
	}
	component_slot = {
		name = "EXTRA_LARGE_02"
		slot_size = extra_large
		slot_type = weapon
		locatorname = "root"
	}
	component_slot = {
		name = "EXTRA_LARGE_03"
		slot_size = extra_large
		slot_type = weapon
		locatorname = "root"
	}
	component_slot = {
		name = "EXTRA_LARGE_04"
		slot_size = extra_large
		slot_type = weapon
		locatorname = "root"
	}
	component_slot = {
		name = "MEDIUM_GUN_01"
		slot_size = medium
		slot_type = weapon
		locatorname = "large_gun_01"
	}
	component_slot = {
		name = "MEDIUM_GUN_02"
		slot_size = medium
		slot_type = weapon
		locatorname = "large_gun_02"
	}
	component_slot = {
		name = "MEDIUM_GUN_03"
		slot_size = medium
		slot_type = weapon
		locatorname = "large_gun_03"
	}
	component_slot = {
		name = "MEDIUM_GUN_04"
		slot_size = medium
		slot_type = weapon
		locatorname = "large_gun_01"
	}
	component_slot = {
		name = "MEDIUM_GUN_05"
		slot_size = medium
		slot_type = weapon
		locatorname = "large_gun_02"
	}
	component_slot = {
		name = "MEDIUM_GUN_06"
		slot_size = medium
		slot_type = weapon
		locatorname = "large_gun_03"
	}
	component_slot = {
		name = "STRIKE_CRAFT_01"
		slot_size = large
		slot_type = strike_craft
		locatorname = "large_gun_01"
	}
	component_slot = {
		name = "STRIKE_CRAFT_02"
		slot_size = large
		slot_type = strike_craft
		locatorname = "large_gun_02"
	}
	component_slot = {
		name = "STRIKE_CRAFT_03"
		slot_size = large
		slot_type = strike_craft
		locatorname = "large_gun_03"
	}
	component_slot = {
		name = "STRIKE_CRAFT_04"
		slot_size = large
		slot_type = strike_craft
		locatorname = "large_gun_01"
	}
	component_slot = {
		name = "STRIKE_CRAFT_05"
		slot_size = large
		slot_type = strike_craft
		locatorname = "large_gun_02"
	}
	component_slot = {
		name = "STRIKE_CRAFT_06"
		slot_size = large
		slot_type = strike_craft
		locatorname = "large_gun_03"
	}
	component_slot = {
		name = "STRIKE_CRAFT_07"
		slot_size = large
		slot_type = strike_craft
		locatorname = "large_gun_03"
	}
	component_slot = {
		name = "STRIKE_CRAFT_08"
		slot_size = large
		slot_type = strike_craft
		locatorname = "large_gun_01"
	}
	component_slot = {
		name = "STRIKE_CRAFT_09"
		slot_size = large
		slot_type = strike_craft
		locatorname = "large_gun_02"
	}
	component_slot = {
		name = "STRIKE_CRAFT_10"
		slot_size = large
		slot_type = strike_craft
		locatorname = "large_gun_03"
	}

	large_utility_slots = 18
	aux_utility_slots = 5
}

#	--------------------------- --------------------------- ---------------------------
#	Hivemind FE - Escort
#	--------------------------- --------------------------- ---------------------------

ship_section_template = {
	key = "FEE_hivemind_FE_escort_key"
	ship_size = FEE_hivemind_FE_escort
	fits_on_slot = mid
	entity = "warship_small_entity"
	icon = "GFX_ship_part_core_mid"

	component_slot = {
		name = "LARGE_GUN_01"
		slot_size = large
		slot_type = weapon
		locatorname = "root"
	}
	component_slot = {
		name = "MEDIUM_GUN_01"
		slot_size = medium
		slot_type = weapon
		locatorname = "medium_gun_01"
		is_side_slot = yes
	}
	component_slot = {
		name = "MEDIUM_GUN_02"
		slot_size = medium
		slot_type = weapon
		locatorname = "medium_gun_02"
		is_side_slot = yes
	}
	component_slot = {
		name = "MEDIUM_GUN_03"
		slot_size = medium
		slot_type = weapon
		locatorname = "medium_gun_03"
		is_side_slot = yes
	}

	medium_utility_slots = 8
	aux_utility_slots = 3
}

#	--------------------------- --------------------------- ---------------------------
#	Hivemind FE - Fortress
#	--------------------------- --------------------------- ---------------------------

ship_section_template = {
	key = "FEE_hivemind_FE_fortress_key"
	ship_size = FEE_hivemind_FE_fortress
	fits_on_slot = mid
	entity = "empty_section_entity"
	icon = "GFX_ship_part_core_mid"

	component_slot = {
		name = "EXTRA_LARGE_01"
		slot_size = extra_large
		slot_type = weapon
		locatorname = "root"
	}
	component_slot = {
		name = "EXTRA_LARGE_02"
		slot_size = extra_large
		slot_type = weapon
		locatorname = "root"
	}
	component_slot = {
		name = "EXTRA_LARGE_03"
		slot_size = extra_large
		slot_type = weapon
		locatorname = "root"
	}
	component_slot = {
		name = "EXTRA_LARGE_04"
		slot_size = extra_large
		slot_type = weapon
		locatorname = "root"
	}
	component_slot = {
		name = "LARGE_GUN_01"
		slot_size = large
		slot_type = weapon
		locatorname = "root"
	}
	component_slot = {
		name = "LARGE_GUN_02"
		slot_size = large
		slot_type = weapon
		locatorname = "root"
	}
	component_slot = {
		name = "LARGE_GUN_03"
		slot_size = large
		slot_type = weapon
		locatorname = "root"
	}
	component_slot = {
		name = "LARGE_GUN_04"
		slot_size = large
		slot_type = weapon
		locatorname = "root"
	}
	component_slot = {
		name = "LARGE_GUN_05"
		slot_size = large
		slot_type = weapon
		locatorname = "root"
	}
	component_slot = {
		name = "LARGE_GUN_06"
		slot_size = large
		slot_type = weapon
		locatorname = "root"
	}
	component_slot = {
		name = "MEDIUM_GUN_01"
		slot_size = medium
		slot_type = weapon
		locatorname = "root"
	}
	component_slot = {
		name = "MEDIUM_GUN_02"
		slot_size = medium
		slot_type = weapon
		locatorname = "root"
	}
	component_slot = {
		name = "MEDIUM_GUN_03"
		slot_size = medium
		slot_type = weapon
		locatorname = "root"
	}
	component_slot = {
		name = "MEDIUM_GUN_04"
		slot_size = medium
		slot_type = weapon
		locatorname = "root"
	}
	component_slot = {
		name = "MEDIUM_GUN_05"
		slot_size = medium
		slot_type = weapon
		locatorname = "root"
	}
	component_slot = {
		name = "MEDIUM_GUN_06"
		slot_size = medium
		slot_type = weapon
		locatorname = "root"
	}
	component_slot = {
		name = "MEDIUM_GUN_07"
		slot_size = medium
		slot_type = weapon
		locatorname = "root"
	}
	component_slot = {
		name = "MEDIUM_GUN_08"
		slot_size = medium
		slot_type = weapon
		locatorname = "root"
	}
	component_slot = {
		name = "MEDIUM_GUN_09"
		slot_size = medium
		slot_type = weapon
		locatorname = "root"
	}
	component_slot = {
		name = "MEDIUM_GUN_10"
		slot_size = medium
		slot_type = weapon
		locatorname = "root"
	}
	component_slot = {
		name = "MEDIUM_GUN_11"
		slot_size = medium
		slot_type = weapon
		locatorname = "root"
	}
	component_slot = {
		name = "MEDIUM_GUN_12"
		slot_size = medium
		slot_type = weapon
		locatorname = "root"
	}
	component_slot = {
		name = "MEDIUM_GUN_13"
		slot_size = medium
		slot_type = weapon
		locatorname = "root"
	}
	component_slot = {
		name = "MEDIUM_GUN_14"
		slot_size = medium
		slot_type = weapon
		locatorname = "root"
	}

	large_utility_slots = 30
	aux_utility_slots = 4
}

#	--------------------------- --------------------------- ---------------------------
#	Hivemind FE - Outpost
#	--------------------------- --------------------------- ---------------------------

ship_section_template = {
	key = "FEE_hivemind_FE_outpost_key"
	ship_size = FEE_hivemind_FE_outpost
	fits_on_slot = mid
	entity = "empty_section_entity"
	icon = "GFX_ship_part_core_mid"

	component_slot = {
		name = "EXTRA_LARGE_01"
		slot_size = extra_large
		slot_type = weapon
		locatorname = "root"
	}
	component_slot = {
		name = "EXTRA_LARGE_02"
		slot_size = extra_large
		slot_type = weapon
		locatorname = "root"
	}
	component_slot = {
		name = "LARGE_GUN_01"
		slot_size = large
		slot_type = weapon
		locatorname = "root"
	}
	component_slot = {
		name = "LARGE_GUN_02"
		slot_size = large
		slot_type = weapon
		locatorname = "root"
	}
	component_slot = {
		name = "MEDIUM_GUN_01"
		slot_size = medium
		slot_type = weapon
		locatorname = "root"
	}
	component_slot = {
		name = "MEDIUM_GUN_02"
		slot_size = medium
		slot_type = weapon
		locatorname = "root"
	}
	component_slot = {
		name = "MEDIUM_GUN_03"
		slot_size = medium
		slot_type = weapon
		locatorname = "root"
	}
	component_slot = {
		name = "MEDIUM_GUN_04"
		slot_size = medium
		slot_type = weapon
		locatorname = "root"
	}
	component_slot = {
		name = "MEDIUM_GUN_05"
		slot_size = medium
		slot_type = weapon
		locatorname = "root"
	}
	component_slot = {
		name = "MEDIUM_GUN_06"
		slot_size = medium
		slot_type = weapon
		locatorname = "root"
	}
	component_slot = {
		name = "MEDIUM_GUN_07"
		slot_size = medium
		slot_type = weapon
		locatorname = "root"
	}
	component_slot = {
		name = "MEDIUM_GUN_08"
		slot_size = medium
		slot_type = weapon
		locatorname = "root"
	}
	large_utility_slots = 15
	aux_utility_slots = 2
}
