#--
#--		--------------------------- --------------------------- --------------------------- ---------------------------
#--		Fallen Empire Expanded - System Initializer for "Silent Nest" - Hive Mind FE
#--		--------------------------- --------------------------- --------------------------- ---------------------------
#--
#--		PREFACE
#--		?	This file allows the generation of homeworld systems, supplementing existing templates of fallen empires systems.
#--		?	Homeworlds are the capital systems of fallen empires.
#--		?	This file generates its specified homeworld, as mentioned in the title.
#--
#--		--------------------------- --------------------------- --------------------------- ---------------------------
#--

FEE_fallen_hivemind = {
	usage = fallen_empire_init
	name = "NAME_Arcturus_Badlands"
	class = "FEE_sc_rogue_planet"

	flags = { FEE_silent_nest }

	usage_odds = {
		base = 100
		modifier = {
			factor = 0
			is_fe_cluster = yes
		}
	}
	
	max_instances = 1
	
	planet = {
		name = "NAME_Calescent_Nest"
		class = "FEE_pc_fractured_planet"
		entity = "barren_planet_01_destroyed_entity"
		orbit_distance = 0.01
		orbit_angle = { min = 1 max = 360 }
		size = 120
		tile_blockers = none
		modifiers = none
		has_ring = no

		init_effect = {

			set_planet_flag = FEE_homeworld_silent_nest_capital_flag
			save_global_event_target_as = FEE_homeworld_silent_nest_capital_target

			create_cluster = {
				id = fe4_cluster
				radius = 100
				center = this.solar_system
			}
				
			add_modifier = {
				modifier = FEE_hive_arcology
				days = -1
			}

			set_planet_flag = fallen_empire_world

			prevent_anomaly = yes
			set_owner = ROOT
			set_capital = yes

			random_tile = {
				limit = { has_building = no has_blocker = no num_adjacent_tiles > 3 }
				set_building = "building_capital_3"
				set_deposit = d_rich_food_mineral_deposit
			}
			random_tile = {
				limit = {
					has_building = no
					has_blocker = no
				}
				set_building = "building_mem_planetary_shield_generator_heavy"
			}
			random_tile = {
				limit = { has_building = no has_blocker = no }
				set_building = "building_agri_processing_complex"
				set_deposit = d_immense_farmland_deposit
			}
			random_tile = {
				limit = { has_building = no has_blocker = no }
				set_building = "building_agri_processing_complex"
				set_deposit = d_vast_farmland_deposit
			}
			random_tile = {
				limit = { has_building = no has_blocker = no }
				set_building = "building_dark_matter_power_plant"
				set_deposit = d_rich_mineral_energy_deposit
			}
			random_tile = {
				limit = { has_building = no has_blocker = no }
				set_building = "building_dark_matter_power_plant"
				set_deposit = d_rich_mineral_energy_deposit
			}
			random_tile = {
				limit = { has_building = no has_blocker = no }
				set_building = "building_dark_matter_power_plant"
				set_deposit = d_rich_mineral_energy_deposit
			}
			random_tile = {
				limit = { has_building = no has_blocker = no }
				set_building = "building_dark_matter_power_plant"
				set_deposit =d_rich_mineral_energy_deposit
			}
			random_tile = {
				limit = { has_building = no has_blocker = no }
				set_building = "building_dark_matter_power_plant"
				set_deposit = d_rich_mineral_energy_deposit
			}
			random_tile = {
				limit = { has_building = no has_blocker = no }
				set_building = "building_dark_matter_power_plant"
				set_deposit = d_vast_energy_deposit
			}
			random_tile = {
				limit = { has_building = no has_blocker = no }
				set_building = "building_dark_matter_power_plant"
				set_deposit = d_rich_energy_deposit
			}
			random_tile = {
				limit = { has_building = no has_blocker = no }
				set_building = "building_dark_matter_power_plant"
				set_deposit = d_rich_energy_deposit
			}
			random_tile = {
				limit = { has_building = no has_blocker = no }
				set_building = "building_dark_matter_power_plant"
				set_deposit = d_energy_deposit
			}
			random_tile = {
				limit = { has_building = no has_blocker = no }
				set_building = "building_dark_matter_power_plant"
				set_deposit = d_energy_deposit
			}
			random_tile = {
				limit = { has_building = no has_blocker = no }
				set_building = "building_dark_matter_power_plant"
				set_deposit = d_energy_deposit
			}
			random_tile = {
				limit = { has_building = no has_blocker = no }
				set_building = "building_dark_matter_power_plant"
				set_deposit = d_energy_deposit
			}
			random_tile = {
				limit = { has_building = no has_blocker = no }
				set_building = "building_dark_matter_power_plant"
				set_deposit = d_rich_energy_deposit
			}
			random_tile = {
				limit = { has_building = no has_blocker = no }
				set_building = "building_ancient_factory"
				set_deposit = d_rich_mineral_energy_deposit
			}
			random_tile = {
				limit = { has_building = no has_blocker = no }
				set_building = "building_ancient_factory"
				set_deposit = d_rich_food_mineral_deposit
			}
			random_tile = {
				limit = { has_building = no has_blocker = no }
				set_building = "building_ancient_factory"
				set_deposit = d_immense_mineral_deposit
			}
			random_tile = {
				limit = { has_building = no has_blocker = no }
				set_building = "building_ancient_factory"
				set_deposit = d_immense_mineral_deposit
			}
			random_tile = {
				limit = { has_building = no has_blocker = no }
				set_building = "building_ancient_factory"
				set_deposit = d_immense_mineral_deposit
			}
			random_tile = {
				limit = { has_building = no has_blocker = no }
				set_building = "building_ancient_factory"
				set_deposit = d_vast_mineral_deposit
			}
			random_tile = {
				limit = { has_building = no has_blocker = no }
				set_building = "building_ancient_factory"
				set_deposit = d_vast_mineral_deposit
			}
			random_tile = {
				limit = { has_building = no has_blocker = no }
				set_building = "building_ancient_factory"
				set_deposit = d_vast_mineral_deposit
			}
			every_tile = {
				limit = { has_pop = no }
				create_pop = {
					species = owner_main_species
					ethos = owner
				}
			}
			create_army = {
				name = "NAME_1st_Reavers"
				owner = ROOT
				species = ROOT
				type = "elite_guard_army"
			}
			create_army = {
				name = "NAME_2nd_Reavers"
				owner = ROOT
				species = ROOT
				type = "elite_guard_army"
			}
			create_army = {
				name = "NAME_3rd_Reavers"
				owner = ROOT
				species = ROOT
				type = "elite_guard_army"
			}
			create_army = {
				name = "NAME_4th_Reavers"
				owner = ROOT
				species = ROOT
				type = "elite_guard_army"
			}
			create_army = {
				name = "NAME_5th_Reavers"
				owner = ROOT
				species = ROOT
				type = "elite_guard_army"
			}
			create_army = {
				name = "NAME_6th_Reavers"
				owner = ROOT
				species = ROOT
				type = "elite_guard_army"
			}
			create_army = {
				name = random
				owner = ROOT
				species = ROOT
				type = "defense_army"
			}
			create_army = {
				name = random
				owner = ROOT
				species = ROOT
				type = "defense_army"
			}
			create_army = {
				name = random
				owner = ROOT
				species = ROOT
				type = "defense_army"
			}
			create_army = {
				name = random
				owner = ROOT
				species = ROOT
				type = "defense_army"
			}
			create_army = {
				name = random
				owner = ROOT
				species = ROOT
				type = "defense_army"
			}
			create_army = {
				name = random
				owner = ROOT
				species = ROOT
				type = "defense_army"
			}
			create_army = {
				name = random
				owner = ROOT
				species = ROOT
				type = "defense_army"
			}
			create_army = {
				name = random
				owner = ROOT
				species = ROOT
				type = "defense_army"
			}
			create_army = {
				name = random
				owner = ROOT
				species = ROOT
				type = "defense_army"
			}
			create_army = {
				name = random
				owner = ROOT
				species = ROOT
				type = "defense_army"
			}
			create_army = {
				name = random
				owner = ROOT
				species = ROOT
				type = "defense_army"
			}
			create_army = {
				name = random
				owner = ROOT
				species = ROOT
				type = "defense_army"
			}
			create_army = {
				name = random
				owner = ROOT
				species = ROOT
				type = "defense_army"
			}
			create_army = {
				name = random
				owner = ROOT
				species = ROOT
				type = "defense_army"
			}
			create_army = {
				name = random
				owner = ROOT
				species = ROOT
				type = "defense_army"
			}
			create_army = {
				name = random
				owner = ROOT
				species = ROOT
				type = "defense_army"
			}
			create_army = {
				name = random
				owner = ROOT
				species = ROOT
				type = "defense_army"
			}
			create_army = {
				name = random
				owner = ROOT
				species = ROOT
				type = "defense_army"
			}
			create_army = {
				name = random
				owner = ROOT
				species = ROOT
				type = "defense_army"
			}
			create_army = {
				name = random
				owner = ROOT
				species = ROOT
				type = "defense_army"
			}
			create_army = {
				name = random
				owner = ROOT
				species = ROOT
				type = "defense_army"
			}
			create_army = {
				name = random
				owner = ROOT
				species = ROOT
				type = "defense_army"
			}
			create_army = {
				name = random
				owner = ROOT
				species = ROOT
				type = "defense_army"
			}
			create_army = {
				name = random
				owner = ROOT
				species = ROOT
				type = "defense_army"
			}
			create_army = {
				name = random
				owner = ROOT
				species = ROOT
				type = "defense_army"
			}
			create_army = {
				name = random
				owner = ROOT
				species = ROOT
				type = "defense_army"
			}
			create_army = {
				name = random
				owner = ROOT
				species = ROOT
				type = "defense_army"
			}
			create_army = {
				name = random
				owner = ROOT
				species = ROOT
				type = "defense_army"
			}
			create_army = {
				name = random
				owner = ROOT
				species = ROOT
				type = "defense_army"
			}
			create_army = {
				name = random
				owner = ROOT
				species = ROOT
				type = "defense_army"
			}
			create_spaceport = {
				owner = ROOT
				initial_module = "swarm_weapon"
			}
			spaceport = {
				set_spaceport_level = 6
				set_spaceport_module = {
					slot = 1
					module = "crew_quarters"
				}
				set_spaceport_module = {
					slot = 2
					module = "fleet_academy"
				}
				set_spaceport_module = {
					slot = 3
					module = "synchronized_defenses"
				}
				set_spaceport_module = {
					slot = 4
					module = "engineering_bay"
				}
			}
			create_fallen_empire_starting_navy = yes
			create_fleet = {
				effect = {
					set_owner = ROOT
					create_fallen_empire_large_station = yes
					set_location = {
						target = PREV
						distance = 20
						angle = 90
					}
				}
			}
			create_fleet = {
				effect = {
					set_owner = ROOT
					create_fallen_empire_large_station = yes
					set_location = {
						target = PREV
						distance = 20
						angle = 180
					}
				}
			}
			create_fleet = {
				effect = {
					set_owner = ROOT
					create_fallen_empire_large_station = yes
					set_location = {
						target = PREV
						distance = 20
						angle = 270
					}
				}
			}
			create_fleet = {
				effect = {
					set_owner = ROOT
					create_fallen_empire_large_station = yes
					set_location = {
						target = PREV
						distance = 20
						angle = 0
					}
				}
			}
		}
	}

	init_effect = {
		create_ambient_object = {
			type = "large_debris_object"
			location = solar_system
		}
		create_ambient_object = {
			type = "large_debris_object"
			location = solar_system
		}
		create_ambient_object = {
			type = "large_debris_object"
			location = solar_system
		}
		create_ambient_object = {
			type = "large_debris_object"
			location = solar_system
		}
		create_ambient_object = {
			type = "large_debris_object"
			location = solar_system
		}
		create_ambient_object = {
			type = "large_debris_object"
			location = solar_system
		}
		create_ambient_object = {
			type = "medium_debris_01_object"
			location = solar_system
		}
		create_ambient_object = {
			type = "medium_debris_01_object"
			location = solar_system
		}
		create_ambient_object = {
			type = "medium_debris_02_object"
			location = solar_system
		}
		create_ambient_object = {
			type = "medium_debris_02_object"
			location = solar_system
		}
		create_ambient_object = {
			type = "small_debris_object"
			location = solar_system
		}
		create_ambient_object = {
			type = "small_debris_object"
			location = solar_system
		}
		
	}
}
