#--
#--		--------------------------- --------------------------- --------------------------- ---------------------------
#--		Fallen Empire Expanded - System Initializer for "Eternal Citadel" - Xenophobe FE
#--		--------------------------- --------------------------- --------------------------- ---------------------------
#--
#--		PREFACE
#--		�	This file allows the generation of homeworld systems, supplementing existing templates of fallen empires systems.
#--		�	Homeworlds are the capital systems of fallen empires.
#--		�	This file generates its specified homeworld, as mentioned in the title.
#--
#--		--------------------------- --------------------------- --------------------------- ---------------------------
#--

#	--------------------------- --------------------------- --------------------------- ---------------------------
#	"Eternal Citadel" - Xenophobe FE
#	--------------------------- --------------------------- --------------------------- ---------------------------

FEE_fallen_8 = {
	usage = fallen_empire_init
	class = "rl_standard_stars"
	asteroids_distance = 80

	flags = { FEE_eternal_citadel }

	planet = {
		count = 1
		class = star
		orbit_distance = 0
		orbit_angle = 1
		size = 80
		has_ring = no

		init_effect = {
			set_planet_flag = FEE_homeworld_eternal_citadel_star_flag
			save_global_event_target_as = FEE_homeworld_eternal_citadel_star_target
		}
	}

	change_orbit = 80

	planet = {
		count = 6
		class = random_asteroid
		orbit_distance = { min = 0 max = 1 }
		orbit_angle = { min = 90 max = 270 }
		size = { min = 1 max = 3 }
	}
	change_orbit = 30

	planet = {
		count = 1
		orbit_distance = 20
		class = pc_molten
		orbit_angle = { min = 90 max = 270 }
	}

	change_orbit = 120

	planet = {
		name = "NAME_The_Casualty"
		class = "pc_barren"
		entity = "barren_planet_01_destroyed_entity"
		orbit_angle = { min = 90 max = 270 }
		orbit_distance = { min = 5 max = 10 }
		size = 70
		modifiers = none
		has_ring = no

		init_effect = {
			set_planet_flag = FEE_homeworld_eternal_citadel_casualty_flag
			save_global_event_target_as = FEE_homeworld_eternal_citadel_casualty_target

			prevent_anomaly = yes
			add_modifier = {
				modifier = "ancient_weapon"
				days = -1
			}
			create_ambient_object = {
				type = "large_debris_object"
				location = THIS
			}
			create_ambient_object = {
				type = "abandoned_starbase_01_object"
			}
			last_created_ambient_object = {
				set_location = {
					target = THIS
					distance = 5
					angle = random
				}
			}
		}

		moon = {
			name = "NAME_Eternal_Citadel"
			orbit_distance = 20
			class = pc_gaia
			orbit_distance = 50
			orbit_angle = { min = 90 max = 270 }
			size = 25
			tile_blockers = none
			modifiers = none
			has_ring = no

			init_effect = {
				set_planet_flag = FEE_homeworld_eternal_citadel_capital_flag
				save_global_event_target_as = FEE_homeworld_eternal_citadel_capital_target

				create_cluster = {
					id = fe4_cluster
					radius = 100
					center = this.solar_system
				}

				set_planet_flag = fallen_empire_world
				prevent_anomaly = yes
				set_owner = ROOT
				set_capital = yes

				random_tile = {
					limit = { has_building = no has_blocker = no num_adjacent_tiles > 3 }
					set_building = "building_capital_3"
					set_deposit = d_rich_food_mineral_deposit
				}
				random_tile = {
					limit = {
						has_building = no
						has_blocker = no
					}
					set_building = "building_mem_planetary_shield_generator_heavy"
				}
				random_tile = {
					limit = { has_building = no has_blocker = no }
					set_building = "building_agri_processing_complex"
					set_deposit = d_rich_farmland_deposit
				}
				random_tile = {
					limit = { has_building = no has_blocker = no }
					set_building = "building_dark_matter_power_plant"
					set_deposit = d_energy_deposit
				}
				random_tile = {
					limit = { has_building = no has_blocker = no }
					set_building = "building_dark_matter_power_plant"
					set_deposit = d_energy_deposit
				}
				random_tile = {
					limit = { has_building = no has_blocker = no }
					set_building = "building_dark_matter_power_plant"
					set_deposit = d_energy_deposit
				}
				random_tile = {
					limit = { has_building = no has_blocker = no }
					set_building = "building_dark_matter_power_plant"
					set_deposit = d_energy_deposit
				}
				random_tile = {
					limit = { has_building = no has_blocker = no }
					set_building = "building_dark_matter_power_plant"
					set_deposit = d_energy_deposit
				}
				random_tile = {
					limit = { has_building = no has_blocker = no }
					set_building = "building_dark_matter_power_plant"
					set_deposit = d_energy_deposit
				}
				random_tile = {
					limit = { has_building = no has_blocker = no }
					set_building = "building_dark_matter_power_plant"
					set_deposit = d_energy_deposit
				}
				random_tile = {
					limit = { has_building = no has_blocker = no }
					set_building = "building_dark_matter_power_plant"
					set_deposit = d_energy_deposit
				}
				random_tile = {
					limit = { has_building = no has_blocker = no }
					set_building = "building_dark_matter_power_plant"
					set_deposit = d_energy_deposit
				}
				random_tile = {
					limit = { has_building = no has_blocker = no }
					set_building = "building_dark_matter_power_plant"
					set_deposit = d_energy_deposit
				}
				random_tile = {
					limit = { has_building = no has_blocker = no }
					set_building = "building_dark_matter_power_plant"
					set_deposit = d_energy_deposit
				}
				random_tile = {
					limit = { has_building = no has_blocker = no }
					set_building = "building_dark_matter_power_plant"
					set_deposit = d_energy_deposit
				}
				random_tile = {
					limit = { has_building = no has_blocker = no }
					set_building = "building_dark_matter_power_plant"
					set_deposit = d_energy_deposit
				}
				random_tile = {
					limit = { has_building = no has_blocker = no }
					set_building = "building_dark_matter_power_plant"
					set_deposit = d_energy_deposit
				}
				random_tile = {
					limit = { has_building = no has_blocker = no }
					set_building = "building_dark_matter_power_plant"
					set_deposit = d_energy_deposit
				}
				random_tile = {
					limit = { has_building = no has_blocker = no }
					set_building = "building_ancient_factory"
					set_deposit = d_mineral_deposit
				}
				random_tile = {
					limit = { has_building = no has_blocker = no }
					set_building = "building_ancient_factory"
					set_deposit = d_mineral_deposit
				}
				create_pop = {
					species = owner_main_species
					ethos = owner
				}
				create_pop = {
					species = owner_main_species
					ethos = owner
				}
				create_pop = {
					species = owner_main_species
					ethos = owner
				}
				create_pop = {
					species = owner_main_species
					ethos = owner
				}
				create_pop = {
					species = owner_main_species
					ethos = owner
				}
				create_pop = {
					species = owner_main_species
					ethos = owner
				}
				create_pop = {
					species = owner_main_species
					ethos = owner
				}
				create_pop = {
					species = owner_main_species
					ethos = owner
				}
				create_pop = {
					species = owner_main_species
					ethos = owner
				}
				create_pop = {
					species = owner_main_species
					ethos = owner
				}
				create_pop = {
					species = owner_main_species
					ethos = owner
				}
				create_pop = {
					species = owner_main_species
					ethos = owner
				}
				create_pop = {
					species = owner_main_species
					ethos = owner
				}
				create_pop = {
					species = owner_main_species
					ethos = owner
				}
				create_pop = {
					species = owner_main_species
					ethos = owner
				}
				create_pop = {
					species = ROBOT_POP_SPECIES_3
					ethos = owner
				}
				create_pop = {
					species = ROBOT_POP_SPECIES_3
					ethos = owner
				}
				create_pop = {
					species = ROBOT_POP_SPECIES_3
					ethos = owner
				}
				create_pop = {
					species = ROBOT_POP_SPECIES_3
					ethos = owner
				}
				create_pop = {
					species = ROBOT_POP_SPECIES_3
					ethos = owner
				}
				every_tile = {
					limit = { has_building = no has_blocker = no has_pop = no }
					set_blocker = tb_ancient_ruins
				}
				create_army = {
					name = random
					owner = ROOT
					species = ROOT
					type = "android_army"
				}
				create_army = {
					name = random
					owner = ROOT
					species = ROOT
					type = "android_army"
				}
				create_army = {
					name = random
					owner = ROOT
					species = ROOT
					type = "android_army"
				}
				create_army = {
					name = random
					owner = ROOT
					species = ROOT
					type = "android_army"
				}
				create_army = {
					name = random
					owner = ROOT
					species = ROOT
					type = "android_army"
				}
				create_army = {
					name = random
					owner = ROOT
					species = ROOT
					type = "android_army"
				}
				create_army = {
					name = random
					owner = ROOT
					species = ROOT
					type = "defense_army"
				}
				create_army = {
					name = random
					owner = ROOT
					species = ROOT
					type = "defense_army"
				}
				create_army = {
					name = random
					owner = ROOT
					species = ROOT
					type = "defense_army"
				}
				create_army = {
					name = random
					owner = ROOT
					species = ROOT
					type = "defense_army"
				}
				create_army = {
					name = random
					owner = ROOT
					species = ROOT
					type = "defense_army"
				}
				create_army = {
					name = random
					owner = ROOT
					species = ROOT
					type = "defense_army"
				}
				create_spaceport = {
					owner = ROOT
					initial_module = "fallen_empire_weapon"
				}
				spaceport = {
					set_spaceport_level = 6
					set_spaceport_module = {
						slot = 1
						module = "crew_quarters"
					}
					set_spaceport_module = {
						slot = 2
						module = "fleet_academy"
					}
					set_spaceport_module = {
						slot = 3
						module = "synchronized_defenses"
					}
					set_spaceport_module = {
						slot = 4
						module = "engineering_bay"
					}
				}
				create_fleet = {
					effect = {
						set_owner = ROOT
						create_ship = {
							name = random
							design = "NAME_Reaper"
							graphical_culture = root
						}
						set_location = {
							target = PREV
							distance = 20
							angle = 90
						}
					}
				}
				create_fleet = {
					effect = {
						set_owner = ROOT
						create_ship = {
							name = random
							design = "NAME_Reaper"
							graphical_culture = root
						}
						set_location = {
							target = PREV
							distance = 20
							angle = 180
						}
					}
				}
				create_fleet = {
					effect = {
						set_owner = ROOT
						create_ship = {
							name = random
							design = "NAME_Reaper"
							graphical_culture = root
						}
						set_location = {
							target = PREV
							distance = 20
							angle = 270
						}
					}
				}
				create_fleet = {
					effect = {
						set_owner = ROOT
						create_ship = {
							name = random
							design = "NAME_Reaper"
							graphical_culture = root
						}
						set_location = {
							target = PREV
							distance = 20
							angle = 0
						}
					}
				}
				create_fallen_empire_starting_navy = yes
			}
		}
	}

	change_orbit = 70

	planet = {
		count = 1
		name = "NAME_War_Foundry"
		class = ideal_planet_class
		orbit_distance = 40
		orbit_angle = 120
		size = 25
		tile_blockers = none
		modifiers = none
		has_ring = no

		init_effect = {
			set_planet_flag = FEE_homeworld_eternal_citadel_foundry_flag
			save_global_event_target_as = FEE_homeworld_eternal_citadel_foundry_target

			set_planet_flag = fallen_empire_world
			prevent_anomaly = yes
			set_owner = ROOT

			add_modifier = {
				modifier = "ancient_forgeworld"
				days = -1
			}

			add_modifier = {
				modifier = "ultra_rich"
				days = -1
			}

			random_tile = {
				limit = { has_building = no has_blocker = no num_adjacent_tiles > 3 }
				set_building = "building_capital_2"
				set_deposit = d_rich_food_mineral_deposit
			}
			random_tile = {
				limit = { has_building = no has_blocker = no }
				set_building = "building_agri_processing_complex"
				set_deposit = d_rich_farmland_deposit
			}
			random_tile = {
				limit = { has_building = no has_blocker = no }
				set_building = "building_ancient_factory"
				set_deposit = d_rich_mineral_deposit
			}
			random_tile = {
				limit = { has_building = no has_blocker = no }
				set_building = "building_ancient_factory"
				set_deposit = d_rich_mineral_deposit
			}
			random_tile = {
				limit = { has_building = no has_blocker = no }
				set_building = "building_ancient_factory"
				set_deposit = d_rich_mineral_deposit
			}
			random_tile = {
				limit = { has_building = no has_blocker = no }
				set_building = "building_ancient_factory"
				set_deposit = d_rich_mineral_deposit
			}
			random_tile = {
				limit = { has_building = no has_blocker = no }
				set_building = "building_ancient_factory"
				set_deposit = d_rich_mineral_deposit
			}
			random_tile = {
				limit = { has_building = no has_blocker = no }
				set_building = "building_ancient_factory"
				set_deposit = d_rich_mineral_deposit
			}
			random_tile = {
				limit = { has_building = no has_blocker = no }
				set_building = "building_ancient_factory"
				set_deposit = d_rich_mineral_deposit
			}
			random_tile = {
				limit = { has_building = no has_blocker = no }
				set_building = "building_ancient_factory"
				set_deposit = d_rich_mineral_deposit
			}
			random_tile = {
				limit = { has_building = no has_blocker = no }
				set_building = "building_ancient_factory"
				set_deposit = d_rich_mineral_deposit
			}
			random_tile = {
				limit = { has_building = no has_blocker = no }
				set_building = "building_ancient_factory"
				set_deposit = d_rich_mineral_deposit
			}
			random_tile = {
				limit = { has_building = no has_blocker = no }
				set_building = "building_ancient_factory"
				set_deposit = d_rich_mineral_deposit
			}
			random_tile = {
				limit = { has_building = no has_blocker = no }
				set_building = "building_ancient_factory"
				set_deposit = d_rich_mineral_deposit
			}
			random_tile = {
				limit = { has_building = no has_blocker = no }
				set_building = "building_ancient_factory"
				set_deposit = d_rich_mineral_deposit
			}
			random_tile = {
				limit = { has_building = no has_blocker = no }
				set_building = "building_ancient_factory"
				set_deposit = d_rich_mineral_deposit
			}
			random_tile = {
				limit = { has_building = no has_blocker = no }
				set_building = "building_ancient_factory"
				set_deposit = d_rich_mineral_deposit
			}
			random_tile = {
				limit = { has_building = no has_blocker = no }
				set_building = "building_ancient_factory"
				set_deposit = d_rich_mineral_deposit
			}
			create_pop = {
				species = owner_main_species
				ethos = owner
			}
			create_pop = {
				species = owner_main_species
				ethos = owner
			}
			create_pop = {
				species = owner_main_species
				ethos = owner
			}
			create_pop = {
				species = owner_main_species
				ethos = owner
			}
			create_pop = {
				species = owner_main_species
				ethos = owner
			}
			create_pop = {
				species = owner_main_species
				ethos = owner
			}
			create_pop = {
				species = owner_main_species
				ethos = owner
			}
			create_pop = {
				species = owner_main_species
				ethos = owner
			}
			create_pop = {
				species = owner_main_species
				ethos = owner
			}
			create_pop = {
				species = owner_main_species
				ethos = owner
			}
			create_pop = {
				species = owner_main_species
				ethos = owner
			}
			create_pop = {
				species = owner_main_species
				ethos = owner
			}
			create_pop = {
				species = owner_main_species
				ethos = owner
			}
			create_pop = {
				species = owner_main_species
				ethos = owner
			}
			create_pop = {
				species = owner_main_species
				ethos = owner
			}
			create_pop = {
				species = owner_main_species
				ethos = owner
			}
			create_pop = {
				species = owner_main_species
				ethos = owner
			}
			create_pop = {
				species = owner_main_species
				ethos = owner
			}
			every_tile = {
				limit = { has_building = no has_blocker = no has_pop = no }
				set_blocker = tb_ancient_ruins
			}
			create_army = {
				name = random
				owner = ROOT
				species = ROOT
				type = "defense_army"
			}
			create_army = {
				name = random
				owner = ROOT
				species = ROOT
				type = "defense_army"
			}
			create_army = {
				name = random
				owner = ROOT
				species = ROOT
				type = "defense_army"
			}
			create_army = {
				name = random
				owner = ROOT
				species = ROOT
				type = "defense_army"
			}
			create_spaceport = {
				owner = ROOT
				initial_module = "fallen_empire_weapon"
			}
			spaceport = {
				set_spaceport_level = 6
				set_spaceport_module = {
					slot = 1
					module = "crew_quarters"
				}
				set_spaceport_module = {
					slot = 2
					module = "fleet_academy"
				}
				set_spaceport_module = {
					slot = 3
					module = "synchronized_defenses"
				}
				set_spaceport_module = {
					slot = 4
					module = "engineering_bay"
				}
			}
			create_fleet = {
				effect = {
					set_owner = ROOT
					create_ship = {
						name = random
						design = "NAME_Reaper"
						graphical_culture = root
					}
					set_location = {
						target = PREV
						distance = 20
						angle = 90
					}
				}
			}
			create_fleet = {
				effect = {
					set_owner = ROOT
					create_ship = {
						name = random
						design = "NAME_Reaper"
						graphical_culture = root
					}
					set_location = {
						target = PREV
						distance = 20
						angle = 180
					}
				}
			}
			create_fleet = {
				effect = {
					set_owner = ROOT
					create_ship = {
						name = random
						design = "NAME_Reaper"
						graphical_culture = root
					}
					set_location = {
						target = PREV
						distance = 20
						angle = 270
					}
				}
			}
			create_fleet = {
				effect = {
					set_owner = ROOT
					create_ship = {
						name = random
						design = "NAME_Reaper"
						graphical_culture = root
					}
					set_location = {
						target = PREV
						distance = 20
						angle = 0
					}
				}
			}
		}
	}

	planet = {
		class = random_non_colonizable
		orbit_distance = { min = 25 max = 75 }
		orbit_angle = { min = 90 max = 270 }

		change_orbit = 15

		moon = {
			count = { min = 0 max = 1 }
			orbit_angle = { min = 90 max = 270 }
			orbit_distance = { min = 5 max = 10 }
		}
	}

	planet = {
		class = pc_barren_cold
		orbit_distance = { min = 25 max = 75 }
		orbit_angle = { min = 90 max = 270 }

		change_orbit = 15

		moon = {
			count = { min = 0 max = 1 }
			orbit_angle = { min = 90 max = 270 }
			orbit_distance = { min = 5 max = 10 }
		}
	}

	change_orbit = 50

	planet = {
		class = pc_gas_giant
		orbit_distance = 25
		orbit_angle = { min = 90 max = 270 }
		size = 35

		change_orbit = 15

		moon = {
			count = { min = 2 max = 4 }
			orbit_angle = { min = 90 max = 270 }
			orbit_distance = { min = 5 max = 10 }
		}
	}

	change_orbit = 50

	planet = {
		class = pc_frozen
		orbit_distance = { min = 25 max = 75 }
		orbit_angle = { min = 90 max = 270 }

		change_orbit = 15

		moon = {
			count = { min = 0 max = 1 }
			orbit_angle = { min = 90 max = 270 }
			orbit_distance = { min = 5 max = 10 }
		}
	}

	init_effect = {
		random_system_planet = {
			limit = { has_deposit_for = shipclass_mining_station }
			create_mining_station = {
				owner = ROOT
			}
			create_fleet = {
				effect = {
					set_owner = ROOT
					create_fallen_empire_small_station = yes
					set_location = {
						target = PREV
						distance = 20
						angle = 90
					}
				}
			}
		}
		random_system_planet = {
			limit = {
				has_deposit_for = shipclass_mining_station
				has_mining_station = no
			}
			create_mining_station = {
				owner = ROOT
			}
		}
		random_system_planet = {
			limit = { has_deposit_for = shipclass_research_station }
			create_research_station = {
				owner = ROOT
			}
			create_fleet = {
				effect = {
					set_owner = ROOT
					create_fallen_empire_small_station = yes
					set_location = {
						target = PREV
						distance = 20
						angle = 90
					}
				}
			}
		}
		random_system_planet = {
			limit = {
				has_deposit_for = shipclass_research_station
				has_research_station = no
			}
			create_research_station = {
				owner = ROOT
			}
		}
	}
	neighbor_system = {
		distance = { min = 0 max = 100 }
		initializer = "FEE_fallen_col_3"
	}
	neighbor_system = {
		distance = { min = 0 max = 100 }
		initializer = "FEE_fallen_col_5"
	}
	neighbor_system = {
		distance = { min = 0 max = 200 }
		initializer = "FEE_fallen_necropolis"
	}
	neighbor_system = {
		distance = { min = 0 max = 50 }
		initializer = "FEE_fallen_shielded_4"
	}

}
