#########################
### TERRAFORMING COST ###
#########################

@easy_cost = 2000			#on-class terraforming
@medium_cost = 4000			#adjacent terraforming
@hard_cost = 8000			#tomb world, barren/hothouse, exotic, and 2x adjacent
@difficult_cost = 12000		#gaia world, machine world, toxic, frozen, molten
@very_difficult_cost = 16000#uninhabitable to machine

#########################
### TERRAFORMING TIME ###
#########################
							#Base Colony Dev Speed = 1800 (5yr)
@easy_time = 1800 			#(5yr)
@medium_time = 2700 		#(7.5yr)
@hard_time = 5400 			#(10yr)
@difficult_time = 7200 		#(15yr)
@very_difficult_time = 9900 #(17.5yr)




####################
## TO: Hive World ##
####################{

### Exotic & Special

#Barren (Hot)
terraform_link = {
	to = "pc_hiveworld"
	from = "pc_barren"
	
	energy = @very_difficult_cost
	duration = @very_difficult_time
	
	potential = {
		has_ascension_perk = ap_hive_worlds
		from = { 
			OR = {
				has_modifier = terraforming_candidate 
				has_modifier = terraforming_candidate_precursor
			}
		}
	}
	
	effect = {
		from = { 
			remove_modifier = terraforming_candidate 
			remove_modifier = terraforming_candidate_precursor 
			add_modifier = {
				modifier = "mod_pc_hiveworld_0"
				days = -1
			}
		}
	}
	
	ai_weight = {
		weight = 10
	}
}

#Barren (Cold)
terraform_link = {
	to = "pc_hiveworld"
	from = "pc_barren_cold"
	
	energy = @very_difficult_cost
	duration = @very_difficult_time
	
	potential = {
		has_ascension_perk = ap_hive_worlds
		from = { 
			OR = {
				has_modifier = terraforming_candidate 
				has_modifier = terraforming_candidate_precursor
			}
		}
	}
	
	effect = {
		from = { 
			remove_modifier = terraforming_candidate 
			remove_modifier = terraforming_candidate_precursor 
			add_modifier = {
				modifier = "mod_pc_hiveworld_0"
				days = -1
			}
		}
	}
	
	ai_weight = {
		weight = 10
	}
}

#Hothouse
terraform_link = {
	to = "pc_hiveworld"
	from = "pc_hothouse"
	
	energy = @very_difficult_cost
	duration = @very_difficult_time
	
	potential = {
		has_ascension_perk = ap_hive_worlds
		from = { 
			OR = {
				has_modifier = terraforming_candidate 
				has_modifier = terraforming_candidate_precursor
			}
		}
	}
	
	effect = {
		from = { 
			remove_modifier = terraforming_candidate 
			remove_modifier = terraforming_candidate_precursor 
			add_modifier = {
				modifier = "mod_pc_hiveworld_0"
				days = -1
			}
		}
	}
	
	ai_weight = {
		weight = 10
	}
}

#Toxic World
terraform_link = {
	to = "pc_hiveworld"
	from = "pc_toxic"
	
	energy = @very_difficult_cost
	duration = @very_difficult_time
	
	potential = {
		from = { 
			has_modifier = terraforming_candidate_precursor 
		}
		has_ascension_perk = ap_hive_worlds
	}
	
	effect = {
		from = { remove_modifier = terraforming_candidate_precursor }
		from = { 
			add_modifier = {
				modifier = "mod_pc_hiveworld_0"
				days = -1
			}
		}
	}
	
	ai_weight = {
		weight = 10
	}
}

#Molten World
terraform_link = {
	to = "pc_hiveworld"
	from = "pc_molten"
	
	energy = @very_difficult_cost
	duration = @very_difficult_time
	
	potential = {
		from = { 
			has_modifier = terraforming_candidate_precursor 
		}
		has_ascension_perk = ap_hive_worlds
	}
	
	effect = {
		from = { remove_modifier = terraforming_candidate_precursor }
		from = { 
			add_modifier = {
				modifier = "mod_pc_hiveworld_0"
				days = -1
			}
		}
	}
	
	ai_weight = {
		weight = 10
	}
}

#Frozen World
terraform_link = {
	to = "pc_hiveworld"
	from = "pc_frozen"
	
	energy = @very_difficult_cost
	duration = @very_difficult_time
	
	potential = {
		from = { 
			has_modifier = terraforming_candidate_precursor 
		}
		has_ascension_perk = ap_hive_worlds
	}
	
	effect = {
		from = { remove_modifier = terraforming_candidate_precursor }
		from = { 
			add_modifier = {
				modifier = "mod_pc_hiveworld_0"
				days = -1
			}
		}
	}
	
	ai_weight = {
		weight = 10
	}
}

#Tomb
terraform_link = {
	to = "pc_hiveworld"
	from = "pc_hiveworld"
	
	energy = @very_difficult_cost
	duration = @very_difficult_time
	
	potential = {
		has_ascension_perk = ap_hive_worlds
	}
	
	effect = {
		from = { 
			add_modifier = {
				modifier = "mod_pc_hiveworld_0"
				days = -1
			}
		}
	}
	
	ai_weight = {
		weight = 10
	}
}

#Gaia
terraform_link = {
	to = "pc_hiveworld"
	from = "pc_gaia"
	
	energy = @hard_cost
	duration = @hard_time
	
	potential = {
		has_ascension_perk = ap_hive_worlds
	}
	
	condition = { 
		from = {
			OR = {
				is_owned_by = root
				NOT = { has_modifier = "holy_planet" }
			}		
		}
	}
	
	effect = {
		from = { 
			add_modifier = {
				modifier = "mod_pc_hiveworld_0"
				days = -1
			}
		}
	}
	
	ai_weight = {
		weight = 5
	}
}

#Ammonia
terraform_link = {
	to = "pc_hiveworld"
	from = "pc_ammonia"
	
	energy = @hard_cost
	duration = @hard_time
	
	potential = {
		has_ascension_perk = ap_hive_worlds
	}
	
	effect = {
		from = { 
			add_modifier = {
				modifier = "mod_pc_hiveworld_0"
				days = -1
			}
		}
	}
	
	ai_weight = {
		weight = 10
		modifier = {
			factor = 0
			from = {
				is_colony = no
			}
		}
	}
}

#Methane
terraform_link = {
	to = "pc_hiveworld"
	from = "pc_methane"
	
	energy = @hard_cost
	duration = @hard_time
	
	potential = {
		has_ascension_perk = ap_hive_worlds
	}
	
	effect = {
		from = { 
			add_modifier = {
				modifier = "mod_pc_hiveworld_0"
				days = -1
			}
		}
	}
	
	ai_weight = {
		weight = 10
		modifier = {
			factor = 0
			from = {
				is_colony = no
			}
		}
	}
}

#Irradiated
terraform_link = {
	to = "pc_hiveworld"
	from = "pc_irradiated"
	
	energy = @very_difficult_cost
	duration = @very_difficult_time
	
	potential = {
		has_ascension_perk = ap_hive_worlds
	}
	
	effect = {
		from = { 
			add_modifier = {
				modifier = "mod_pc_hiveworld_0"
				days = -1
			}
		}
	}
	
	ai_weight = {
		weight = 10
		modifier = {
			factor = 0
			from = {
				is_colony = no
			}
		}
	}
}


### Cold/Dry

#Tundra
terraform_link = {
	to = "pc_hiveworld"
	from = "pc_tundra"
	
	energy = @hard_cost
	duration = @hard_time
	
	potential = {
		has_ascension_perk = ap_hive_worlds
	}
	
	effect = {
		from = { 
			add_modifier = {
				modifier = "mod_pc_hiveworld_0"
				days = -1
			}
		}
	}
	
	ai_weight = {
		weight = 10
		modifier = {
			factor = 0
			from = {
				is_colony = no
			}
		}
	}
}

#Alpine
terraform_link = {
	to = "pc_hiveworld"
	from = "pc_alpine"
	
	energy = @hard_cost
	duration = @hard_time
	
	potential = {
		has_ascension_perk = ap_hive_worlds
	}
	
	effect = {
		from = { 
			add_modifier = {
				modifier = "mod_pc_hiveworld_0"
				days = -1
			}
		}
	}
	
	ai_weight = {
		weight = 10
		modifier = {
			factor = 0
			from = {
				is_colony = no
			}
		}
	}
}

#Steppe
terraform_link = {
	to = "pc_hiveworld"
	from = "pc_steppe"
	
	energy = @hard_cost
	duration = @hard_time
	
	potential = {
		has_ascension_perk = ap_hive_worlds
	}
	
	effect = {
		from = { 
			add_modifier = {
				modifier = "mod_pc_hiveworld_0"
				days = -1
			}
		}
	}
	
	ai_weight = {
		weight = 10
		modifier = {
			factor = 0
			from = {
				is_colony = no
			}
		}
	}
}

#Frozen Desert
terraform_link = {
	to = "pc_hiveworld"
	from = "pc_frozen_desert"
	
	energy = @hard_cost
	duration = @hard_time
	
	potential = {
		has_ascension_perk = ap_hive_worlds
	}
	
	effect = {
		from = { 
			add_modifier = {
				modifier = "mod_pc_hiveworld_0"
				days = -1
			}
		}
	}
	
	ai_weight = {
		weight = 10
		modifier = {
			factor = 0
			from = {
				is_colony = no
			}
		}
	}
}

#High-Altitude Desert
terraform_link = {
	to = "pc_hiveworld"
	from = "pc_hadesert"
	
	energy = @hard_cost
	duration = @hard_time
	
	potential = {
		has_ascension_perk = ap_hive_worlds
	}
	
	effect = {
		from = { 
			add_modifier = {
				modifier = "mod_pc_hiveworld_0"
				days = -1
			}
		}
	}
	
	ai_weight = {
		weight = 10
		modifier = {
			factor = 0
			from = {
				is_colony = no
			}
		}
	}
}

#Antarctic
terraform_link = {
	to = "pc_hiveworld"
	from = "pc_antarctic"
	
	energy = @hard_cost
	duration = @hard_time
	
	potential = {
		has_ascension_perk = ap_hive_worlds
	}
	
	effect = {
		from = { 
			add_modifier = {
				modifier = "mod_pc_hiveworld_0"
				days = -1
			}
		}
	}
	
	ai_weight = {
		weight = 10
		modifier = {
			factor = 0
			from = {
				is_colony = no
			}
		}
	}
}


### Cold/Wet

#Arctic
terraform_link = {
	to = "pc_hiveworld"
	from = "pc_arctic"
	
	energy = @hard_cost
	duration = @hard_time
	
	potential = {
		has_ascension_perk = ap_hive_worlds
	}
	
	effect = {
		from = { 
			add_modifier = {
				modifier = "mod_pc_hiveworld_0"
				days = -1
			}
		}
	}
	
	ai_weight = {
		weight = 10
		modifier = {
			factor = 0
			from = {
				is_colony = no
			}
		}
	}
}

#Cascadian
terraform_link = {
	to = "pc_hiveworld"
	from = "pc_cascadian"
	
	energy = @hard_cost
	duration = @hard_time
	
	potential = {
		has_ascension_perk = ap_hive_worlds
	}
	
	effect = {
		from = { 
			add_modifier = {
				modifier = "mod_pc_hiveworld_0"
				days = -1
			}
		}
	}
	
	ai_weight = {
		weight = 10
		modifier = {
			factor = 0
			from = {
				is_colony = no
			}
		}
	}
}

#Geothermal
terraform_link = {
	to = "pc_hiveworld"
	from = "pc_geothermal"
	
	energy = @hard_cost
	duration = @hard_time
	
	potential = {
		has_ascension_perk = ap_hive_worlds
	}
	
	effect = {
		from = { 
			add_modifier = {
				modifier = "mod_pc_hiveworld_0"
				days = -1
			}
		}
	}
	
	ai_weight = {
		weight = 10
		modifier = {
			factor = 0
			from = {
				is_colony = no
			}
		}
	}
}

#Swamp
terraform_link = {
	to = "pc_hiveworld"
	from = "pc_swamp"
	
	energy = @hard_cost
	duration = @hard_time
	
	potential = {
		has_ascension_perk = ap_hive_worlds
	}
	
	effect = {
		from = { 
			add_modifier = {
				modifier = "mod_pc_hiveworld_0"
				days = -1
			}
		}
	}
	
	ai_weight = {
		weight = 10
		modifier = {
			factor = 0
			from = {
				is_colony = no
			}
		}
	}
}

#Subarctic
terraform_link = {
	to = "pc_hiveworld"
	from = "pc_subarctic"
	
	energy = @hard_cost
	duration = @hard_time
	
	potential = {
		has_ascension_perk = ap_hive_worlds
	}
	
	effect = {
		from = { 
			add_modifier = {
				modifier = "mod_pc_hiveworld_0"
				days = -1
			}
		}
	}
	
	ai_weight = {
		weight = 10
		modifier = {
			factor = 0
			from = {
				is_colony = no
			}
		}
	}
}

#Glacial
terraform_link = {
	to = "pc_hiveworld"
	from = "pc_glacial"
	
	energy = @hard_cost
	duration = @hard_time
	
	potential = {
		has_ascension_perk = ap_hive_worlds
	}
	
	effect = {
		from = { 
			add_modifier = {
				modifier = "mod_pc_hiveworld_0"
				days = -1
			}
		}
	}
	
	ai_weight = {
		weight = 10
		modifier = {
			factor = 0
			from = {
				is_colony = no
			}
		}
	}
}


### Hot/Dry

#Desert
terraform_link = {
	to = "pc_hiveworld"
	from = "pc_desert"
	
	energy = @hard_cost
	duration = @hard_time
	
	potential = {
		has_ascension_perk = ap_hive_worlds
	}
	
	effect = {
		from = { 
			add_modifier = {
				modifier = "mod_pc_hiveworld_0"
				days = -1
			}
		}
	}
	
	ai_weight = {
		weight = 10
		modifier = {
			factor = 0
			from = {
				is_colony = no
			}
		}
	}
}

#Arid
terraform_link = {
	from = "pc_arid"
	to = "pc_hiveworld"
	
	energy = @hard_cost
	duration = @hard_time
	
	potential = {
		has_ascension_perk = ap_hive_worlds
	}
	
	effect = {
		from = { 
			add_modifier = {
				modifier = "mod_pc_hiveworld_0"
				days = -1
			}
		}
	}
	
	ai_weight = {
		weight = 10
		modifier = {
			factor = 0
			from = {
				is_colony = no
			}
		}
	}
}

#Savannah
terraform_link = {
	to = "pc_hiveworld"
	from = "pc_savannah"
	
	energy = @hard_cost
	duration = @hard_time
	
	potential = {
		has_ascension_perk = ap_hive_worlds
	}
	
	effect = {
		from = { 
			add_modifier = {
				modifier = "mod_pc_hiveworld_0"
				days = -1
			}
		}
	}
	
	ai_weight = {
		weight = 10
		modifier = {
			factor = 0
			from = {
				is_colony = no
			}
		}		
	}
}

#Sand Sea
terraform_link = {
	to = "pc_hiveworld"
	from = "pc_sandsea"
	
	energy = @hard_cost
	duration = @hard_time
	
	potential = {
		has_ascension_perk = ap_hive_worlds
	}
	
	effect = {
		from = { 
			add_modifier = {
				modifier = "mod_pc_hiveworld_0"
				days = -1
			}
		}
	}
	
	ai_weight = {
		weight = 10
		modifier = {
			factor = 0
			from = {
				is_colony = no
			}
		}
	}
}

#Mesa
terraform_link = {
	to = "pc_hiveworld"
	from = "pc_mesa"
	
	energy = @hard_cost
	duration = @hard_time
	
	potential = {
		has_ascension_perk = ap_hive_worlds
	}
	
	effect = {
		from = { 
			add_modifier = {
				modifier = "mod_pc_hiveworld_0"
				days = -1
			}
		}
	}
	
	ai_weight = {
		weight = 10
		modifier = {
			factor = 0
			from = {
				is_colony = no
			}
		}
	}
}

#Oasis
terraform_link = {
	to = "pc_hiveworld"
	from = "pc_oasis"
	
	energy = @hard_cost
	duration = @hard_time
	
	potential = {
		has_ascension_perk = ap_hive_worlds
	}
	
	effect = {
		from = { 
			add_modifier = {
				modifier = "mod_pc_hiveworld_0"
				days = -1
			}
		}
	}
	
	ai_weight = {
		weight = 10
		modifier = {
			factor = 0
			from = {
				is_colony = no
			}
		}
	}
}


### Hot/Wet

#Continental
terraform_link = {
	to = "pc_hiveworld"
	from = "pc_continental"
	
	energy = @hard_cost
	duration = @hard_time
	
	potential = {
		has_ascension_perk = ap_hive_worlds
	}
	
	effect = {
		from = { 
			add_modifier = {
				modifier = "mod_pc_hiveworld_0"
				days = -1
			}
		}
	}
	
	ai_weight = {
		weight = 10
		
		modifier = {
			factor = 0
			from = {
				is_colony = no
			}
		}
	}
}

#Tropical
terraform_link = {
	to = "pc_hiveworld"
	from = "pc_tropical"
	
	energy = @hard_cost
	duration = @hard_time
	
	potential = {
		has_ascension_perk = ap_hive_worlds
	}
	
	effect = {
		from = { 
			add_modifier = {
				modifier = "mod_pc_hiveworld_0"
				days = -1
			}
		}
	}
	
	ai_weight = {
		weight = 10
		
		modifier = {
			factor = 0
			from = {
				is_colony = no
			}
		}
	}
}

#Ocean
terraform_link = {
	to = "pc_hiveworld"
	from = "pc_ocean"
	
	energy = @hard_cost
	duration = @hard_time
	
	potential = {
		has_ascension_perk = ap_hive_worlds
	}
	
	effect = {
		from = { 
			add_modifier = {
				modifier = "mod_pc_hiveworld_0"
				days = -1
			}
		}
	}
	
	ai_weight = {
		weight = 10
		
		modifier = {
			factor = 0
			from = {
				is_colony = no
			}
		}
	}
}

#Desert Island
terraform_link = {
	to = "pc_hiveworld"
	from = "pc_desertislands"
	
	energy = @hard_cost
	duration = @hard_time
	
	potential = {
		has_ascension_perk = ap_hive_worlds
	}
	
	effect = {
		from = { 
			add_modifier = {
				modifier = "mod_pc_hiveworld_0"
				days = -1
			}
		}
	}
	
	ai_weight = {
		weight = 10
		
		modifier = {
			factor = 0
			from = {
				is_colony = no
			}
		}
	}
}

#High-Altitude Jungle
terraform_link = {
	to = "pc_hiveworld"
	from = "pc_hajungle"
	
	energy = @hard_cost
	duration = @hard_time
	
	potential = {
		has_ascension_perk = ap_hive_worlds
	}
	
	effect = {
		from = { 
			add_modifier = {
				modifier = "mod_pc_hiveworld_0"
				days = -1
			}
		}
	}
	
	ai_weight = {
		weight = 10
		
		modifier = {
			factor = 0
			from = {
				is_colony = no
			}
		}
	}
}

#Mangrove
terraform_link = {
	to = "pc_hiveworld"
	from = "pc_mangrove"
	
	energy = @hard_cost
	duration = @hard_time
	
	potential = {
		has_ascension_perk = ap_hive_worlds
	}
	
	effect = {
		from = { 
			add_modifier = {
				modifier = "mod_pc_hiveworld_0"
				days = -1
			}
		}
	}
	
	ai_weight = {
		weight = 10
		
		modifier = {
			factor = 0
			from = {
				is_colony = no
			}
		}
	}
}

#}

#########################
## FROM: Machine World ##
#########################{

### Exotic

#Methane
terraform_link = {
	to = "pc_methane"
	from = "pc_hiveworld"
	
	energy = @hard_cost
	duration = @hard_time
	
	potential = {
		has_technology = tech_climate_restoration
	}
	
	ai_weight = {
		weight = 2
		modifier = {
			factor = 0
			has_authority = auth_hive_mind
		}
	}
}

#Ammonia
terraform_link = {
	to = "pc_ammonia"
	from = "pc_hiveworld"
	
	energy = @hard_cost
	duration = @hard_time
	
	potential = {
		has_technology = tech_climate_restoration
	}
	
	ai_weight = {
		weight = 2
		modifier = {
			factor = 0
			has_authority = auth_hive_mind
		}
	}
}

### Cold/Dry

#Tundra
terraform_link = {
	from = "pc_hiveworld"
	to = "pc_tundra"
	
	energy = @hard_cost
	duration = @hard_time
	
	potential = {
		has_technology = tech_climate_restoration
	}
	
	ai_weight = {
		weight = 2
		modifier = {
			factor = 0
			has_authority = auth_hive_mind
		}
	}
}

#Alpine
terraform_link = {
	from = "pc_hiveworld"
	to = "pc_alpine"
	
	energy = @hard_cost
	duration = @hard_time
	
	potential = {
		has_technology = tech_climate_restoration
	}
	
	ai_weight = {
		weight = 2
		modifier = {
			factor = 0
			has_authority = auth_hive_mind
		}
	}
}

#Steppe
terraform_link = {
	to = "pc_steppe"
	from = "pc_hiveworld"
	
	energy = @hard_cost
	duration = @hard_time
	
	potential = {
		has_technology = tech_climate_restoration
	}
	
	ai_weight = {
		weight = 2
		modifier = {
			factor = 0
			has_authority = auth_hive_mind
		}
	}
}

#Frozen Desert
terraform_link = {
	to = "pc_frozen_desert"
	from = "pc_hiveworld"
	
	energy = @hard_cost
	duration = @hard_time
	
	potential = {
		has_technology = tech_climate_restoration
	}
	
	ai_weight = {
		weight = 2
		modifier = {
			factor = 0
			has_authority = auth_hive_mind
		}
	}
}

#High-Altitude Desert
terraform_link = {
	to = "pc_hadesert"
	from = "pc_hiveworld"
	
	energy = @hard_cost
	duration = @hard_time
	
	potential = {
		has_technology = tech_climate_restoration
	}
	
	ai_weight = {
		weight = 2
		modifier = {
			factor = 0
			has_authority = auth_hive_mind
		}
	}
}

#Antarctic
terraform_link = {
	to = "pc_antarctic"
	from = "pc_hiveworld"
	
	energy = @hard_cost
	duration = @hard_time
	
	potential = {
		has_technology = tech_climate_restoration
	}
	
	ai_weight = {
		weight = 2
		modifier = {
			factor = 0
			has_authority = auth_hive_mind
		}
	}
}


### Cold/Wet

#Arctic
terraform_link = {
	from = "pc_hiveworld"
	to = "pc_arctic"
	
	energy = @hard_cost
	duration = @hard_time
	
	potential = {
		has_technology = tech_climate_restoration
	}
	
	ai_weight = {
		weight = 2
		modifier = {
			factor = 0
			has_authority = auth_hive_mind
		}
	}
}

#Cascadian
terraform_link = {
	to = "pc_cascadian"
	from = "pc_hiveworld"
	
	energy = @hard_cost
	duration = @hard_time
	
	potential = {
		has_technology = tech_climate_restoration
	}
	
	ai_weight = {
		weight = 2
		modifier = {
			factor = 0
			has_authority = auth_hive_mind
		}
	}
}

#Geothermal
terraform_link = {
	to = "pc_geothermal"
	from = "pc_hiveworld"
	
	energy = @hard_cost
	duration = @hard_time
	
	potential = {
		has_technology = tech_climate_restoration
	}
	
	ai_weight = {
		weight = 2
		modifier = {
			factor = 0
			has_authority = auth_hive_mind
		}
	}
}

#Swamp
terraform_link = {
	to = "pc_swamp"
	from = "pc_hiveworld"
	
	energy = @hard_cost
	duration = @hard_time
	
	potential = {
		has_technology = tech_climate_restoration
	}
	
	ai_weight = {
		weight = 2
		modifier = {
			factor = 0
			has_authority = auth_hive_mind
		}
	}
}

#Subarctic
terraform_link = {
	to = "pc_subarctic"
	from = "pc_hiveworld"
	
	energy = @hard_cost
	duration = @hard_time
	
	potential = {
		has_technology = tech_climate_restoration
	}
	
	ai_weight = {
		weight = 2
		modifier = {
			factor = 0
			has_authority = auth_hive_mind
		}
	}
}

#Glacial
terraform_link = {
	to = "pc_glacial"
	from = "pc_hiveworld"
	
	energy = @hard_cost
	duration = @hard_time
	
	potential = {
		has_technology = tech_climate_restoration
	}
	
	ai_weight = {
		weight = 2
		modifier = {
			factor = 0
			has_authority = auth_hive_mind
		}
	}
}

### Hot/Dry

#Desert
terraform_link = {
	from = "pc_hiveworld"
	to = "pc_desert"
	
	energy = @hard_cost
	duration = @hard_time
	
	potential = {
		has_technology = tech_climate_restoration
	}
	
	ai_weight = {
		weight = 2
		modifier = {
			factor = 0
			has_authority = auth_hive_mind
		}
	}
}

#Arid
terraform_link = {
	to = "pc_arid"
	from = "pc_hiveworld"
	
	energy = @hard_cost
	duration = @hard_time
	
	potential = {
		has_technology = tech_climate_restoration
	}
	
	ai_weight = {
		weight = 2
		modifier = {
			factor = 0
			has_authority = auth_hive_mind
		}
	}
}

#Savannah
terraform_link = {
	from = "pc_hiveworld"
	to = "pc_savannah"
	
	energy = @hard_cost
	duration = @hard_time
	
	potential = {
		has_technology = tech_climate_restoration
	}
	
	ai_weight = {
		weight = 2
		modifier = {
			factor = 0
			has_authority = auth_hive_mind
		}
	}
}

#Sandsea
terraform_link = {
	to = "pc_sandsea"
	from = "pc_hiveworld"
	
	energy = @hard_cost
	duration = @hard_time
	
	potential = {
		has_technology = tech_climate_restoration
	}
	
	ai_weight = {
		weight = 2
		modifier = {
			factor = 0
			has_authority = auth_hive_mind
		}
	}
}

#Mesa
terraform_link = {
	to = "pc_mesa"
	from = "pc_hiveworld"
	
	energy = @hard_cost
	duration = @hard_time
	
	potential = {
		has_technology = tech_climate_restoration
	}
	
	ai_weight = {
		weight = 2
		modifier = {
			factor = 0
			has_authority = auth_hive_mind
		}
	}
}

#Oasis
terraform_link = {
	to = "pc_oasis"
	from = "pc_hiveworld"
	
	energy = @hard_cost
	duration = @hard_time
	
	potential = {
		has_technology = tech_climate_restoration
	}
	
	ai_weight = {
		weight = 2
		modifier = {
			factor = 0
			has_authority = auth_hive_mind
		}
	}
}


### Hot/Wet

#Continental
terraform_link = {
	from = "pc_hiveworld"
	to = "pc_continental"
	
	energy = @hard_cost
	duration = @hard_time
	
	potential = {
		has_technology = tech_climate_restoration
	}
	
	ai_weight = {
		weight = 2
		modifier = {
			factor = 0
			has_authority = auth_hive_mind
		}
	}
}

#Tropical
terraform_link = {
	from = "pc_hiveworld"
	to = "pc_tropical"
	
	energy = @hard_cost
	duration = @hard_time
	
	potential = {
		has_technology = tech_climate_restoration
	}
	
	ai_weight = {
		weight = 2
		modifier = {
			factor = 0
			has_authority = auth_hive_mind
		}
	}
}

#Ocean
terraform_link = {
	from = "pc_hiveworld"
	to = "pc_ocean"
	
	energy = @hard_cost
	duration = @hard_time
	
	potential = {
		has_technology = tech_climate_restoration
	}
	
	ai_weight = {
		weight = 2
		modifier = {
			factor = 0
			has_authority = auth_hive_mind
		}
	}
}

#Desert Islands
terraform_link = {
	to = "pc_desertislands"
	from = "pc_hiveworld"
	
	energy = @hard_cost
	duration = @hard_time
	
	potential = {
		has_technology = tech_climate_restoration
	}
	
	ai_weight = {
		weight = 2
		modifier = {
			factor = 0
			has_authority = auth_hive_mind
		}
	}
}

#High-Altitude Jungle
terraform_link = {
	to = "pc_hajungle"
	from = "pc_hiveworld"
	
	energy = @hard_cost
	duration = @hard_time
	
	potential = {
		has_technology = tech_climate_restoration
	}
	
	ai_weight = {
		weight = 2
		modifier = {
			factor = 0
			has_authority = auth_hive_mind
		}
	}
}

#Mangrove
terraform_link = {
	to = "pc_mangrove"
	from = "pc_hiveworld"
	
	energy = @hard_cost
	duration = @hard_time
	
	potential = {
		has_technology = tech_climate_restoration
	}
	
	ai_weight = {
		weight = 2
		modifier = {
			factor = 0
			has_authority = auth_hive_mind
		}
	}
}

#}