@perfect = 1.0
@primary = 0.70
@secondary = 0.50
@tertiary = 0.25
@nuked = 0.10
@quaternary = 0.10

trait_pc_ecumenopolis_preference = {
	#icon = "gfx/interface/icons/traits/trait_pc_ring_preference.dds"
	modification = no
	modifier = {
		pc_ecumenopolis_habitability = 1.0
		pc_nuked_habitability = @nuked
		pc_irradiated_habitability = @nuked 
		pc_desert_habitability = @tertiary
		pc_tropical_habitability = @tertiary
		pc_continental_habitability = @tertiary
		pc_ocean_habitability = @tertiary
		pc_arctic_habitability = @tertiary
		pc_tundra_habitability = @tertiary
		pc_arid_habitability = @tertiary
		pc_savannah_habitability = @tertiary
		pc_alpine_habitability = @tertiary
		pc_oasis_habitability = @tertiary
		pc_mesa_habitability = @tertiary
		pc_swamp_habitability = @tertiary
		pc_frozen_desert_habitability = @tertiary
		pc_steppe_habitability = @tertiary
		pc_cascadian_habitability = @tertiary
		pc_hadesert_habitability = @tertiary
		pc_geothermal_habitability = @tertiary
		pc_desertislands_habitability = @tertiary
		pc_sandsea_habitability = @tertiary
		pc_mangrove_habitability = @tertiary
		pc_glacial_habitability = @tertiary
		pc_subarctic_habitability = @tertiary
		pc_hajungle_habitability = @tertiary
		pc_antarctic_habitability = @tertiary
	}
}

trait_pc_hiveworld_preference = {
	#icon = "gfx/interface/icons/traits/trait_pc_ring_preference.dds"
	modification = no
	modifier = {
		pc_hiveworld_habitability = 1.0
		pc_nuked_habitability = @nuked
		pc_irradiated_habitability = @nuked 
		pc_desert_habitability = @tertiary
		pc_tropical_habitability = @tertiary
		pc_continental_habitability = @tertiary
		pc_ocean_habitability = @tertiary
		pc_arctic_habitability = @tertiary
		pc_tundra_habitability = @tertiary
		pc_arid_habitability = @tertiary
		pc_savannah_habitability = @tertiary
		pc_alpine_habitability = @tertiary
		pc_oasis_habitability = @tertiary
		pc_mesa_habitability = @tertiary
		pc_swamp_habitability = @tertiary
		pc_frozen_desert_habitability = @tertiary
		pc_steppe_habitability = @tertiary
		pc_cascadian_habitability = @tertiary
		pc_hadesert_habitability = @tertiary
		pc_geothermal_habitability = @tertiary
		pc_desertislands_habitability = @tertiary
		pc_sandsea_habitability = @tertiary
		pc_mangrove_habitability = @tertiary
		pc_glacial_habitability = @tertiary
		pc_subarctic_habitability = @tertiary
		pc_hajungle_habitability = @tertiary
		pc_antarctic_habitability = @tertiary
	}
}

trait_hive_mind = {
	cost = 0
	sorting_priority = 20
	
	initial = no
	randomized = no
	modification = no
	forced_happiness = yes
	alters_species_identity = yes
	
	leader_age_min = 10
	leader_age_max = 20
	
	allowed_archetypes = { BIOLOGICAL }	
	#allowed_classes = { HUM MAM REP AVI ART MOL FUN PLANT }
	
	modifier = {
		pc_hiveworld_habitability = 1.0
		FEE_pc_fractured_planet_habitability = 1.0
	}
	
	ai_weight = {
		weight = 0
	}

	icon = "gfx/interface/icons/traits/leader_traits/leader_trait_ruler_hive_mind.dds"
}