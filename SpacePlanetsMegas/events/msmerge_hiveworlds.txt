namespace = hiveworld


#Hive Worlds: Advance hive world modifier when edict completes
planet_event = {
	id = hiveworld.101
	hide_window = yes
	is_triggered_only = yes
	
	immediate = {
		if = {
			limit = {
				AND = {
					owner = { has_ascension_perk = ap_hive_worlds }
					has_planet_flag = developing_hiveworld
				}
			}
			remove_planet_flag = developing_hiveworld
			if = {
				limit = { planet_size < 20 }
				random_list = {
					7 = {}
					3 = { change_planet_size = 1 }
				}
			}
			if = {
				limit = {
					has_modifier = mod_pc_hiveworld_0
				}
				remove_modifier = mod_pc_hiveworld_0
				add_modifier = {
					modifier = "mod_pc_hiveworld_1"
					days = -1
				}
				break = yes
			}
			if = {
				limit = {
					has_modifier = mod_pc_hiveworld_1
				}
				remove_modifier = mod_pc_hiveworld_1
				add_modifier = {
					modifier = "mod_pc_hiveworld_2"
					days = -1
				}
				break = yes
			}
			if = {
				limit = {
					has_modifier = mod_pc_hiveworld_2
				}
				remove_modifier = mod_pc_hiveworld_2
				add_modifier = {
					modifier = "mod_pc_hiveworld_3"
					days = -1
				}
				break = yes
			}
			if = {
				limit = {
					has_modifier = mod_pc_hiveworld_3
				}
				remove_modifier = mod_pc_hiveworld_3
				add_modifier = {
					modifier = "mod_pc_hiveworld_4"
					days = -1
				}
				break = yes
			}
			if = {
				limit = {
					has_modifier = mod_pc_hiveworld_4
				}
				remove_modifier = mod_pc_hiveworld_4
				add_modifier = {
					modifier = "mod_pc_hiveworld_5"
					days = -1
				}
				break = yes
			}
		}
	}
}

#Hive Worlds: Exchange useful/useless modifiers when planet changes hands
planet_event = {
	id = hiveworld.102
	hide_window = yes
	is_triggered_only = yes
	
	trigger = {
		is_planet_class = pc_hiveworld
	}
	
	immediate = {
		if = {
			limit = { has_planet_flag = developing_hiveworld }
			remove_planet_flag = developing_hiveworld
			remove_modifier = mod_developing_hiveworld
		}
		if = {
			limit = { owner = { NOT = { has_authority = auth_hive_mind } } }
			if = {
				limit = { has_modifier = mod_pc_hiveworld_0 }
				remove_modifier = mod_pc_hiveworld_0
				add_modifier = {
					modifier = "mod_pc_hiveworld_0_useless"
					days = -1
				} 
				break = yes
			}
			if = {
				limit = { has_modifier = mod_pc_hiveworld_1 }
				remove_modifier = mod_pc_hiveworld_1
				add_modifier = {
					modifier = "mod_pc_hiveworld_1_useless"
					days = -1
				}
				break = yes
			}
			if = {
				limit = { has_modifier = mod_pc_hiveworld_2 }
				remove_modifier = mod_pc_hiveworld_2
				add_modifier = {
					modifier = "mod_pc_hiveworld_2_useless"
					days = -1
				}
				break = yes
			}
			if = {
				limit = { has_modifier = mod_pc_hiveworld_3 }
				remove_modifier = mod_pc_hiveworld_3
				add_modifier = {
					modifier = "mod_pc_hiveworld_3_useless"
					days = -1
				} 
				break = yes
			}
			if = {
				limit = { has_modifier = mod_pc_hiveworld_4 }
				remove_modifier = mod_pc_hiveworld_4
				add_modifier = {
					modifier = "mod_pc_hiveworld_4_useless"
					days = -1
				} 
				break = yes
			}
			if = {
				limit = { has_modifier = mod_pc_hiveworld_5 }
				remove_modifier = mod_pc_hiveworld_5
				add_modifier = {
					modifier = "mod_pc_hiveworld_5_useless"
					days = -1
				} 
				break = yes
			}
			else = {
				if = {
					limit = { has_modifier = mod_pc_hiveworld_0_useless }
					remove_modifier = mod_pc_hiveworld_0_useless
					add_modifier = {
						modifier = "mod_pc_hiveworld_0"
						days = -1
					} 
					break = yes
				}
				if = {
					limit = { has_modifier = mod_pc_hiveworld_1_useless }
					remove_modifier = mod_pc_hiveworld_1_useless
					add_modifier = {
						modifier = "mod_pc_hiveworld_1"
						days = -1
					} 
					break = yes
				}
				if = {
					limit = { has_modifier = mod_pc_hiveworld_2_useless }
					remove_modifier = mod_pc_hiveworld_2_useless
					add_modifier = {
						modifier = "mod_pc_hiveworld_2"
						days = -1
					} 
					break = yes
				}
				if = {
					limit = { has_modifier = mod_pc_hiveworld_3_useless }
					remove_modifier = mod_pc_hiveworld_3_useless
					add_modifier = {
						modifier = "mod_pc_hiveworld_3"
						days = -1
					}
					break = yes
				}
				if = {
					limit = { has_modifier = mod_pc_hiveworld_4_useless }
					remove_modifier = mod_pc_hiveworld_4_useless
					add_modifier = {
						modifier = "mod_pc_hiveworld_4"
						days = -1
					}
					break = yes
				}
				if = {
					limit = { has_modifier = mod_pc_hiveworld_5_useless }
					remove_modifier = mod_pc_hiveworld_5_useless
					add_modifier = {
						modifier = "mod_pc_hiveworld_5"
						days = -1
					}
					break = yes
				}
			}
		}
	}
}

#Hive Worlds: Clean planet when terraformed
planet_event = {
	id = hiveworld.103
	hide_window = yes
	is_triggered_only = yes
	
	trigger = {
		NOT = { is_planet_class = pc_hiveworld }
	}
	
	immediate = {
		remove_planet_flag = developing_hiveworld
		remove_modifier = mod_developing_hiveworld
		remove_modifier = mod_pc_hiveworld_0
		remove_modifier = mod_pc_hiveworld_1
		remove_modifier = mod_pc_hiveworld_2
		remove_modifier = mod_pc_hiveworld_3
		remove_modifier = mod_pc_hiveworld_4
		remove_modifier = mod_pc_hiveworld_5
		remove_modifier = mod_pc_hiveworld_0_useless
		remove_modifier = mod_pc_hiveworld_1_useless
		remove_modifier = mod_pc_hiveworld_2_useless
		remove_modifier = mod_pc_hiveworld_3_useless
		remove_modifier = mod_pc_hiveworld_4_useless
		remove_modifier = mod_pc_hiveworld_5_useless
	}
}
