setup_scenario = {
	name = "large"
	priority = 3					#priority decides in which order the scenarios are listed
	num_stars = 800
	radius = 450					#should be less than 500, preferably less than ~460
	num_empires = { min = 0 max = 31 }	#limits player customization
	num_empire_default = 17
	fallen_empire_default = 3
	fallen_empire_max = 8	
	advanced_empire_default = 5
	colonizable_planet_odds = 1.0
	primitive_odds = 1.0
	crisis_strength = 1.25
	
	cluster_count = {
		# method = one_every_x_empire
		method = constant
		value = 1
	}
	cluster_radius = 360
	cluster_distance_from_core = 0
	
	num_nebulas	= 8
	nebula_size = 60
	nebula_min_dist = 200
	
	supports_shape = elliptical
	supports_shape = spiral_2
	supports_shape = spiral_4
	supports_shape = ring
}