### Icons set here are the ones shown in compnent editor list.

component_set = {
	key = "NHSC_TORPEDO_PSI"
	
	icon = "GFX_ship_part_nhsc_psitorpedo"
	icon_frame = 1	
}

component_set = {
	key = "NHSC_CANNON_PSI"

	icon = "GFX_ship_part_nhsc_psicannon"
	icon_frame = 1
}

component_set = {
	key = "NHSC_PSI_LANCE"

	icon = "GFX_ship_part_nhsc_psilance"
	icon_frame = 1
}