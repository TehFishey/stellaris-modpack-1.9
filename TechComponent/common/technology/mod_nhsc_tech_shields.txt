##################
### TECH COSTS ###
##################

@tier1cost1 = 250
@tier1cost2 = 500
@tier1cost3 = 750
@tier1cost4 = 1000

@tier2cost1 = 1500
@tier2cost2 = 2000
@tier2cost3 = 2500
@tier2cost4 = 3000

@tier3cost1 = 4500
@tier3cost2 = 6000
@tier3cost3 = 8500
@tier3cost4 = 10000

@tier4cost1 = 12000
@tier4cost2 = 14000
@tier4cost3 = 16000
@tier4cost4 = 18000

@tier5cost1 = 22000
@tier5cost2 = 26000

####################
### TECH WEIGHTS ###
####################

@tier1weight1 = 105
@tier1weight2 = 100
@tier1weight3 = 95
@tier1weight4 = 90

@tier2weight1 = 75
@tier2weight2 = 70
@tier2weight3 = 65
@tier2weight4 = 60

@tier3weight1 = 45
@tier3weight2 = 40
@tier3weight3 = 35
@tier3weight4 = 30

@tier4weight1 = 25
@tier4weight2 = 20
@tier4weight3 = 15
@tier4weight4 = 10

@tier5weight1 = 5
@tier5weight2 = 3

####################
### A.I. WEIGHTS ###
####################

@redundant = 1
@important = 50
@strategic = 100
@crucial = 1000

################
### SCARCITY ###
################

@very_common = 1.5
@common = 1.25
@scarce = 0.75
@rare = 0.5
@very_rare = 0.25

############################################################
###                     TECHNOLOGIES                     ###
############################################################

## Improved Hyper Shields
nhsc_tech_advshields_1 = {
	area = physics
	category = { field_manipulation }
	tier = 4
	cost = @tier4cost2
	weight = @tier4weight2
	ai_update_type = all
	prerequisites = { "tech_shields_5" "tech_physics_lab_3" }

	modifier = {
		planet_fortification_strength = 0.1
	}
	
	weight_modifier = {
		factor = @common
		modifier = {
			factor = @common
			research_leader = {
				area = physics
				OR = {
					has_trait = "leader_trait_expertise_field_manipulation"
					has_trait = "leader_trait_expertise_field_manipulation_est_2"
				}
			}
		}
		modifier = {
			factor = 0
			OR = {
				has_technology = "nhsc_tech_advshields_2"
				has_technology = "nhsc_tech_waveforcearmor"
				has_technology = "nhsc_tech_enigmatic_shield_2"
				has_technology = "nhsc_tech_atfield"
			}
		}
	}
	
	ai_weight = {
		factor = @important
		modifier = {
			factor = @common
			research_leader = {
				area = physics
				OR = {
					has_trait = "leader_trait_expertise_field_manipulation"
					has_trait = "leader_trait_expertise_field_manipulation_est_2"
				}
			}
		}	
	}
}

## Citadel Shields
nhsc_tech_advshields_2 = {
	area = physics
	category = { field_manipulation }
	tier = 4
	cost = @tier4cost4
	weight = @tier4weight4
	ai_update_type = all
	prerequisites = { "nhsc_tech_advshields_1" }
	#is_rare = yes

	modifier = {
		planet_fortification_strength = 0.1
	}
	
	weight_modifier = {
		factor = @common
		modifier = {
			factor = @common
			research_leader = {
				area = physics
				OR = {
					has_trait = "leader_trait_expertise_field_manipulation"
					has_trait = "leader_trait_expertise_field_manipulation_est_2"
				}
			}
		}
		modifier = {
			factor = 0
			OR = {
				has_technology = "nhsc_tech_waveforcearmor"
				has_technology = "nhsc_tech_enigmatic_shield_2"
				has_technology = "nhsc_tech_atfield"
			}
		}
	}
	
	ai_weight = {
		factor = @important
		modifier = {
			factor = @common
			research_leader = {
				area = physics
				OR = {
					has_trait = "leader_trait_expertise_field_manipulation"
					has_trait = "leader_trait_expertise_field_manipulation_est_2"
				}
			}
		}	
	}
}

## Wave-Force Armor
nhsc_tech_waveforcearmor = {
	area = physics
	category = { field_manipulation }
	tier = 5
	cost = @tier5cost1
	weight = @tier5weight2
	ai_update_type = all
	prerequisites = { "nhsc_tech_advshields_2" "nhsc_tech_gravmanipulation" }
	is_rare = yes
	#is_dangerous = yes
	#is_reverse_engineerable = no

	modifier = {
		planet_fortification_strength = 0.2
	}
	
	weight_modifier = {
		factor = @rare
		modifier = {
			factor = 2
			research_leader = {
				area = physics
				OR = {
					has_trait = "leader_trait_curator"
					has_trait = "leader_trait_spark_of_genius"
					has_trait = "leader_trait_spark_of_genius_est_2"
					has_trait = "leader_trait_scientist_psionic"						
				}
			}	
		}
		modifier = {
			factor = 1.5
			research_leader = {
				area = physics
				OR = {
					has_trait = "leader_trait_sapient_AI_assistant"
					has_trait = "leader_trait_sentient_AI_assistant_est_2"
					has_trait = "leader_trait_maniacal"
					has_trait = "leader_trait_maniacal_est_2"
					has_trait = "leader_trait_scientist_synthetic"
					has_trait = "leader_trait_scientist_irobotic"
				}
			}	
		}
		modifier = {
			factor = @common
			research_leader = {
				area = physics
				OR = {
					has_trait = "leader_trait_expertise_field_manipulation"
					has_trait = "leader_trait_expertise_field_manipulation_est_2"
				}
			}
		}
	}
	
	ai_weight = {
		factor = @strategic
		modifier = {
			factor = @common
			is_at_war = yes
		}
		modifier = {
			factor = 1.25
			research_leader = {
				area = physics
				OR = {
					has_trait = "leader_trait_expertise_field_manipulation"
					has_trait = "leader_trait_expertise_field_manipulation_est_2"
				}
			}
		}
	}
}

## Cyclonic Shielding
nhsc_tech_cyclonicshields_1 = {
    area = physics
	category = { field_manipulation }
	tier = 4
	cost = @tier4cost1
	weight = @tier4weight1
	ai_update_type = all
	prerequisites = { "tech_shields_4" "tech_shield_rechargers_1" }
	is_rare = yes

	modifier = {
		planet_fortification_strength = 0.05
	}
	
	weight_modifier = {
		factor = @scarce
		modifier = {
			factor = 2
			research_leader = {
				area = physics
				OR = {
					has_trait = "leader_trait_curator"
					has_trait = "leader_trait_spark_of_genius"
					has_trait = "leader_trait_spark_of_genius_est_2"
					has_trait = "leader_trait_scientist_psionic"						
				}
			}	
		}
		modifier = {
			factor = 1.5
			research_leader = {
				area = physics
				OR = {
					has_trait = "leader_trait_sapient_AI_assistant"
					has_trait = "leader_trait_sentient_AI_assistant_est_2"
					has_trait = "leader_trait_maniacal"
					has_trait = "leader_trait_maniacal_est_2"
					has_trait = "leader_trait_scientist_synthetic"
					has_trait = "leader_trait_scientist_irobotic"
				}
			}	
		}
		modifier = {
			factor = @common
			research_leader = {
				area = physics
				OR = {
					has_trait = "leader_trait_expertise_field_manipulation"
					has_trait = "leader_trait_expertise_field_manipulation_est_2"
				}
			}
		}
	}
	
	ai_weight = {
		factor = @important
		modifier = {
			factor = @very_common
			is_at_war = yes
		}
		modifier = {
			factor = 1.25
			research_leader = {
				area = physics
				OR = {
					has_trait = "leader_trait_expertise_field_manipulation"
					has_trait = "leader_trait_expertise_field_manipulation_est_2"
				}
			}
		}
	}	
}

## Improved Cyclonic Shielding
nhsc_tech_cyclonicshields_2 = {
    area = physics
	category = { field_manipulation }
	tier = 4
	cost = @tier4cost3
	weight = @tier4weight3
	ai_update_type = all
	prerequisites = { "nhsc_tech_cyclonicshields_1" "tech_shields_5" }
	is_rare = yes
	
	modifier = {
		planet_fortification_strength = 0.05
	}
	
	weight_modifier = {
		factor = @scarce
		modifier = {
			factor = 2
			research_leader = {
				area = physics
				OR = {
					has_trait = "leader_trait_curator"
					has_trait = "leader_trait_spark_of_genius"
					has_trait = "leader_trait_spark_of_genius_est_2"
					has_trait = "leader_trait_scientist_psionic"						
				}
			}	
		}
		modifier = {
			factor = 1.5
			research_leader = {
				area = physics
				OR = {
					has_trait = "leader_trait_sapient_AI_assistant"
					has_trait = "leader_trait_sentient_AI_assistant_est_2"
					has_trait = "leader_trait_maniacal"
					has_trait = "leader_trait_maniacal_est_2"
					has_trait = "leader_trait_scientist_synthetic"
					has_trait = "leader_trait_scientist_irobotic"
				}
			}	
		}
		modifier = {
			factor = @common
			research_leader = {
				area = physics
				OR = {
					has_trait = "leader_trait_expertise_field_manipulation"
					has_trait = "leader_trait_expertise_field_manipulation_est_2"
				}
			}
		}
	}
	
	ai_weight = {
		factor = @important
		modifier = {
			factor = @very_common
			is_at_war = yes
		}
		modifier = {
			factor = 1.25
			research_leader = {
				area = physics
				OR = {
					has_trait = "leader_trait_expertise_field_manipulation"
					has_trait = "leader_trait_expertise_field_manipulation_est_2"
				}
			}
		}
	}	
}

## Cascade Shielding (Formerly Cyclonic Protectors)
nhsc_tech_cyclonicshields_3 = {
    area = physics
	category = { field_manipulation }
	tier = 4
	cost = @tier4cost4
	weight = @tier4weight4
	ai_update_type = all
	prerequisites = { "nhsc_tech_cyclonicshields_2" }
	is_rare = yes
	
	modifier = {
		planet_fortification_strength = 0.05
	}
	
	weight_modifier = {
		factor = @scarce
		modifier = {
			factor = 2
			research_leader = {
				area = physics
				OR = {
					has_trait = "leader_trait_curator"
					has_trait = "leader_trait_spark_of_genius"
					has_trait = "leader_trait_spark_of_genius_est_2"
					has_trait = "leader_trait_scientist_psionic"						
				}
			}	
		}
		modifier = {
			factor = 1.5
			research_leader = {
				area = physics
				OR = {
					has_trait = "leader_trait_sapient_AI_assistant"
					has_trait = "leader_trait_sentient_AI_assistant_est_2"
					has_trait = "leader_trait_maniacal"
					has_trait = "leader_trait_maniacal_est_2"
					has_trait = "leader_trait_scientist_synthetic"
					has_trait = "leader_trait_scientist_irobotic"
				}
			}	
		}
		modifier = {
			factor = @common
			research_leader = {
				area = physics
				OR = {
					has_trait = "leader_trait_expertise_field_manipulation"
					has_trait = "leader_trait_expertise_field_manipulation_est_2"
				}
			}
		}
	}
	
	ai_weight = {
		factor = @important
		modifier = {
			factor = @very_common
			is_at_war = yes
		}
		modifier = {
			factor = 1.25
			research_leader = {
				area = physics
				OR = {
					has_trait = "leader_trait_expertise_field_manipulation"
					has_trait = "leader_trait_expertise_field_manipulation_est_2"
				}
			}
		}
	}	
}

## Improved Enigmatic Shield
nhsc_tech_enigmatic_shield_2 = {
	area = physics
	category = { field_manipulation }
	tier = 5
	cost = @tier5cost2
	weight = @tier5weight1
	ai_update_type = all
	prerequisites = { "tech_enigmatic_deflector" "nhsc_tech_advshields_2" }
	is_rare = yes
	#is_reverse_engineerable = no

	modifier = {
		planet_fortification_strength = 0.2
	}

	weight_modifier = {
		factor = @very_rare
		modifier = {
			factor = 2
			research_leader = {
				area = physics
				OR = {
					has_trait = "leader_trait_curator"
					has_trait = "leader_trait_spark_of_genius"
					has_trait = "leader_trait_spark_of_genius_est_2"
					has_trait = "leader_trait_scientist_psionic"						
				}
			}	
		}
		modifier = {
			factor = 1.5
			research_leader = {
				area = physics
				OR = {
					has_trait = "leader_trait_sapient_AI_assistant"
					has_trait = "leader_trait_sentient_AI_assistant_est_2"
					has_trait = "leader_trait_maniacal"
					has_trait = "leader_trait_maniacal_est_2"
					has_trait = "leader_trait_scientist_synthetic"
					has_trait = "leader_trait_scientist_irobotic"
				}
			}	
		}
		modifier = {
			factor = @common
			research_leader = {
				area = physics
				OR = {
					has_trait = "leader_trait_expertise_field_manipulation"
					has_trait = "leader_trait_expertise_field_manipulation_est_2"
				}
			}
		}
	}
	
	ai_weight = {
		factor = @crucial
		modifier = {
			factor = 1.25
			research_leader = {
				area = physics
				OR = {
					has_trait = "leader_trait_expertise_field_manipulation"
					has_trait = "leader_trait_expertise_field_manipulation_est_2"
				}
			}
		}
	}
}

## A.T Field
nhsc_tech_atfield = {
	tier = 5
	area = physics
	category = { psionics }
	cost = @tier5cost2
	weight = @tier5weight1
	ai_update_type = all	
	prerequisites = { "tech_psionic_shield" "nhsc_tech_psiweapons" "nhsc_tech_advshields_2" }
	is_rare = yes
	is_reverse_engineerable = no

	weight_modifier = {
		@rare
		modifier = {
			factor = 1.15
			OR = {
				has_ethic = ethic_spiritualist
				has_ethic = ethic_fanatic_spiritualist
			}
		}
		modifier = {
			factor = @common
			research_leader = {
				area = physics
				OR = {
					has_trait = "leader_trait_expertise_psionics"
					has_trait = "leader_trait_scientist_psionic"						
				}
			}
		}
		modifier = {
			factor = 2
			research_leader = {
				area = physics
				OR = {
					has_trait = "leader_trait_curator"
					has_trait = "leader_trait_spark_of_genius"
					has_trait = "leader_trait_spark_of_genius_est_2"
					has_trait = "leader_trait_scientist_psionic"						
				}
			}	
		}
		modifier = {
			factor = 1.5
			research_leader = {
				area = physics
				OR = {
					has_trait = "leader_trait_sapient_AI_assistant"
					has_trait = "leader_trait_sentient_AI_assistant_est_2"
					has_trait = "leader_trait_maniacal"
					has_trait = "leader_trait_maniacal_est_2"
					has_trait = "leader_trait_scientist_synthetic"
					has_trait = "leader_trait_scientist_irobotic"
				}
			}	
		}
		modifier = {
			factor = 2
			has_country_strategic_resource = {
               type = sr_shroud_energy
                 amount > 0
            }
		}
	}
	
	ai_weight = {
		factor = @crucial
		modifier = {
			factor = 1.25
			research_leader = {
				area = society
				OR = {
					has_trait = "leader_trait_expertise_psionics"
					has_trait = "leader_trait_expertise_psionics_est_2"
				}
			}
		}
	}
}