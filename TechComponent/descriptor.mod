name="*CST* Technology & Components MERGE [1.9.*]"
path="mod/TechComponent"
tags={
	"Fixes"
	"Overhaul"
	"Gameplay"
	"Balance"
	"AI"
}
picture="thumb_technology.png"
supported_version="1.9.*"
