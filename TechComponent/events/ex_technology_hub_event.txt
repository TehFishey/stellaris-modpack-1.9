namespace = ex_technology_hub_event

country_event = {
	id = ex_technology_hub_event.1
	hide_window = yes
	is_triggered_only = yes

	immediate = {
		#Armor
		if = {
			limit ={
				OR = {
					last_increased_tech = "nhsc_tech_adv_armor_2"
					last_increased_tech = "nhsc_tech_adv_armor_1"
					last_increased_tech = "tech_dragon_armor"
					last_increased_tech = "tech_ship_armor_5"
					last_increased_tech = "tech_ship_armor_4"
					last_increased_tech = "tech_ship_armor_3"
					last_increased_tech = "tech_ship_armor_2"
				}
			}
			country_event = { id = ex_technology_events.101 }
		}
		#Special Armor
		if = {
			limit ={
				OR = {
					last_increased_tech = "nhsc_tech_nanodragonscales"
					last_increased_tech = "nhsc_tech_nanoarmor_2"
					#last_increased_tech = "nhsc_tech_shieldarmor_3"
					#last_increased_tech = "nhsc_tech_shieldarmor_2"
				}
			}
			country_event = { id = ex_technology_events.102 }
		}
		#Shields
		if = {
			limit ={
				OR = {
					last_increased_tech = "nhsc_tech_waveforcearmor"
					last_increased_tech = "nhsc_tech_advshields_2"
					last_increased_tech = "nhsc_tech_advshields_1"
					last_increased_tech = "tech_shields_5"
					last_increased_tech = "tech_shields_4"
					last_increased_tech = "tech_shields_3"
					last_increased_tech = "tech_shields_2"
				}
			}
			country_event = { id = ex_technology_events.201 }
		}
		#Special Shields
		if = {
			limit ={
				OR = {
					last_increased_tech = "nhsc_tech_enigmatic_shield_2"
					last_increased_tech = "tech_enigmatic_deflector"
					last_increased_tech = "nhsc_tech_atfield"
					last_increased_tech = "tech_psionic_shield"
					last_increased_tech = "nhsc_tech_cyclonicshields_3"
					last_increased_tech = "nhsc_tech_cyclonicshields_2"
				}
			}
			country_event = { id = ex_technology_events.202 }
		}			
		#Thrusters
		if = {
			limit ={
				OR = {
					last_increased_tech = "nhsc_tech_gravmanipulationengines"
					last_increased_tech = "nhsc_tech_antimatterthrusters"
					last_increased_tech = "tech_thrusters_4"
					last_increased_tech = "tech_thrusters_3"
					last_increased_tech = "tech_thrusters_2"	
				}
			}
			country_event = { id = ex_technology_events.301 }
		}				
		#Sensors
		if = {
			limit ={
				OR = {
					last_increased_tech = "nhsc_tech_psisensors"
					last_increased_tech = "nhsc_tech_stringtheory"
					last_increased_tech = "nhsc_tech_improvedtachyonsensors"
					last_increased_tech = "tech_sensors_4"
					last_increased_tech = "tech_sensors_3"
				}
			}	
			country_event = { id = ex_technology_events.401 }
		}	
		#Combat Computers
		if = {
			limit = {											
				OR = {
					last_increased_tech = "tech_combat_computers_3"
					last_increased_tech = "tech_combat_computers_2"
				}	
			}
			country_event = { id = ex_technology_events.5 }
		}																								
		#Crystal Armor
		if = {
			limit = {
				last_increased_tech = "tech_crystal_armor_2"					
			}		
			country_event = { id = ex_technology_events.8 }
		}										
		#Afterburners
		if = {
			limit = {
				last_increased_tech = "tech_afterburners_2"							
			}	
			country_event = { id = ex_technology_events.9 }
		}				
		#Shield Rechargers
		if = {
			limit = {
				last_increased_tech = "nhsc_tech_advshieldcapacitors"
			}	
			country_event = { id = ex_technology_events.1001 }
		}																
		#Vanilla Reactors
		if = {
			limit = {
				OR = {
					last_increased_tech = tech_cold_fusion_power
					last_increased_tech = tech_antimatter_power
					last_increased_tech = tech_zero_point_power					
				}					
			}	
			country_event = { id = ex_technology_events.1301 }
		}
		#Special Reactors
		if = {
			limit = {
				OR = {
					last_increased_tech = nhsc_tech_advzeropointreactor_1
					last_increased_tech = nhsc_tech_advzeropointreactor_2
					last_increased_tech = nhsc_tech_advzeropointreactor_3
					last_increased_tech = tech_enigmatic_power_core
					last_increased_tech = nhsc_tech_enigmatic_power_2				
				}					
			}	
			country_event = { id = ex_technology_events.1302 }
		}		
		#Lasers
		if = {
			limit = {
				OR = {
					last_increased_tech = "nhsc_tech_laser_7"
					last_increased_tech = "nhsc_tech_laser_6"
					last_increased_tech = "tech_lasers_5"
					last_increased_tech = "tech_lasers_4"
					last_increased_tech = "tech_lasers_3"
					last_increased_tech = "tech_lasers_2"
					last_increased_tech = "tech_lasers_1"
				}				
				
			}	
			country_event = { id = ex_technology_events.1401 }
		}					
		#Mass Drivers
		if = {
			limit = {
				OR = {
					last_increased_tech = "nhsc_tech_mass_driver_7"	
					last_increased_tech = "nhsc_tech_mass_driver_6"	
					last_increased_tech = "tech_mass_drivers_5"
					last_increased_tech = "tech_mass_drivers_4"
					last_increased_tech = "tech_mass_drivers_3"
					last_increased_tech = "tech_mass_drivers_2"						
				}								
			}	
			country_event = { id = ex_technology_events.1501 }
		}							
		#Missiles
		if = {
			limit = {
				OR = {
					last_increased_tech = "nhsc_tech_missile_7"
					last_increased_tech = "nhsc_tech_missile_6"
					last_increased_tech = "tech_missiles_5"
					last_increased_tech = "tech_missiles_4"
					last_increased_tech = "tech_missiles_3"
					last_increased_tech = "tech_missiles_2"		
				}								
			}	
			country_event = { id = ex_technology_events.1601 }
		}									
		#Kinetic Artillery
		if = {
			limit = {
				OR = { 
					last_increased_tech = "nhsc_tech_kinetic_artillery_3"
					last_increased_tech = "tech_kinetic_artillery_2"
					last_increased_tech = "tech_kinetic_artillery_1"
				}
			}	
			country_event = { id = ex_technology_events.1701 }
		}				
		#Flak
		if = {
			limit = {
				OR = { 
					last_increased_tech = "nhsc_tech_flak_battery_3"
					last_increased_tech = "tech_flak_batteries_2"					
				}	
			}	
			country_event = { id = ex_technology_events.1801 }
		}			
		#Swarmer Missiles
		if = {
			limit = {
				OR = { 
					last_increased_tech = "nhsc_tech_swarmermissiles_3"
					last_increased_tech = "tech_swarmer_missiles_2"
					last_increased_tech = "tech_swarmer_missiles_1"
				}
			}	
			country_event = { id = ex_technology_events.1901 }
		}					
		#Energy Torpedoes
		if = {
			limit = {
				OR = { 
					last_increased_tech = "nhsc_tech_energy_torpedo_3"
					last_increased_tech = "tech_energy_torpedoes_2"
				}
			}	
			country_event = { id = ex_technology_events.2001 }
		}									
		#Energy Lance
		if = {
			limit = {
				OR = { 
					last_increased_tech = "nhsc_tech_energy_lance_3"
					last_increased_tech = "tech_energy_lance_2"
				}
			}	
			country_event = { id = ex_technology_events.2101 }
		}	
		#Arc Emitter
		if = {
			limit = {
				OR = { 
					last_increased_tech = "nhsc_tech_arc_emitter_3"
					last_increased_tech = "tech_arc_emitter_2"
				}						
			}	
			country_event = { id = ex_technology_events.2201 }
		}		
		#Mass Accelerators
		if = {
			limit = {
				OR = { 
					last_increased_tech = "nhsc_tech_mass_accelerator_3"
					last_increased_tech = "tech_mass_accelerator_2"
				}
			}	
			country_event = { id = ex_technology_events.2301 }
		}
		#Exotic Missiles
		if = {
			limit = {
				last_increased_tech = "nhsc_tech_xmissile2"
			}	
			country_event = { id = ex_technology_events.2401 }
		}	
		#Autocannons
		if = {
			limit = {
				OR = { 
					last_increased_tech = "nhsc_tech_autocannon_4"
					last_increased_tech = "tech_autocannons_3"
					last_increased_tech = "tech_autocannons_2"
				}
			}	
			country_event = { id = ex_technology_events.2601 }
		}		
		#Torpedoes
		if = {
			limit = {
				OR = { 
					last_increased_tech = "nhsc_tech_torpedoes_4"
					last_increased_tech = "tech_torpedoes_3"
					last_increased_tech = "tech_torpedoes_2"
				}
			}	
			country_event = { id = ex_technology_events.2701 }
		}		
		#Strike Craft
		if = {
			limit = {
				OR = { 
					last_increased_tech = "tech_strike_craft_3"
					last_increased_tech = "tech_strike_craft_2"
					last_increased_tech = "nhsc_tech_tactical_strikecrafts"
					last_increased_tech = "nhsc_tech_siegebreaker_strikecrafts"
				}
			}	
			country_event = { id = ex_technology_events.2801 }
		}		
		#Plasma Weapons
		if = {
			limit = {
				OR = { 
					last_increased_tech = "nhsc_tech_plasma_4"
					last_increased_tech = "tech_plasma_3"
					last_increased_tech = "tech_plasma_2"
				}
			}	
			country_event = { id = ex_technology_events.2901 }
		}
		#Disruptor Weapons
		if = {
			limit = {
				OR = { 
					last_increased_tech = "nhsc_tech_disruptors_4"
					last_increased_tech = "tech_disruptors_3"
					last_increased_tech = "tech_disruptors_2"
				}
			}	
			country_event = { id = ex_technology_events.3001 }
		}		
		#Point Defense
		if = {
			limit = {
				OR = { 
					last_increased_tech = "nhsc_tech_lightstorm_pd"
					last_increased_tech = "nhsc_tech_pd_tracking_4"
					last_increased_tech = "tech_pd_tracking_3"
					last_increased_tech = "tech_pd_tracking_2"
				}
			}	
			country_event = { id = ex_technology_events.3101 }
		}	
		#Special Weapons (Standard)
		if = {
			limit = {
				OR = { 
					last_increased_tech = "nhsc_tech_plasma_missiles_2"
					last_increased_tech = "nhsc_tech_plasma_missiles_1"
					last_increased_tech = "nhsc_tech_torpedo_nano"
					last_increased_tech = "nhsc_tech_psilance"
				}
			}	
			country_event = { id = ex_technology_events.3201 }
		}	
	}	
}

#event = {
#	id = ex_technology_hub_event.2
#	hide_window = yes
#	is_triggered_only = yes
#
#	immediate = {
#		every_country = {
#			if = {
#				limit = {
#					OR = {
#						AND = {
#							NOT = { has_global_flag = "has_nsc_active" }
#							OR = {
#								has_technology = "tech_dragon_armor"
#								has_technology = "tech_ship_armor_5"
#								has_technology = "tech_ship_armor_4"
#								has_technology = "tech_ship_armor_3"
#								has_technology = "tech_ship_armor_2"
#							}
#						}	
#						AND = {
#							has_global_flag = "has_nsc_active"
#							OR = {
#								has_technology = "tech_ship_armor_10_NSC"
#								has_technology = "tech_ship_armor_9_NSC"
#								has_technology = "tech_ship_armor_8_NSC"
#								has_technology = "tech_ship_armor_7_NSC"
#								has_technology = "tech_ship_armor_6_NSC"									
#								has_technology = "tech_dragon_armor"
#								has_technology = "tech_ship_armor_5"
#								has_technology = "tech_ship_armor_4"
#								has_technology = "tech_ship_armor_3"
#								has_technology = "tech_ship_armor_2"
#							}
#						}					
#					}	
#				}	
#				country_event = { id = ex_technology_events.1 }
#			}
#			if = {
#				limit = {
#					OR = {
#						AND = {
#							NOT = { has_global_flag = "has_nsc_active" }
#							OR = {
#								has_technology = "tech_shields_5"
#								has_technology = "tech_shields_4"
#								has_technology = "tech_shields_3"
#								has_technology = "tech_shields_2"
#							}
#						}
#						AND = {
#							has_global_flag = "has_nsc_active"
#							OR = {
#								has_technology = "tech_shields_10_NSC"
#								has_technology = "tech_shields_9_NSC"
#								has_technology = "tech_shields_8_NSC"
#								has_technology = "tech_shields_7_NSC"
#								has_technology = "tech_shields_6_NSC"						
#								has_technology = "tech_shields_5"
#								has_technology = "tech_shields_4"
#								has_technology = "tech_shields_3"
#								has_technology = "tech_shields_2"
#							}
#						}					
#					}					
#				}	
#				country_event = { id = ex_technology_events.2 }
#			}		
#			if = {
#				limit = {
#					OR = {
#						AND = {
#							has_global_flag = "has_nsc_active"
#							OR = {						
#								has_technology = "tech_thrusters_5_NSC"
#								has_technology = "tech_thrusters_4"
#								has_technology = "tech_thrusters_3"
#								has_technology = "tech_thrusters_2"
#							}	
#						}				
#						AND = {
#							has_global_flag = "ex_components_active_active"				
#							OR = {						
#								has_technology = "tech_thrusters_5"
#								has_technology = "tech_thrusters_4"
#								has_technology = "tech_thrusters_3"
#								has_technology = "tech_thrusters_2"
#							}	
#						}
#						AND = {
#							NOR = { 
#								has_global_flag = "ex_components_active_active"
#								has_global_flag = "has_nsc_active"
#							}
#							OR = {						
#								has_technology = "tech_thrusters_4"
#								has_technology = "tech_thrusters_3"
#								has_technology = "tech_thrusters_2"
#							}
#						}					
#					}
#				}	
#				country_event = { id = ex_technology_events.3 }
#			}				
#			if = {
#				limit = {
#					OR = {
#						AND = {
#							has_global_flag = "has_nsc_active"
#							OR = {										
#								has_technology = "tech_sensors_6_NSC"
#								has_technology = "tech_sensors_5_NSC"
#								has_technology = "tech_sensors_4"
#								has_technology = "tech_sensors_3"
#							}
#						}
#						AND = {
#							has_global_flag = "ex_components_active_active"				
#							OR = {												
#								has_technology = "tech_sensors_4"
#								has_technology = "tech_sensors_3"
#								has_technology = "tech_sensors_2"
#							}
#						}
#						AND = {
#							NOR = { 
#								has_global_flag = "ex_components_active_active"
#								has_global_flag = "has_nsc_active"
#							}
#							OR = {						
#								has_technology = "tech_sensors_4"
#								has_technology = "tech_sensors_3"
#							}
#						}
#					}
#					
#				}	
#				country_event = { id = ex_technology_events.4 }
#			}	
#			if = {
#				limit = {
#					OR = {
#						AND = {
#							OR = {
#								has_global_flag = "ex_components_active_active"
#								has_global_flag = "ex_ships_and_stations_active"
#							}	
#							OR = { 
#								has_technology = "tech_enigmatic_decoder"
#								has_technology = "tech_enigmatic_encoder"					
#								has_technology = "tech_combat_computers_3"
#								has_technology = "tech_combat_computers_2"					
#							}	
#						}
#						AND = {
#							NOR = { 
#								has_global_flag = "ex_ships_and_stations_active"
#								has_global_flag = "ex_components_active_active"
#							}												
#							OR = {
#								has_technology = "tech_combat_computers_3"
#								has_technology = "tech_combat_computers_2"
#							}	
#						}
#					}	
#				}	
#				country_event = { id = ex_technology_events.5 }
#			}						
#			if = {
#				limit = {
#					AND = {
#						has_global_flag = "ex_components_active_active"
#						OR = { 
#							has_technology = "tech_regenerative_hull_tissue_3"
#							has_technology = "tech_regenerative_hull_tissue_2"
#						}
#					}	
#				}	
#				country_event = { id = ex_technology_events.6 }
#			}										
#			if = {
#				limit = {
#					AND = {
#						has_global_flag = "ex_components_active_active"
#						OR = { 
#							has_technology = "tech_hull_plating_3"
#							has_technology = "tech_hull_plating_2"		
#							has_technology = "tech_crystal_armor_2"	
#							has_technology = "tech_crystal_armor_1"	
#						}
#					}	
#				}	
#				country_event = { id = ex_technology_events.7 }
#			}								
#			if = {
#				limit = {
#					OR = { 
#						has_technology = "tech_crystal_armor_2"	
#						AND = { 
#							has_technology = "tech_crystal_armor_1"
#							has_global_flag = "ex_components_active_active"				
#						}	
#					}	
#				}	
#				country_event = { id = ex_technology_events.8 }
#			}										
#			if = {
#				limit = {
#					OR = {
#						AND = {
#							has_global_flag = "ex_components_active_active"
#							OR = { 
#								has_technology = "tech_afterburners_3"
#								has_technology = "tech_afterburners_2"
#							}	
#						}
#						AND = {
#							NOT = { has_global_flag = "ex_components_active_active" }
#							has_technology = "tech_afterburners_2"							
#						}					
#					}	
#				}	
#				country_event = { id = ex_technology_events.9 }
#			}				
#			if = {
#				limit = {
#					AND = {
#						has_global_flag = "ex_components_active_active"
#						OR = { 
#							has_technology = "tech_shield_rechargers_3"
#							has_technology = "tech_shield_rechargers_2"
#						}
#					}	
#				}	
#				country_event = { id = ex_technology_events.10 }
#			}							
#			if = {
#				limit = {
#					AND = {
#						has_global_flag = "ex_ships_and_stations_active"
#						OR = { 
#							has_technology = "tech_ship_architecture_5"
#							has_technology = "tech_ship_architecture_4"
#							has_technology = "tech_ship_architecture_3"
#							has_technology = "tech_ship_architecture_2"						
#						}
#					}	
#				}	
#				country_event = { id = ex_technology_events.11 }
#			}				
#			if = {
#				limit = {
#					AND = {
#						has_global_flag = "ex_ships_and_stations_active"
#						OR = { 
#							has_technology = "tech_station_architecture_5"
#							has_technology = "tech_station_architecture_4"
#							has_technology = "tech_station_architecture_3"
#							has_technology = "tech_station_architecture_2"					
#						}
#					}	
#				}	
#				country_event = { id = ex_technology_events.12 }
#			}						
#			if = {
#				limit = {
#					OR = {
#						AND = {
#							NOT = { has_global_flag = "has_nsc_active" }
#							OR = {
#								has_technology = tech_fusion_power
#								has_technology = tech_cold_fusion_power
#								has_technology = tech_antimatter_power
#								has_technology = tech_zero_point_power						
#							}	
#						}
#						AND = {
#							has_global_flag = "has_nsc_active"
#							OR = {
#								has_technology = tech_fusion_power
#								has_technology = tech_cold_fusion_power
#								has_technology = tech_antimatter_power
#								has_technology = tech_zero_point_power
#								has_technology = tech_graviton_power
#								has_technology = tech_dark_matter_power
#								has_technology = tech_adv_zero_point_power
#								has_technology = tech_multidimensional_power
#								has_technology = tech_nsc_zero_point_power		
#							}
#						}					
#					}					
#				}	
#				country_event = { id = ex_technology_events.13 }
#			}			
#			if = {
#				limit = {
#					OR = {
#						has_technology = "tech_lasers_5"
#						has_technology = "tech_lasers_4"
#						has_technology = "tech_lasers_3"
#						has_technology = "tech_lasers_2"
#					}				
#					
#				}	
#				country_event = { id = ex_technology_events.14 }
#			}					
#			if = {
#				limit = {
#					OR = {
#						has_technology = "tech_mass_drivers_5"
#						has_technology = "tech_mass_drivers_4"
#						has_technology = "tech_mass_drivers_3"
#						has_technology = "tech_mass_drivers_2"						
#					}								
#				}	
#				country_event = { id = ex_technology_events.15 }
#			}							
#			if = {
#				limit = {
#					OR = {
#						has_technology = "tech_missiles_5"
#						has_technology = "tech_missiles_4"
#						has_technology = "tech_missiles_3"
#						has_technology = "tech_missiles_2"				
#					}								
#				}	
#				country_event = { id = ex_technology_events.16 }
#			}									
#			if = {
#				limit = {
#					AND = {
#						has_global_flag = "ex_components_active_active"
#						OR = { 
#							has_technology = "tech_kinetic_artillery_2"
#							has_technology = "tech_kinetic_artillery_1"
#						}
#					}	
#				}	
#				country_event = { id = ex_technology_events.17 }
#			}				
#			if = {
#				limit = {
#					OR = { 
#						AND = {
#							has_global_flag = "ex_components_active_active"
#							OR = { 
#								has_technology = "tech_flak_batteries_3"
#								has_technology = "tech_flak_batteries_2"
#							}	
#						}
#						AND = {
#							NOT = { has_global_flag = "ex_components_active_active" }
#							has_technology = "tech_flak_batteries_2"
#						}					
#					}	
#				}	
#				country_event = { id = ex_technology_events.18 }
#			}			
#			if = {
#				limit = {
#					OR = { 
#						AND = {
#							has_global_flag = "ex_components_active_active"
#							OR = { 
#								has_technology = "tech_swarmer_missiles_3"
#								has_technology = "tech_swarmer_missiles_2"
#							}	
#						}
#						AND = {
#							NOT = { has_global_flag = "ex_components_active_active" }
#							has_technology = "tech_swarmer_missiles_2"
#						}					
#					}	
#				}	
#				country_event = { id = ex_technology_events.19 }
#			}					
#			if = {
#				limit = {
#					OR = { 
#						AND = {
#							has_global_flag = "ex_components_active_active"
#							OR = { 
#								has_technology = "tech_energy_torpedoes_3"
#								has_technology = "tech_energy_torpedoes_2"
#							}	
#						}
#						AND = {
#							NOT = { has_global_flag = "ex_components_active_active" }
#							has_technology = "tech_energy_torpedoes_2"
#						}					
#					}	
#				}	
#				country_event = { id = ex_technology_events.20 }
#			}								
#			if = {
#				limit = {
#					OR = { 
#						AND = {
#							has_global_flag = "has_nsc_active"
#							OR = { 
#								has_technology = "tech_energy_lance_NSC"
#								has_technology = "tech_energy_lance_2"
#							}	
#						}				
#						AND = {
#							has_global_flag = "ex_components_active_active"
#							OR = { 
#								has_technology = "tech_energy_lance_3"
#								has_technology = "tech_energy_lance_2"
#							}	
#						}
#						AND = {
#							NOT = { has_global_flag = "ex_components_active_active" }
#							has_technology = "tech_energy_lance_2"
#						}					
#					}	
#				}	
#				country_event = { id = ex_technology_events.21 }
#			}	
#			if = {
#				limit = {
#					OR = { 
#						AND = {
#							has_global_flag = "has_nsc_active"
#							OR = { 
#								has_technology = "tech_arc_emitter_NSC"
#								has_technology = "tech_arc_emitter_2"
#							}	
#						}				
#						AND = {
#							has_global_flag = "ex_components_active_active"
#							OR = { 
#								has_technology = "tech_arc_emitter_3"
#								has_technology = "tech_arc_emitter_2"
#							}	
#						}
#						AND = {
#							NOT = { has_global_flag = "ex_components_active_active" }
#							has_technology = "tech_arc_emitter_2"
#						}					
#					}	
#				}	
#				country_event = { id = ex_technology_events.22 }
#			}		
#			if = {
#				limit = {
#					OR = { 
#						AND = {
#							has_global_flag = "has_nsc_active"
#							OR = { 
#								has_technology = "tech_mass_accelerator_NSC"
#								has_technology = "tech_mass_accelerator_2"
#							}	
#						}				
#						AND = {
#							has_global_flag = "ex_components_active_active"
#							OR = { 
#								has_technology = "tech_mass_accelerator_3"
#								has_technology = "tech_mass_accelerator_2"
#							}	
#						}
#						AND = {
#							NOT = { has_global_flag = "ex_components_active_active" }
#							has_technology = "tech_mass_accelerator_2"
#						}					
#					}	
#				}	
#				country_event = { id = ex_technology_events.23 }
#			}
#			if = {
#				limit = {
#					AND = {
#						has_global_flag = "ex_components_active_active"
#						OR = { 
#							has_technology = "tech_xl_missile_3"
#							has_technology = "tech_xl_missile_2"
#						}	
#					}
#				}	
#				country_event = { id = ex_technology_events.24 }
#			}
#			if = {
#				limit = {
#					AND = {
#						has_global_flag = "ex_components_active_active"
#						OR = { 
#							has_technology = "tech_xl_disruptor_3"
#							has_technology = "tech_xl_disruptor_2"
#						}	
#					}
#				}	
#				country_event = { id = ex_technology_events.25 }
#			}	
#			if = {
#				limit = {
#					OR = { 
#						has_technology = "tech_autocannons_3"
#						has_technology = "tech_autocannons_2"
#					}
#				}	
#				country_event = { id = ex_technology_events.26 }
#			}		
#			if = {
#				limit = {
#					OR = { 
#						has_technology = "tech_torpedoes_3"
#						has_technology = "tech_torpedoes_2"
#					}
#				}	
#				country_event = { id = ex_technology_events.27 }
#			}		
#			if = {
#				limit = {
#					OR = { 
#						has_technology = "tech_strike_craft_3"
#						has_technology = "tech_strike_craft_2"
#					}
#				}	
#				country_event = { id = ex_technology_events.28 }
#			}		
#			if = {
#				limit = {
#					OR = { 
#						has_technology = "tech_plasma_3"
#						has_technology = "tech_plasma_2"
#					}
#				}	
#				country_event = { id = ex_technology_events.29 }
#			}
#			if = {
#				limit = {
#					OR = { 
#						has_technology = "tech_disruptors_3"
#						has_technology = "tech_disruptors_2"
#					}
#				}	
#				country_event = { id = ex_technology_events.30 }
#			}		
#			if = {
#				limit = {
#					OR = { 
#						has_technology = "tech_pd_tracking_3"
#						has_technology = "tech_pd_tracking_2"
#					}
#				}	
#				country_event = { id = ex_technology_events.31 }
#			}		
#			if = {
#				limit = {
#					AND = {
#						has_global_flag = "ex_components_active_active"
#						OR = { 
#							has_technology = tech_blue_crystal_weapon_1
#							has_technology = tech_green_crystal_weapon_1
#							has_technology = tech_yellow_crystal_weapon_1
#							has_technology = tech_red_crystal_weapon_1
#						}	
#					}
#				}	
#				country_event = { id = ex_technology_events.32 }
#			}		
#		}	
#	}
#}	