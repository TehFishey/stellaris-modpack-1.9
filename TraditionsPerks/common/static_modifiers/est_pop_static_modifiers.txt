##########################################################################
# Modifiers
##########################################################################

#Aesthetics
mod_est_aesthetics_4 = {	
	pop_resource_output = 0.15
}

#Cybernetics
mod_est_cybernetics_1_robot = { # Modded penalty is -0.30, reduced to -0.10
	tile_resource_energy_mult = 0.20 
}
mod_est_cybernetics_1_droid = { # Modded penalty is -0.15, reduced to -0.05
	tile_resource_energy_mult = 0.10
}
mod_est_cybernetics_1_synthetic = {
	tile_resource_energy_mult = 0.05
}
mod_est_cybernetics_1_synthetic_ai_servitude = {
	tile_resource_energy_mult = 0.1
}
mod_est_cybernetics_1_synthetic_ai_outlawed = {
	tile_resource_energy_mult = 0.15
}
mod_est_cybernetics_2_robot = { # Modded penalty is -1.00, reduced to -0.15
	tile_resource_engineering_research_mult = 0.85
	tile_resource_physics_research_mult = 0.85
	tile_resource_society_research_mult = 0.85
}
mod_est_cybernetics_2_droid = { # Modded penalty is -1.00, reduced to -0.05
	tile_resource_engineering_research_mult = 0.95
	tile_resource_physics_research_mult = 0.95
	tile_resource_society_research_mult = 0.95
}
mod_est_cybernetics_2_synthetic = {
	tile_resource_engineering_research_mult = 0.05
	tile_resource_physics_research_mult = 0.05
	tile_resource_society_research_mult = 0.05
}
mod_est_cybernetics_2_synthetic_ai_servitude = {
	tile_resource_engineering_research_mult = 0.15
	tile_resource_physics_research_mult = 0.15
	tile_resource_society_research_mult = 0.15
}
mod_est_cybernetics_2_synthetic_ai_outlawed = {
	tile_resource_engineering_research_mult = 0.30
	tile_resource_physics_research_mult = 0.30
	tile_resource_society_research_mult = 0.30
}
mod_est_cybernetics_5_slave = {
	pop_happiness = 0.1
}
mod_est_cybernetics_5_chemical_bliss = {
	pop_happiness = 0.2
}

#Malice
mod_est_malice_1 = {
	pop_consumer_goods_mult = -0.5
}
mod_est_malice_2 = {
	pop_resettlement_cost_mult = -0.25
}

#Justice

mod_est_justice_finish = {	
	pop_resource_output = 0.05
}

#Piracy
mod_est_piracy_5 = {
	pop_happiness = 0.05
}