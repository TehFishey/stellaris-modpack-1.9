namespace = est_foundation

### FOUNDATION ###

#Foundation adopt, 1 and finish. Bonus to core planets.
event = {
	id = est_foundation.1
	hide_window = yes
	is_triggered_only = yes
	
	trigger = {
		has_global_flag = est_foundation_adopt_picked_flag	
	}

	immediate = { 
		every_country = {
			limit =  {
				has_non_swapped_tradition = tr_est_foundation_adopt
			}			
			if = {
				limit = {
					Not = {
						has_non_swapped_tradition = tr_est_foundation_1					
					}	
				}
				every_owned_planet = {
					if = {
						limit = {
							sector_controlled = yes 
						}
						if = {
							limit = {
								has_modifier = mod_est_foundation_adopt	
							}
							remove_modifier = "mod_est_foundation_adopt"
						}
						else = {
							if = {
								limit = {
									Not = {
										has_modifier = mod_est_foundation_adopt	
									}
								}
								add_modifier = {
									modifier = "mod_est_foundation_adopt"
								}
							}						
						}
					}
				}	
				else = {
					if = {
						limit = {
							Not = {
								has_non_swapped_tradition = tr_est_foundation_finish					
							}	
						}	
						every_owned_planet = {
							if = {
								limit = {
									sector_controlled = yes 
								}
								if = {
									limit = {
										has_modifier = mod_est_foundation_1
									}
									remove_modifier = "mod_est_foundation_1"
								}
								else = {
									if = {
										limit = {
											Not = {
												has_modifier = mod_est_foundation_1	
											}
										}
										add_modifier = {
											modifier = "mod_est_foundation_1"
										}
									}						
								}
							}
						}		
						else = {		
							every_owned_planet = {
								if = {
									limit = {
										sector_controlled = yes 
									}
									if = {
										limit = {
											has_modifier = mod_est_foundation_finish
										}
										remove_modifier = "mod_est_foundation_finish"
									}
									else = {
										if = {
											limit = {
												Not = {
													has_modifier = mod_est_foundation_finish	
												}
											}
											add_modifier = {
												modifier = "mod_est_foundation_finish"
											}
										}						
									}
								}
							}		
						}
					}
				}
			}				
		}		
	}
}

#Foundation 2, bonus to capital.
event = {
	id = est_foundation.2
	hide_window = yes
	is_triggered_only = yes
	
	trigger = {
		has_global_flag = est_foundation_2_picked_flag		
	}
	
	immediate = { 
		every_country = {	
			limit =  {
				has_non_swapped_tradition = tr_est_foundation_2
			}	
			capital_scope = {		
				if = {
					limit = {
						owner = {
							has_technology = tech_galactic_administration
						}
					}	
					if = {
						limit = {
							Not = {
								has_modifier = mod_est_foundation_2_3
							}
						}
						add_modifier = {
							modifier = "mod_est_foundation_2_3"
						}	
						owner = {
							country_event = {
								id = est_foundation.3
							}
						}
					}
					else = {
						if = {
							limit = {
								owner = {
									has_technology = tech_colonial_centralization
								}
							}
							if = {
								limit = {
									Not = {
										has_modifier = mod_est_foundation_2_2
									}
								}
								add_modifier = {
									modifier = "mod_est_foundation_2_2"
								}	
								owner = {
									country_event = {
										id = est_foundation.3
									}
								}
							}
							else = {
								if = {
									limit = {
										Not = {
											has_modifier = mod_est_foundation_2_1
										}
									}
									add_modifier = {
										modifier = "mod_est_foundation_2_1"
									}	
									owner = {
										country_event = {
											id = est_foundation.3
										}
									}
								}
							}
						}
					}
				}
			}
        }
	}
}

#Foundation 2, we have changed capital, remove bonus from old capital.
country_event = {
	id = est_foundation.3
	hide_window = yes
	is_triggered_only = yes
	
	immediate = { 
		every_owned_planet = {
			limit = {
				is_capital = no
				Or = {
					has_modifier = mod_est_foundation_2_1
					has_modifier = mod_est_foundation_2_2
					has_modifier = mod_est_foundation_2_3
				}
			}
			switch = {
				trigger = has_modifier
				
				mod_est_foundation_2_1 = { remove_modifier = "mod_est_foundation_2_1" }	
				mod_est_foundation_2_2 = { remove_modifier = "mod_est_foundation_2_2" }	
				mod_est_foundation_2_3 = { remove_modifier = "mod_est_foundation_2_3" }	
			}
		}
	}
}

#Foundation 2, update bonus to capital with tech.
country_event = {
	id = est_foundation.4
	hide_window = yes
	is_triggered_only = yes
	
	trigger = {
		has_non_swapped_tradition = tr_est_foundation_2
	}
	
	immediate = { 
		if = {
			limit = {
				last_increased_tech = tech_galactic_administration
			}
			capital_scope = {
				if = {
					limit = {
						has_modifier = mod_est_foundation_2_2
					}				
					remove_modifier = "mod_est_foundation_2_2"
					else = {
						owner = {
							country_event = {
								id = est_foundation.3
							}
						}
					}
				}
				add_modifier = {
					modifier = "mod_est_foundation_2_3"
				}					
			}
		}
		if = {
			limit = {
				last_increased_tech = tech_colonial_centralization
			}
			capital_scope = {
				if = {
					limit = {
						has_modifier = mod_est_foundation_2_1
					}				
					remove_modifier = "mod_est_foundation_2_1"
					else = {
						owner = {
							country_event = {
								id = est_foundation.3
							}
						}
					}
				}
				add_modifier = {
					modifier = "mod_est_foundation_2_2"
				}					
			}
		}
	}
}

#Foundation 5, gives naval capacity form core worlds
event = {
	id = est_foundation.5
	hide_window = yes
	is_triggered_only = yes
	
	trigger = {
		has_global_flag = est_foundation_5_picked_flag
	}

	immediate = {
		every_country = {
			limit = {			
				has_non_swapped_tradition = tr_est_foundation_5
			}
			country_event = {
				id = est_foundation.6
			}
			set_variable = {
				which = var_est_foundation_5_count
				value = 0
			}
			every_owned_planet = {
				limit = {
					sector_controlled = no
					has_spaceport = yes
				}
				owner = {
					change_variable = {
						which = var_est_foundation_5_count
						value = 1
					}
				}
				if = {
					limit = {
						Or = {
							And = {
								num_spaceport_modules = 6
								num_free_spaceport_module_slots = 0
							}
							And = {
								num_spaceport_modules = 5
								num_free_spaceport_module_slots = 1
							}
							And = {
								num_spaceport_modules = 4
								num_free_spaceport_module_slots = 2
							}
							And = {
								num_spaceport_modules = 3
								num_free_spaceport_module_slots = 3
							}
							And = {
								num_spaceport_modules = 2
								num_free_spaceport_module_slots = 4
							}
							And = {
								num_spaceport_modules = 1
								num_free_spaceport_module_slots = 5
							}
							And = {
								num_spaceport_modules = 0
								num_free_spaceport_module_slots = 6
							}
						}
					}
					owner = {
						change_variable = {
							which = var_est_foundation_5_count
							value = 1
						}
					}		
				}
			}
			country_event = {
				id = est_foundation.7
			}
		}
	}
}

#Foundation 5, removes old modifiers.
country_event = {
	id = est_foundation.6
	hide_window = yes
	is_triggered_only = yes
	
	immediate = {
		if = {
			limit = {
				check_variable = { 
					which = var_est_foundation_5_count
					value < 13
				}
			}
			if = {
				limit = {
					check_variable = { 
						which = var_est_foundation_5_count
						value < 9
					}
				}
				if = {
					limit = {
						check_variable = { 
							which = var_est_foundation_5_count
							value < 5
						}
					}
					switch = {
						trigger = has_modifier
						mod_est_foundation_5_1 = { remove_modifier = "mod_est_foundation_5_1" }
						mod_est_foundation_5_2 = { remove_modifier = "mod_est_foundation_5_2" }
						mod_est_foundation_5_3 = { remove_modifier = "mod_est_foundation_5_3" }
						mod_est_foundation_5_4 = { remove_modifier = "mod_est_foundation_5_4" }
					}
					else = {
						switch = {
							trigger = has_modifier
							mod_est_foundation_5_5 = { remove_modifier = "mod_est_foundation_5_5" }
							mod_est_foundation_5_6 = { remove_modifier = "mod_est_foundation_5_6" }
							mod_est_foundation_5_7 = { remove_modifier = "mod_est_foundation_5_7" }
							mod_est_foundation_5_8 = { remove_modifier = "mod_est_foundation_5_8" }
						}
					}
				}
				else = {
					switch = {
						trigger = has_modifier
						mod_est_foundation_5_9 = { remove_modifier = "mod_est_foundation_5_9" }
						mod_est_foundation_5_10 = { remove_modifier = "mod_est_foundation_5_10" }
						mod_est_foundation_5_11 = { remove_modifier = "mod_est_foundation_5_11" }
						mod_est_foundation_5_12 = { remove_modifier = "mod_est_foundation_5_12" }
					}
				}
			}
			else = {
				if = {
					limit = {
						check_variable = { 
							which = var_est_foundation_5_count
							value < 17
						}
					}
					switch = {
						trigger = has_modifier
						mod_est_foundation_5_13 = { remove_modifier = "mod_est_foundation_5_13" }
						mod_est_foundation_5_14 = { remove_modifier = "mod_est_foundation_5_14" }
						mod_est_foundation_5_15 = { remove_modifier = "mod_est_foundation_5_15" }
						mod_est_foundation_5_16 = { remove_modifier = "mod_est_foundation_5_16" }
					}
					else = {
						switch = {
							trigger = has_modifier
							mod_est_foundation_5_17 = { remove_modifier = "mod_est_foundation_5_17" }
							mod_est_foundation_5_18 = { remove_modifier = "mod_est_foundation_5_18" }
							mod_est_foundation_5_19 = { remove_modifier = "mod_est_foundation_5_19" }
							mod_est_foundation_5_20 = { remove_modifier = "mod_est_foundation_5_20" }
						}
					}
				}
			}
		}
	}
}

#Foundation 5, sets the right modifier according to the variable value.
country_event = {
	id = est_foundation.7
	hide_window = yes
	is_triggered_only = yes
	
	immediate = {
		if = {
			limit = {
				check_variable = { 
					which = var_est_foundation_5_count
					value < 13
				}
			}
			if = {
				limit = {
					check_variable = { 
						which = var_est_foundation_5_count
						value < 9
					}
				}
				if = {
					limit = {
						check_variable = { 
							which = var_est_foundation_5_count
							value < 5
						}
					}
					if = {
						limit = {
							check_variable = { 
								which = var_est_foundation_5_count
								value = 1
							}
						}
						add_modifier = {
							modifier = "mod_est_foundation_5_1"
							days = -1
						}
						break = yes
					}
					if = {
						limit = {
							check_variable = { 
								which = var_est_foundation_5_count
								value = 2
							}
						}
						add_modifier = {
							modifier = "mod_est_foundation_5_2"
							days = -1
						}
						break = yes
					}
					if = {
						limit = {
							check_variable = { 
								which = var_est_foundation_5_count
								value = 3
							}
						}
						add_modifier = {
							modifier = "mod_est_foundation_5_3"
							days = -1
						}
						break = yes
					}
					if = {
						limit = {
							check_variable = { 
								which = var_est_foundation_5_count
								value = 4
							}
						}
						add_modifier = {
							modifier = "mod_est_foundation_5_4"
							days = -1
						}
						break = yes
					}
					else = {
						if = {
							limit = {
								check_variable = { 
									which = var_est_foundation_5_count
									value = 5
								}
							}
							add_modifier = {
								modifier = "mod_est_foundation_5_5"
								days = -1
							}
							break = yes
						}
						if = {
							limit = {
								check_variable = { 
									which = var_est_foundation_5_count
									value = 6
								}
							}
							add_modifier = {
								modifier = "mod_est_foundation_5_6"
								days = -1
							}
							break = yes
						}
						if = {
							limit = {
								check_variable = { 
									which = var_est_foundation_5_count
									value = 7
								}
							}
							add_modifier = {
								modifier = "mod_est_foundation_5_7"
								days = -1
							}
							break = yes
						}
						if = {
							limit = {
								check_variable = { 
									which = var_est_foundation_5_count
									value = 8
								}
							}
							add_modifier = {
								modifier = "mod_est_foundation_5_8"
								days = -1
							}
							break = yes
						}
					}
				}
				else = {
					if = {
						limit = {
							check_variable = { 
								which = var_est_foundation_5_count
								value = 9
							}
						}
						add_modifier = {
							modifier = "mod_est_foundation_5_9"
							days = -1
						}
						break = yes
					}
					if = {
						limit = {
							check_variable = { 
								which = var_est_foundation_5_count
								value = 10
							}
						}
						add_modifier = {
							modifier = "mod_est_foundation_5_10"
							days = -1
						}
						break = yes
					}
					if = {
						limit = {
							check_variable = { 
								which = var_est_foundation_5_count
								value = 11
							}
						}
						add_modifier = {
							modifier = "mod_est_foundation_5_11"
							days = -1
						}
						break = yes
					}
					if = {
						limit = {
							check_variable = { 
								which = var_est_foundation_5_count
								value = 12
							}
						}
						add_modifier = {
							modifier = "mod_est_foundation_5_12"
							days = -1
						}
						break = yes
					}
				}
			}
			else = {
				if = {
					limit = {
						check_variable = { 
							which = var_est_foundation_5_count
							value < 17
						}
					}
					if = {
						limit = {
							check_variable = { 
								which = var_est_foundation_5_count
								value = 13
							}
						}
						add_modifier = {
							modifier = "mod_est_foundation_5_13"
							days = -1
						}
						break = yes
					}
					if = {
						limit = {
							check_variable = { 
								which = var_est_foundation_5_count
								value = 14
							}
						}
						add_modifier = {
							modifier = "mod_est_foundation_5_14"
							days = -1
						}
						break = yes
					}
					if = {
						limit = {
							check_variable = { 
								which = var_est_foundation_5_count
								value = 15
							}
						}
						add_modifier = {
							modifier = "mod_est_foundation_5_15"
							days = -1
						}
						break = yes
					}
					if = {
						limit = {
							check_variable = { 
								which = var_est_foundation_5_count
								value = 16
							}
						}
						add_modifier = {
							modifier = "mod_est_foundation_5_16"
							days = -1
						}
						break = yes
					}
					else = {
						if = {
							limit = {
								check_variable = { 
									which = var_est_foundation_5_count
									value = 17
								}
							}
							add_modifier = {
								modifier = "mod_est_foundation_5_17"
								days = -1
							}
							break = yes
						}
						if = {
							limit = {
								check_variable = { 
									which = var_est_foundation_5_count
									value = 18
								}
							}
							add_modifier = {
								modifier = "mod_est_foundation_5_18"
								days = -1
							}
							break = yes
						}
						if = {
							limit = {
								check_variable = { 
									which = var_est_foundation_5_count
									value = 19
								}
							}
							add_modifier = {
								modifier = "mod_est_foundation_5_19"
								days = -1
							}
							break = yes
						}
						if = {
							limit = {
								check_variable = { 
									which = var_est_foundation_5_count
									value > 19
								}
							}
							add_modifier = {
								modifier = "mod_est_foundation_5_20"
								days = -1
							}
							break = yes
						}
					}
				}
			}
		}
	}
}			

#Foundation 5, update AI Naval Capacity Bonus
country_event = {
	id = est_foundation.8
	hide_window = yes
	is_triggered_only = yes
	
	trigger = {
		has_swapped_tradition = tr_est_foundation_5_AI
	}
	
	immediate = { 
		if = {
			limit = {
				last_increased_tech = tech_doctrine_fleet_size_1
			}
			add_modifier = {
				modifier = "mod_est_foundation_5_AI_1"
				days = -1				
			}
		}
		if = {
			limit = {
				last_increased_tech = tech_doctrine_fleet_size_2
			}
			remove_modifier = "mod_est_foundation_5_AI_1"
			add_modifier = {
				modifier = "mod_est_foundation_5_AI_2"
				days = -1				
			}
		}
		if = {
			limit = {
				last_increased_tech = tech_doctrine_fleet_size_3
			}
			remove_modifier = "mod_est_foundation_5_AI_2"
			add_modifier = {
				modifier = "mod_est_foundation_5_AI_3"
				days = -1				
			}
		}
		if = {
			limit = {
				last_increased_tech = tech_doctrine_fleet_size_4
			}
			remove_modifier = "mod_est_foundation_5_AI_3"
			add_modifier = {
				modifier = "mod_est_foundation_5_AI_4"
				days = -1				
			}
		}
		if = {
			limit = {
				last_increased_tech = tech_doctrine_fleet_size_5
			}
			remove_modifier = "mod_est_foundation_5_AI_4"
			add_modifier = {
				modifier = "mod_est_foundation_5_AI_5"
				days = -1				
			}
		}
	}
}


### CONFEDERACY ###

#Confederacy Adopt and 2, gives bonus to sector planets (that have a governon, in the case of 2)
event = {
	id = est_foundation.9
	hide_window = yes
	is_triggered_only = yes
	
	trigger = {	
		has_global_flag = est_confederacy_adopt_picked_flag
	}
	
	immediate = {
		every_country = {
			limit = {
				Or = {
					has_swapped_tradition = tr_est_confederacy_adopt
				}
			}
			every_owned_planet = {	
				if = {
					limit = {
						sector_controlled = yes 
					}
					if = {
						limit = {
							Nor = {
								sector = {
									exists = leader
								}
								owner = {
									has_swapped_tradition = tr_est_confederacy_2
								}
							}
						}
						if = {
							limit = {
								has_modifier = mod_est_confederacy_2								
							}
							remove_modifier = "mod_est_confederacy_2"
						}	
						if = {
							limit = {
								Not = {
									has_modifier = mod_est_confederacy_adopt
								}							
							}
							add_modifier = {
								modifier = "mod_est_confederacy_adopt"							
								days = -1
							}
						}
						else = {
							if = {
								limit = {
									has_modifier = mod_est_confederacy_adopt								
								}
								remove_modifier = "mod_est_confederacy_adopt"
							}	
							if = {
								limit = {
									Not = {
										has_modifier = mod_est_confederacy_2
									}							
								}
								add_modifier = {
									modifier = "mod_est_confederacy_2"							
									days = -1
								}
							}
						}
					}
					else = {
						if = {
							limit = {
								has_modifier = mod_est_confederacy_adopt								
							}
							remove_modifier = "mod_est_confederacy_adopt"
						}	
						if = {
							limit = {
								has_modifier = mod_est_confederacy_2								
							}
							remove_modifier = "mod_est_confederacy_2"
						}						
					}
				}
			}
		}
	}
}

#Confederacy 4, gives bonuses to subjects.
event = {
	id = est_foundation.10
	hide_window = yes
	is_triggered_only = yes
	
	trigger = {	
		has_global_flag = est_confederacy_4_picked_flag
	}
	immediate = {
		every_country = {
			if = {
				limit = {
					is_subject = yes
					overlord = {
						has_swapped_tradition = tr_est_confederacy_4	
					}
				}
				if = {
					limit = {
						Not = {
							has_modifier = mod_est_confederacy_4
						}
					}
					add_modifier = {
						modifier = "mod_est_confederacy_4"
						days = -1
					}
				}					
				else = {
					if = {
						limit = {
							has_modifier = mod_est_confederacy_4								
						}
						remove_modifier = "mod_est_confederacy_4"
					}					
				}
			}
		}	
	}
}
					
#Confederacy 5, reduce upkeep cost for Military Stations controlled by Sectors.
event = {
	id = est_foundation.11
	hide_window = yes
	is_triggered_only = yes
	
	trigger = {	
		has_global_flag = est_confederacy_5_picked_flag
	}
	
	immediate = {
		every_country = {
			limit = {
				has_swapped_tradition = tr_est_confederacy_5
			}
			every_owned_ship = {
				limit = {
					Or = {
						is_ship_size = military_station_small
						is_ship_size = military_station_medium
						is_ship_size = military_station_large
					}
				}
				if = {
					limit = {
						fleet = {
							sector_controlled = yes 		
						}
					}
					if = {
						limit = {
							Not = {
								has_modifier = mod_est_confederacy_5
							}
						}
						add_modifier = {
							modifier = "mod_est_confederacy_5"
							days = -1
						}
					}					
					else = {
						if = {
							limit = {
								has_modifier = mod_est_confederacy_5							
							}
							remove_modifier = "mod_est_confederacy_5"
						}					
					}
				}
			}
		}
	}
}

### SUBMINDS ###

#Subminds Adopt and 2, gives bonus to sector planets (that have a governon, in the case of 2)
event = {
	id = est_foundation.12
	hide_window = yes
	is_triggered_only = yes
	
	trigger = {	
		has_global_flag = est_subminds_adopt_picked_flag
	}
	
	immediate = {
		every_country = {
			limit = {
				Or = {
					has_swapped_tradition = tr_est_subminds_adopt
				}
			}
			every_owned_planet = {	
				if = {
					limit = {
						sector_controlled = yes 
					}
					if = {
						limit = {
							Nor = {
								sector = {
									exists = leader
								}
								owner = {
									has_swapped_tradition = tr_est_subminds_2
								}
							}
						}
						if = {
							limit = {
								has_modifier = mod_est_subminds_2								
							}
							remove_modifier = "mod_est_subminds_2"
						}	
						if = {
							limit = {
								Not = {
									has_modifier = mod_est_subminds_adopt
								}							
							}
							add_modifier = {
								modifier = "mod_est_subminds_adopt"							
								days = -1
							}
						}
						else = {
							if = {
								limit = {
									has_modifier = mod_est_subminds_adopt								
								}
								remove_modifier = "mod_est_subminds_adopt"
							}	
							if = {
								limit = {
									Not = {
										has_modifier = mod_est_subminds_2
									}							
								}
								add_modifier = {
									modifier = "mod_est_subminds_2"							
									days = -1
								}
							}
						}
					}
					else = {
						if = {
							limit = {
								has_modifier = mod_est_subminds_adopt								
							}
							remove_modifier = "mod_est_subminds_adopt"
						}	
						if = {
							limit = {
								has_modifier = mod_est_subminds_2								
							}
							remove_modifier = "mod_est_subminds_2"
						}						
					}
				}
			}
		}
	}
}

#Subminds 5, edict bonus for sector planets
event = {
	id = est_foundation.13
	hide_window = yes
	is_triggered_only = yes
	
	trigger = {	
		Or = {
			has_global_flag = est_subminds_3_picked_flag
			has_global_flag = est_subminds_3_machines_picked_flag
		}
	}
	
	immediate = {
		every_country = {
			limit = {
				Or = {
					has_swapped_tradition = tr_est_subminds_3
					has_swapped_tradition = tr_est_subminds_3_machines
				}
			}
			if = {
				limit = {
					has_edict = est_subminds_3_energy
				}
				if = {
					limit = {
						Not = {
							has_country_flag = esap_subminds_3_active_energy
						}
					}
					set_country_flag = esap_subminds_3_active_energy
					every_owned_planet = {
						limit = {
							sector_controlled = yes 						
						}
						add_modifier = {
							modifier = "mod_est_subminds_3_edict_energy"							
							days = -1
						}
					}
					else = {
						every_owned_planet = {
							if = {
								limit = {
									sector_controlled = yes 						
								}
								if = {
									limit = {
										Not = {
											has_modifier = mod_est_subminds_3_edict_energy
										}
									}
									add_modifier = {
										modifier = "mod_est_subminds_3_edict_energy"							
										days = -1
									}
								}
								else = {
									if = {
										limit = {
											has_modifier = mod_est_subminds_3_edict_energy
										}
										remove_modifier = "mod_est_subminds_3_edict_energy"		
									}
								}
							}
						
						}
					}
				}
				else = {
					if = {
						limit = {
							has_country_flag = esap_subminds_3_active_energy
						}
						every_owned_planet = {
							limit = {
								has_modifier = mod_est_subminds_3_edict_energy
							}
							remove_modifier = "mod_est_subminds_3_edict_energy"
						}	
						remove_country_flag = esap_subminds_3_active_energy				
					}					
				}
			}
			if = {
				limit = {
					has_edict = est_subminds_3_minerals
				}
				if = {
					limit = {
						Not = {
							has_country_flag = esap_subminds_3_active_minerals
						}
					}
					set_country_flag = esap_subminds_3_active_minerals
					every_owned_planet = {
						limit = {
							sector_controlled = yes 						
						}
						add_modifier = {
							modifier = "mod_est_subminds_3_edict_minerals"							
							days = -1
						}
					}
					else = {
						every_owned_planet = {
							if = {
								limit = {
									sector_controlled = yes 						
								}
								if = {
									limit = {
										Not = {
											has_modifier = mod_est_subminds_3_edict_minerals
										}
									}
									add_modifier = {
										modifier = "mod_est_subminds_3_edict_minerals"							
										days = -1
									}
								}
								else = {
									if = {
										limit = {
											has_modifier = mod_est_subminds_3_edict_minerals
										}
										remove_modifier = "mod_est_subminds_3_edict_minerals"		
									}
								}
							}
						
						}
					}
				}
				else = {
					if = {
						limit = {
							has_country_flag = esap_subminds_3_active_minerals
						}
						every_owned_planet = {
							limit = {
								has_modifier = mod_est_subminds_3_edict_minerals
							}
							remove_modifier = "mod_est_subminds_3_edict_minerals"
						}	
						remove_country_flag = esap_subminds_3_active_minerals				
					}					
				}
			}
			if = {
				limit = {
					has_edict = est_subminds_3_food
				}
				if = {
					limit = {
						Not = {
							has_country_flag = esap_subminds_3_active_food
						}
					}
					set_country_flag = esap_subminds_3_active_food
					every_owned_planet = {
						limit = {
							sector_controlled = yes 						
						}
						add_modifier = {
							modifier = "mod_est_subminds_3_edict_food"							
							days = -1
						}
					}
					else = {
						every_owned_planet = {
							if = {
								limit = {
									sector_controlled = yes 						
								}
								if = {
									limit = {
										Not = {
											has_modifier = mod_est_subminds_3_edict_food
										}
									}
									add_modifier = {
										modifier = "mod_est_subminds_3_edict_food"							
										days = -1
									}
								}
								else = {
									if = {
										limit = {
											has_modifier = mod_est_subminds_3_edict_food
										}
										remove_modifier = "mod_est_subminds_3_edict_food"		
									}
								}
							}
						
						}
					}
				}
				else = {
					if = {
						limit = {
							has_country_flag = esap_subminds_3_active_food
						}
						every_owned_planet = {
							limit = {
								has_modifier = mod_est_subminds_3_edict_food
							}
							remove_modifier = "mod_est_subminds_3_edict_food"
						}	
						remove_country_flag = esap_subminds_3_active_food				
					}					
				}
			}
			if = {
				limit = {
					has_edict = est_subminds_3_unity
				}
				if = {
					limit = {
						Not = {
							has_country_flag = esap_subminds_3_active_unity
						}
					}
					set_country_flag = esap_subminds_3_active_unity
					every_owned_planet = {
						limit = {
							sector_controlled = yes 						
						}
						add_modifier = {
							modifier = "mod_est_subminds_3_edict_unity"							
							days = -1
						}
					}
					else = {
						every_owned_planet = {
							if = {
								limit = {
									sector_controlled = yes 						
								}
								if = {
									limit = {
										Not = {
											has_modifier = mod_est_subminds_3_edict_unity
										}
									}
									add_modifier = {
										modifier = "mod_est_subminds_3_edict_unity"							
										days = -1
									}
								}
								else = {
									if = {
										limit = {
											has_modifier = mod_est_subminds_3_edict_unity
										}
										remove_modifier = "mod_est_subminds_3_edict_unity"		
									}
								}
							}
						
						}
					}
				}
				else = {
					if = {
						limit = {
							has_country_flag = esap_subminds_3_active_unity
						}
						every_owned_planet = {
							limit = {
								has_modifier = mod_est_subminds_3_edict_unity
							}
							remove_modifier = "mod_est_subminds_3_edict_unity"
						}	
						remove_country_flag = esap_subminds_3_active_unity				
					}					
				}
			}
			if = {
				limit = {
					has_edict = est_subminds_3_research
				}
				if = {
					limit = {
						Not = {
							has_country_flag = esap_subminds_3_active_research
						}
					}
					set_country_flag = esap_subminds_3_active_research
					every_owned_planet = {
						limit = {
							sector_controlled = yes 						
						}
						add_modifier = {
							modifier = "mod_est_subminds_3_edict_research"							
							days = -1
						}
					}
					else = {
						every_owned_planet = {
							if = {
								limit = {
									sector_controlled = yes 						
								}
								if = {
									limit = {
										Not = {
											has_modifier = mod_est_subminds_3_edict_research
										}
									}
									add_modifier = {
										modifier = "mod_est_subminds_3_edict_research"							
										days = -1
									}
								}
								else = {
									if = {
										limit = {
											has_modifier = mod_est_subminds_3_edict_research
										}
										remove_modifier = "mod_est_subminds_3_edict_research"		
									}
								}
							}
						
						}
					}
				}
				else = {
					if = {
						limit = {
							has_country_flag = esap_subminds_3_active_research
						}
						every_owned_planet = {
							limit = {
								has_modifier = mod_est_subminds_3_edict_research
							}
							remove_modifier = "mod_est_subminds_3_edict_research"
						}	
						remove_country_flag = esap_subminds_3_active_research				
					}					
				}
			}
		}
	}
}

#Subminds 5, reduce upkeep cost for Military Stations controlled by Sectors.
event = {
	id = est_foundation.14
	hide_window = yes
	is_triggered_only = yes
	
	trigger = {	
		has_global_flag = est_subminds_5_picked_flag
	}
	
	immediate = {
		every_country = {
			limit = {
				has_swapped_tradition = tr_est_subminds_5
			}
			every_owned_ship = {
				limit = {
					Or = {
						is_ship_size = military_station_small
						is_ship_size = military_station_medium
						is_ship_size = military_station_large
					}
				}
				if = {
					limit = {
						fleet = {
							sector_controlled = yes 		
						}
					}
					if = {
						limit = {
							Not = {
								has_modifier = mod_est_confederacy_5
							}
						}
						add_modifier = {
							modifier = "mod_est_confederacy_5"
							days = -1
						}
					}					
					else = {
						if = {
							limit = {
								has_modifier = mod_est_confederacy_5							
							}
							remove_modifier = "mod_est_confederacy_5"
						}					
					}
				}
			}
		}
	}
}
