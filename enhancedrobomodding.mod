name="*MOD* Enhanced Robomodding [1.9.*]"
path="mod/enhancedrobomodding"
tags={
	"Gameplay"
	"Species"
	"Balance"
}
picture="thumb.png"
supported_version="1.9.*"
