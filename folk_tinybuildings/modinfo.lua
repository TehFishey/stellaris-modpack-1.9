-- modinfo read by stlrel from http://github.com/stellaris-mods/scripts
return {
	path = "folk_tinybuildings",
	name = "Tiny Building Constructions",
	tags = { "Graphics", "Utilities" },
	picture = "thumb.png",
	supported_version = "1.8.*",
	remote_file_id = 803491311,
	readme = "readme.md",
	steambb = "steam.bbcode",
}
