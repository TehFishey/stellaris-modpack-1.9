# See traditions/README.txt for format

@ap_engineered_evolution_POINTS = 3
@ap_engineered_evolution_COST_MULT = -0.25
ap_engineered_evolution = {
	on_enabled = {
		add_research_option = tech_gene_seed_purification
		hidden_effect = {
			country_event = { id = utopia.2700 }
		}
	}
	possible = {
		custom_tooltip = {
			fail_text = "synthetic_empire_biological_ascension"
			NOT = { has_trait = trait_mechanical }
		}	
		custom_tooltip = {
			fail_text = "requires_ascension_perks_1"
			num_ascension_perks > 0
		}
		custom_tooltip = {
			fail_text = "requires_technology_gene_tailoring"
			has_technology = tech_gene_tailoring
		}
		custom_tooltip = {
			fail_text = "requires_not_ap_the_flesh_is_weak"
			NOT = { has_ascension_perk = ap_the_flesh_is_weak }
		}
		custom_tooltip = {
			fail_text = "requires_not_ap_mind_over_matter"
			NOT = { has_ascension_perk = ap_mind_over_matter }
		}
	}
	modifier = {
		description = ap_engineered_evolution_modifier_desc
		description_parameters = {
			POINTS = @ap_engineered_evolution_POINTS
			COST_MULT = @ap_engineered_evolution_COST_MULT
		}

		BIOLOGICAL_species_trait_points_add = @ap_engineered_evolution_POINTS

		modify_species_cost_mult = @ap_engineered_evolution_COST_MULT
	}
	
	potential = {
		NOT = {
			has_ascension_perk = ap_engineered_evolution
			has_authority = "auth_machine_intelligence"
		}
		is_mechanical_empire = no
		is_cyborg_empire = no
	}
	
	ai_weight = {
		factor = 20
	    modifier = { #it's really best for big empires
            factor = 0.5
            num_owned_planets < 5
        }
        modifier = {
            factor = 1.5
            count_owned_pops = {
                limit = { pop_has_trait = trait_hive_mind }
                count > 10
            }
        }
        modifier = { #they stand to gain the most
            factor = 3
            has_authority = auth_hive_mind
            NOT = { has_valid_civic = civic_hive_devouring_swarm }
        }
        modifier = { #xenophobes don't like gene modification
            factor = 0.5
            has_ethic = ethic_xenophobe
        }
        modifier = {
            factor = 0.2
            has_ethic = ethic_fanatic_xenophobe
        }
		modifier = {
			factor = 0
			OR = {
				has_ethic = ethic_spiritualist
				has_ethic = ethic_fanatic_spiritualist
				has_ethic = ethic_materialist
				has_ethic = ethic_fanatic_materialist
			}#have their own paths
		}
		modifier = {
			factor = 0
			NOT = {
				has_ascension_perk = ap_technological_ascendancy
			}
		}
	}
}

ap_the_flesh_is_weak = {
	on_enabled = {
		custom_tooltip = "flesh_is_weak_tooltip"
		hidden_effect = {
			country_event = { id = utopia.2500 }
		}
	}
	modifier = {
		pop_robot_upkeep_mult = -0.20
	}	
	possible = {
		custom_tooltip = {
			fail_text = "synthetic_empire_synthetic_ascension"
			NOT = { has_trait = trait_mechanical }
		}
		custom_tooltip = {
			fail_text = "hive_mind_biological_ascension_only2"
			NOT = { has_authority = auth_hive_mind }
		}
		custom_tooltip = {
			fail_text = "requires_ascension_perks_1"
			num_ascension_perks > 0
		}
		custom_tooltip = {
			fail_text = "requires_technology_droid_workers"
			has_technology = tech_droid_workers
		}
		custom_tooltip = {
			fail_text = "requires_not_ap_engineered_evolution"
			NOT = { has_ascension_perk = ap_engineered_evolution }
		}
		custom_tooltip = {
			fail_text = "requires_not_ap_mind_over_matter"
			NOT = { has_ascension_perk = ap_mind_over_matter }
		}
	}
	
	potential = {
		NOT = {
			has_ascension_perk = ap_the_flesh_is_weak
			has_authority = "auth_machine_intelligence"
		}
		is_mechanical_empire = no		
	}
	
	ai_weight = {
		factor = 20
		modifier = {
			factor = 0.1
			NOR = {
				has_ethic = ethic_materialist
				has_ethic = ethic_fanatic_materialist
			}
		}
		modifier = {    #common sense addition
            factor = 0
            has_policy_flag = robots_outlawed
        }
        modifier = {    #common sense addition
            factor = 0
            NOT = { has_policy_flag = ai_full_rights }
        }
        modifier = {
            factor = 0
            NOT = {
                any_owned_pop = {
                    is_robot_pop = yes
                }
            }
        }
        modifier = {
            factor = 2
            count_owned_pops = {
                limit = {
                    is_robot_pop = yes
                }
                count > 10
            }
        }
        modifier = {
            factor = 0
            OR = {
                has_ethic = ethic_spiritualist
                has_ethic = ethic_fanatic_spiritualist
            }
        }
        modifier = {
            factor = 2
            has_ethic = ethic_materialist
        }
        modifier = {
            factor = 3
            has_ethic = ethic_fanatic_materialist
        }
		modifier = {
			factor = 0
			NOT = {
				has_ascension_perk = ap_technological_ascendancy
			}
		}
	}
}

ap_mind_over_matter = {
	on_enabled = {
		custom_tooltip = "mind_over_matter_tooltip"
		hidden_effect = {
			country_event = { id = utopia.2600 }
		}
		add_research_option = tech_telepathy
	}
	possible = {	
		custom_tooltip = {
			fail_text = "synthetic_empire_psionic_ascension"
			NOT = { has_trait = trait_mechanical }
		}
		custom_tooltip = {
			fail_text = "hive_mind_biological_ascension_only1"
			NOT = { has_authority = auth_hive_mind }
		}
		custom_tooltip = {
			fail_text = "requires_ascension_perks_1"
			num_ascension_perks > 0
		}
		custom_tooltip = {
			fail_text = "requires_technology_psionic_theory"
			has_technology = tech_psionic_theory
		}
		custom_tooltip = {
			fail_text = "requires_not_ap_engineered_evolution"
			NOT = { has_ascension_perk = ap_engineered_evolution }
		}
		custom_tooltip = {
			fail_text = "requires_not_ap_the_flesh_is_weak"
			NOT = { has_ascension_perk = ap_the_flesh_is_weak }
		}
	}
	
	potential = {
		NOT = {
			has_ascension_perk = ap_mind_over_matter
			has_authority = "auth_machine_intelligence"
		}
		is_mechanical_empire = no
		is_cyborg_empire = no
	}
	
	ai_weight = {
		factor = 20
		modifier = {
			factor = 0
			NOR = {
				has_ethic = ethic_spiritualist
				has_ethic = ethic_fanatic_spiritualist
			}
		}
		modifier = {
            factor = 3
            has_ethic = ethic_spiritualist
        }
        modifier = {
            factor = 5
            has_ethic = ethic_fanatic_spiritualist
        }
		modifier = {
			factor = 0
			NOT = {
				has_ascension_perk = ap_technological_ascendancy
			}
		}
	}
}

ap_world_shaper = {
	on_enabled = {
		give_technology = { message = no tech = tech_atmospheric_manipulation }
	}
	possible = {
		custom_tooltip = {
			fail_text = "requires_technology_terrestrial_sculpting"
			has_technology = tech_terrestrial_sculpting
		}
	}

	modifier = {
		terraform_speed_mult = 0.5
		terraforming_cost_mult = -0.25
	}
	
	potential = {
		NOT = {
			has_ascension_perk = ap_world_shaper
		}
	}
	
	ai_weight = {
		factor = 0
	}
}

ap_galactic_force_projection = {
	possible = {
		custom_tooltip = {
			fail_text = "requires_ascension_perks_2"
			num_ascension_perks > 1
		}
	}
	modifier = {
		navy_size_add = 200
	}
	
	potential = {
		NOT = {
			has_ascension_perk = ap_galactic_force_projection
		}
	}
	
	ai_weight = {
		factor = 10
		modifier = {
			factor = 1.5
			num_owned_planets < 10
		}
		modifier = {
			factor = 1.5
			num_owned_planets < 20
		}	
		modifier = {
			factor = 2
			has_ethic = ethic_militarist
		}
		modifier = {
			factor = 3
			has_ethic = ethic_fanatic_militarist
		}
		modifier = {
			factor = 0
			NOT = {
				has_ascension_perk = ap_technological_ascendancy
			}
		}
	}
}

ap_defender_of_the_galaxy = {
	possible = {
		custom_tooltip = {
			fail_text = "requires_ascension_perks_2"
			num_ascension_perks > 1
		}
	}
	modifier = {
		description = ap_defender_of_the_galaxy_modifier_desc 
		damage_vs_country_type_swarm_mult = 0.5
		damage_vs_country_type_extradimensional_mult = 0.5
		damage_vs_country_type_extradimensional_2_mult = 0.5
		damage_vs_country_type_extradimensional_3_mult = 0.5
		damage_vs_country_type_ai_empire_mult = 0.5
	}

	on_enabled = {
		custom_tooltip = "ap_defender_of_the_galaxy_effect_opinion"
		# see triggered_opinion_ap_defender_of_the_galaxy
	}
	
	potential = {
		NOT = {
			has_ascension_perk = ap_defender_of_the_galaxy
		}
	}
	
	ai_weight = {
		factor = 5
		modifier = {
			factor = 0
			NOT = { has_global_flag = galactic_crisis_happened }
		}
	}
}

ap_master_builders = {
	on_enabled = {
		if = {
			limit = { 
				NOT = { has_technology = tech_mega_engineering }
			}
			give_technology = { message = no tech = tech_mega_engineering }
		}
	}
	possible = {
		custom_tooltip = {
			fail_text = "requires_ascension_perks_2"
			num_ascension_perks > 1
		}
		custom_tooltip = {
			fail_text = "requires_ms_perk"
			OR = {
				has_ascension_perk = ap_voidborn
				has_ascension_perk = ap_galactic_wonders
				has_ascension_perk = ap_orbital_engineering_org
				has_ascension_perk = ap_orbital_engineering_mac
			}
		}
		custom_tooltip = {
			fail_text = "requires_technology_zero_point_power"
			has_technology = tech_zero_point_power
		}		
	}
	modifier = {
		megastructure_build_speed_mult = 1
		mod_megastructure_build_cost_mult = -0.10
	}
	
	potential = {
		NOT = {
			has_ascension_perk = ap_master_builders
		}
	}
	
	ai_weight = {
		factor = 10
		modifier = {
			factor = 10
			OR = {
				has_ethic = ethic_pacifist
				has_ethic = ethic_materialist
				has_ethic = ethic_fanatic_materialist
				has_ethic = ethic_fanatic_pacifist
			}
		}
		modifier = {
			factor = 0
			NOT = {
				has_ascension_perk = ap_technological_ascendancy
			}
		}
	}
}

ap_interstellar_dominion = {
	modifier = {
		country_border_mult = 0.25
	}
	
	potential = {
		NOT = {
			has_ascension_perk = ap_interstellar_dominion
		}
	}
	
	ai_weight = {
		factor = 1
		modifier = {
			factor = 10
			has_ethic = ethic_xenophobe
		}
		modifier = {
			factor = 20
			has_ethic = ethic_fanatic_xenophobe
		}
		modifier = {
			factor = 0
			NOT = {
				AND = {
					has_ascension_perk = ap_technological_ascendancy
					has_ascension_perk = ap_engineered_evolution
					has_ascension_perk = ap_the_flesh_is_weak
					has_ascension_perk = ap_mind_over_matter
					has_ascension_perk = ap_synthetic_evolution
					has_ascension_perk = ap_evolutionary_mastery
					has_ascension_perk = ap_transcendence
					has_ascension_perk = ap_synthetic_age
				}
			}
		}
	}
}

ap_galactic_contender = {
	possible = {
		custom_tooltip = {
			fail_text = "requires_ascension_perks_2"
			num_ascension_perks > 1
		}
	}
	modifier = {
		damage_vs_country_type_fallen_empire_mult = 0.33
		damage_vs_country_type_awakened_fallen_empire_mult = 0.33
	}
	
	potential = {
		NOT = {
			has_ascension_perk = ap_galactic_contender
		}
		any_relation = {
			has_communications = root
			OR = {
				is_country_type = fallen_empire
				is_country_type = awakened_fallen_empire
			}
		}		
	}
	
	ai_weight = {
		weight = 0
	}
}

ap_technological_ascendancy = {
	modifier = {
		all_technology_research_speed = 0.1
	}
	
	potential = {
		NOT = {
			has_ascension_perk = ap_technological_ascendancy
		}
	}
	
	ai_weight = {
		factor = 100000
	}
}

ap_one_vision = {
	modifier = {
		pop_government_ethic_attraction = 0.50
	}
	
	potential = {
		NOT = {
			has_ascension_perk = ap_one_vision
			has_authority = "auth_machine_intelligence"
		}
	}
	
	ai_weight = {
		factor = 1
		modifier = {
			factor = 10
			has_ethic = ethic_authoritarian
		}
		modifier = {
			factor = 10
			has_ethic = ethic_fanatic_authoritarian
		}
		modifier = {
			factor = 10
			has_ethic = ethic_spiritualist
		}
		modifier = {
			factor = 10
			has_ethic = ethic_fanatic_spiritualist
		}
		modifier = {
			factor = 0
			NOT = {
				AND = {
					has_ascension_perk = ap_technological_ascendancy
					has_ascension_perk = ap_engineered_evolution
					has_ascension_perk = ap_the_flesh_is_weak
					has_ascension_perk = ap_mind_over_matter
					has_ascension_perk = ap_synthetic_evolution
					has_ascension_perk = ap_evolutionary_mastery
					has_ascension_perk = ap_transcendence
					has_ascension_perk = ap_synthetic_age
				}
			}
		}		
	}
}

ap_consecrated_worlds = {
	on_enabled = {
		custom_tooltip = "allow_edict_consectrated_worlds"
		custom_tooltip = "describe_edict_consectrated_worlds"
	}
	
	potential = {
		NOT = {
			has_ascension_perk = ap_consecrated_worlds
		}
		OR = {
			has_ethic = ethic_spiritualist
			has_ethic = ethic_fanatic_spiritualist
		}
	}
	
	ai_weight = {
		factor = 10
		modifier = {
			factor = 0
			NOT = {
				has_ascension_perk = ap_technological_ascendancy
			}
		}
	}
}

ap_mastery_of_nature = {
	on_enabled = {
		custom_tooltip = "ap_mastery_of_nature_unlocks"
		hidden_effect = {
			give_technology = { message = no tech = tech_tb_mountain_range }
			give_technology = { message = no tech = tech_tb_volcano }
			give_technology = { message = no tech = tech_tb_dangerous_wildlife }
			give_technology = { message = no tech = tech_tb_dense_jungle }
			give_technology = { message = no tech = tech_tb_quicksand_basin }
			give_technology = { message = no tech = tech_tb_noxious_swamp }
			give_technology = { message = no tech = tech_tb_massive_glacier }
			give_technology = { message = no tech = tech_tb_toxic_kelp }
			give_technology = { message = no tech = tech_tb_deep_sinkhole }
			give_technology = { message = no tech = tech_c_tb_Arachnophobia }
			give_technology = { message = no tech = tech_c_tb_Dunes }
			give_technology = { message = no tech = tech_c_tb_Seasonal_Flooding }
			give_technology = { message = no tech = tech_c_tb_Locust_Plagues }
			give_technology = { message = no tech = tech_c_tb_Unusual_Formations }
			give_technology = { message = no tech = tech_c_tb_Toxic_Gas }
		}
	}
	modifier = {
		planet_clear_blocker_cost_mult = -0.5
	}
	
	potential = {
		NOT = {
			has_ascension_perk = ap_mastery_of_nature
		}
	}
	
	ai_weight = {
		factor = 50		
	}
}

ap_voidborn = {
	potential = {
		NOT = {
			has_ascension_perk = ap_voidborn
		}
	}
	
	possible = {
		custom_tooltip = {
			fail_text = "requires_space_defense_station_3"
			has_technology = tech_space_defense_station_3
		}
		custom_tooltip = {
			fail_text = "requires_spaceport_6"
			has_technology = tech_spaceport_6
		}
	}
	
	on_enabled = {
		custom_tooltip = "allow_habitats"
	}
	
	ai_weight = {
		factor = 100
		modifier = {
			factor = 10
			OR = {
				has_ethic = ethic_pacifist
				has_ethic = ethic_materialist
				has_ethic = ethic_fanatic_materialist
				has_ethic = ethic_fanatic_pacifist
			}
		}
		modifier = {
			factor = 0
			NOT = {
				has_ascension_perk = ap_technological_ascendancy
			}
		}
	}
}

#ap_the_circle_of_life = {
#	on_enabled = {
#		custom_tooltip = "allow_ring_world"
#	}
#	
#	possible = {
#		custom_tooltip = {
#			fail_text = "requires_ascension_perks_3"
#			num_ascension_perks > 2
#		}
#		custom_tooltip = {
#			fail_text = "requires_ap_voidborn"
#			has_ascension_perk = ap_voidborn
#		}
#		custom_tooltip = {
#			fail_text = "requires_mega_engineering"
#			has_technology = tech_mega_engineering
#		}		
#	}
#
#	potential = {
#		NOT = {
#			has_ascension_perk = ap_the_circle_of_life
#		}
#	}
#	
#	ai_weight = {
#		factor = 10
#		modifier = {
#			factor = 10
#			OR = {
#				has_ethic = ethic_pacifist
#				has_ethic = ethic_materialist
#				has_ethic = ethic_fanatic_materialist
#			  
#				has_ethic = ethic_fanatic_pacifist
#			}
#		}
#	}
#}

ap_galactic_wonders = {
	on_enabled = {
		custom_tooltip = "allow_spy_orb"
		custom_tooltip = "allow_think_tank"
		custom_tooltip = "allow_dysons_sphere"
		custom_tooltip = "allow_star_lifter"									  
	}
	
	possible = {
		custom_tooltip = {
			fail_text = "requires_ascension_perks_3"
			num_ascension_perks > 2
		}
		custom_tooltip = {
			fail_text = "requires_mega_engineering"
			has_technology = tech_mega_engineering
		}		
	}				
	
	potential = {
		NOT = {
			has_ascension_perk = ap_galactic_wonders
		}
	}
	
	ai_weight = {
		factor = 100000
	}
}

ap_imperial_prerogative = {
	modifier = {
		country_core_sector_system_cap = 5
	}
	
	potential = {
		NOT = {
			has_ascension_perk = ap_imperial_prerogative
		}
	}
	
	ai_weight = {
		factor = 0
	}
	
	on_enabled = {
		run_ai_strategic_data = yes
	}
}

ap_shared_destiny = {
	modifier = {
		subject_integration_influence_cost_mult = -0.5
	}
	
	potential = {
		NOR = {
			has_valid_civic = civic_fanatic_purifiers
			has_valid_civic = civic_hive_devouring_swarm
			has_valid_civic = civic_inwards_perfection
			has_valid_civic = civic_machine_terminator
		}
		NOT = {
			has_ascension_perk = ap_shared_destiny
		}
	}
	
	ai_weight = {
		factor = 0
	}
}

ap_synthetic_age = {
	modifier = {
		MACHINE_species_trait_points_add = 2
		modify_species_cost_mult = -0.33
	}
	
	potential = {
		has_authority = auth_machine_intelligence
		NOT = {
			has_ascension_perk = ap_synthetic_age
		}
	}
	
	possible = {
		custom_tooltip = {
			fail_text = "requires_technology_robomodding"
			OR = {
				has_technology = tech_robomodding_m
				has_technology = tech_robomodding
			}
		}
		custom_tooltip = {
			fail_text = "requires_ascension_perks_2"
			num_ascension_perks > 1
		}				
	}
	
	ai_weight = {
		factor = 10000
		modifier = {
			factor = 0
			NOT = {
				has_ascension_perk = ap_technological_ascendancy
			}
		}
	}
}

#ap_machine_worlds = {
#	on_enabled = {
#		custom_tooltip = "allow_machine_worlds"
#		custom_tooltip = "describe_machine_worlds"
#	}
#
#	potential = {
#		has_authority = auth_machine_intelligence
#		NOT = {
#			has_ascension_perk = ap_machine_worlds
#		}
#	}
#	
#	possible = {
#		custom_tooltip = {
#			fail_text = "requires_technology_tech_climate_restoration"
#			has_technology = tech_climate_restoration
#		}	
#		custom_tooltip = {
#			fail_text = "requires_ascension_perks_2"
#			num_ascension_perks > 1
#		}	
#	}
#	
#	ai_weight = {
#		factor = 10000
#		modifier = {
#			factor = 0
#			NOT = {
#				has_ascension_perk = ap_technological_ascendancy
#			}
#		}
#	}
#}