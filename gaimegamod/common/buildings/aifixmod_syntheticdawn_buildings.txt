#
# BALANCE
# can change the time and cost of all common buildings with these variables below

@b0time = 120
@b0cost = 30
@b0effect = 1
@b0upkeep = 0.5

@b1time = 210
@b1cost = 60
@b1effect = 2
@b1upkeep = 1

@b2time = 180
@b2cost = 90
@b2effect = 3
@b2upkeep = 1.5

@b3time = 180
@b3cost = 120
@b3effect = 4
@b3upkeep = 2

@b4time = 180
@b4cost = 150
@b4effect = 5
@b4upkeep = 2.5

@b5time = 180
@b5cost = 180
@b5effect = 8
@b5upkeep = 3

@science1 = 1
@science2 = 2
@science3 = 3
@science4 = 4
@science5 = 6

@natural_equilibrium_happiness = 0.02

### UNITY BUILDINGS

# Uplink Node
building_uplink_node = {
	base_buildtime = 180
	planet_unique = yes
	
	cost = {
		minerals = 100
	}
	
	show_tech_unlock_if = { has_authority = auth_machine_intelligence }

	potential = {
		owner = { has_authority = auth_machine_intelligence }
	}

	destroy_if = {
		OR = {
			owner = {
				NOT = { has_authority = auth_machine_intelligence }
			}
			owner = {
				AND = {
					has_tradition = tr_supremacy_finish
					has_tradition = tr_harmony_finish
					has_tradition = tr_prosperity_finish
					has_tradition = tr_domination_finish
					has_tradition = tr_expansion_finish
					has_tradition = tr_discovery_finish
					has_tradition = tr_diplomacy_finish
				}
			}
		}
	}
	
	produced_resources = {
		unity = 2
	}
	
	required_resources = {
		energy = 1
	}
	
	prerequisites = {
		"tech_cultural_heritage"
	}	
	
	upgrades = {
		building_network_junction
	}
	
	ai_allow = {
		NOR = {
			has_resource = {
				type = sr_betharian
				amount > 0
			}	
			has_resource = {
				type = sr_alien_pets
				amount > 0
			}
			tile = {
				has_resource = {
					type = energy
					amount > 0
				}
			}
			tile = {
				has_resource = {
					type = minerals
					amount > 0
				}
			}
			tile = {
				has_resource = {
					type = engineering_research
					amount > 0
				}
			}
			tile = {
				has_resource = {
					type = physics_research
					amount > 0
				}
			}
			tile = {
				has_resource = {
					type = society_research
					amount > 0
				}
			}
			AND = {
				owner = {
					AND = {
						has_tradition = tr_supremacy_finish
						has_tradition = tr_harmony_finish
						has_tradition = tr_prosperity_finish
						has_tradition = tr_domination_finish
						has_tradition = tr_expansion_finish
						has_tradition = tr_discovery_finish
						has_tradition = tr_diplomacy_finish
					}
				}
			}
		}
	}

	ai_weight = {
		weight = 0
		modifier = {
			weight = 100
			planet = {
				count_pops = {
					count > 7
				}
			}
		}
	}
	
}

# Spare Parts Depot
building_spare_parts_depot = {
	base_buildtime = 360
	planet_unique = yes
	
	cost = {
		minerals = 150
	}

	show_tech_unlock_if = { has_authority = auth_machine_intelligence }

	potential = {
		owner = { has_authority = auth_machine_intelligence }
	}

	destroy_if = {
		owner = {
			NOT = { has_authority = auth_machine_intelligence }
		}		
	}
	
	required_resources = {
		energy = 1
	}
	
	produced_resources = {
		society_research = 2
		unity = 1
	}
	
	planet_modifier = {
		pop_robot_build_speed_mult = 0.15
	}
	
	prerequisites = {
		"tech_modular_components"
	}
	
	upgrades = {
		building_unit_assembly_plant
	}
	
	ai_allow = {
		NOR = {
			has_resource = {
				type = sr_betharian
				amount > 0
			}	
			has_resource = {
				type = sr_alien_pets
				amount > 0
			}
			tile = {
				has_resource = {
					type = energy
					amount > 0
				}
			}
			tile = {
				has_resource = {
					type = minerals
					amount > 0
				}
			}
			tile = {
				has_resource = {
					type = engineering_research
					amount > 0
				}
			}
			tile = {
				has_resource = {
					type = physics_research
					amount > 0
				}
			}			
		}
	}

	ai_weight = {
		weight = 100
	}
}

# Singularity Core
building_singularity_core = {
	base_buildtime = 720
	empire_unique = yes
	
	cost = {
		minerals = 500
		influence = 100
	}
	
	produced_resources = {
		engineering_research = 5
		physics_research = 5
		society_research = 5
	}
	
	required_resources = {
		energy = 10
	}
	
	country_modifier = {
		all_technology_research_speed = 0.05
	}
	
	show_tech_unlock_if = { has_authority = auth_machine_intelligence }

	potential = {
		owner = { has_authority = auth_machine_intelligence }
		planet = {
			is_capital = yes
		}
	}

	destroy_if = {
		planet = { is_capital = no }
		owner = {
			NOT = { has_authority = auth_machine_intelligence }
		}		
	}
	
	allow = {
		custom_tooltip = {
			text = "requires_building_machine_capital_3"
			planet = { has_building = "building_machine_capital_3" }
		}
	}
	
	prerequisites = {
		"tech_singularity_core"
	}
	
	ai_allow = {
		NOR = {
			has_resource = {
				type = sr_betharian
				amount > 0
			}	
			has_resource = {
				type = sr_alien_pets
				amount > 0
			}
			tile = {
				has_resource = {
					type = energy
					amount > 0
				}
			}
			tile = {
				has_resource = {
					type = minerals
					amount > 0
				}
			}
		}
	}
	
	ai_weight = {
		weight = 100
	}
}

# Probability Engine
building_probability_engine = {
	base_buildtime = 360
	empire_unique = yes
	
	cost = {
		minerals = 500
		influence = 100
	}
	
	produced_resources = {
		energy = 10
	}
	
	show_tech_unlock_if = { has_authority = auth_machine_intelligence }

	potential = {
		owner = { has_authority = auth_machine_intelligence }
		planet = {
			is_capital = yes
		}
	}

	destroy_if = {
		planet = { is_capital = no }
		owner = {
			NOT = { has_authority = auth_machine_intelligence }
		}		
	}
	
	allow = {
		custom_tooltip = {
			text = "requires_building_machine_capital_3"
			planet = { has_building = "building_machine_capital_3" }
		}
	}
	
	country_modifier = {
		tile_resource_energy_mult = 0.10
	}
	
	prerequisites = {
		"tech_probability_theory"
	}
	
	ai_allow = {
		NOR = {
			has_resource = {
				type = sr_betharian
				amount > 0
			}	
			has_resource = {
				type = sr_alien_pets
				amount > 0
			}
			tile = {
				has_resource = {
					type = minerals
					amount > 0
				}
			}
			tile = {
				has_resource = {
					type = engineering_research
					amount > 0
				}
			}
			tile = {
				has_resource = {
					type = physics_research
					amount > 0
				}
			}
			tile = {
				has_resource = {
					type = society_research
					amount > 0
				}
			}
		}
	}
	
	ai_weight = {
		weight = 100
	}
}

building_control_center = {
	base_buildtime = 360
	planet_unique = yes
	
	cost = {
		minerals = 250
	}
	
	potential = {
		owner = {
			has_swapped_tradition = tr_synchronicity_machine_center
		}
	}
	
	destroy_if = {
		exists = owner
		owner = { NOT = { has_swapped_tradition = tr_synchronicity_machine_center } }
	}
	
	produced_resources = {
		energy = 2
		unity = 2
	}
	
	planet_modifier = {
		pop_robot_production_output = 0.05
	}
	
	ai_allow = {
		OR = {
			tile = {
				has_building = no
			}
			tile = {
				has_building = building_power_hub_1
			}
		}
		NOR = {
			has_resource = {
				type = sr_betharian
				amount > 0
			}	
			has_resource = {
				type = sr_alien_pets
				amount > 0
			}	
			tile = {
				has_resource = {
					type = minerals
					amount > 0
				}
			}
			tile = {
				has_resource = {
					type = engineering_research
					amount > 0
				}
			}
			tile = {
				has_resource = {
					type = physics_research
					amount > 0
				}
			}
			tile = {
				has_resource = {
					type = society_research
					amount > 0
				}
			}			
		}		
	}	

	ai_weight = {
		factor = 10000
	}
}