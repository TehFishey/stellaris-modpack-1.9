####I hate the AI in Stellaris####
tech_im_an_ai_tech = {
	cost = 0
	area = engineering
	tier = 1
	category = { industry }
	weight = 1
	
	weight_modifier = {
		modifier = {
			is_ai = no
			factor = 0
		}
		modifier = {
			is_ai = yes
			factor = 0
		}
	}
}