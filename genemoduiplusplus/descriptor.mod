name="GeneMod_UI PlusPlus [1.9.*]"
path="mod/genemoduiplusplus"
tags={
	"Fixes"
	"User Interface"
	"UI"
	"Genetic Modification"
	"Utilities"
}
picture="thumb.jpg"
supported_version="1.9.*"
