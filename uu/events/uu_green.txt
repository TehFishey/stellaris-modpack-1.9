############################
#
# Untedious Upgrading Events
#
# Written by Waesche
#
############################
namespace = green

#on monthly pulse
event = {
	id = green.1
	hide_window = yes
	is_triggered_only = yes
	trigger = {}
	immediate = {
		every_country = {
			limit = {
				is_ai = no
			}
			country_event = { id = green.2 }
		}
	}


}

country_event = {
	id = green.2
	hide_window = yes
	is_triggered_only = yes
	trigger = {}
	immediate = {
		if = {
			limit = {
				NOT = {has_country_flag = column }
			}
			set_country_flag = column
			new_list = yes
			save_event_target_as = input00 #fallback: country name
			random_owned_planet = {
				limit = {has_planet_flag = selected_planet}
				save_event_target_as = input00
			}
			set_data = yes#first row is reserved for the name of the selected planet from Macro Builder
		}
		every_owned_planet = {
			limit = {
				OR = {
					num_pops > 4
					is_planet_class = pc_habitat
				}
			}
			OWNER = { 
				PREV = {
					set_variable = { which = data_column value = PREV }
				}
			}
			if = {
				limit = {
					NOT = {has_planet_flag = green_planet}
					sector_controlled = no
					OR = {#color if the planet has
						NOT = {#no capital
							has_building = building_capital_1
							has_building_construction = building_capital_1
							has_building = building_capital_2
							has_building_construction = building_capital_2
							has_building = building_capital_3
							has_building_construction = building_capital_3
							has_building = building_hab_capital
							has_building_construction = building_hab_capital
							has_building = building_machine_capital_1
							has_building_construction = building_machine_capital_1
							has_building = building_machine_capital_2
							has_building_construction = building_machine_capital_2
							has_building = building_machine_capital_3
							has_building_construction = building_machine_capital_3
						}
						AND = {#tier 1 capital and more than 9 pops	
							OR = {
								has_building = building_capital_1
								has_building = building_machine_capital_1
							}
							num_pops > 9
						}
						AND = {#tier 2 capital on capital world with more than 14 pops
							OR = {
								has_building = building_capital_2
								has_building = building_machine_capital_2
							}
							is_capital = yes
							num_pops > 14
						}
					}
				}
				if = {
					limit = {
						 has_planet_flag = selected_planet
					}
					OWNER = {
						set_variable = {which = data_row value = 1}
						get_data = yes
					}
					set_name = "[event_target:output00.GetName]"
					remove_planet_flag = selected_planet #deselect planet (there is no need for this in this mod)
				}
				save_event_target_as = input00#save planet as event target input00
				set_data = yes#save the name of the object stored in input00 in the database
				set_planet_flag = green_planet#mark this planet as colored green
				if = {
					limit = {
						has_planet_flag = selected_planet #leftover from Macro Builder #ignore this #will never happen
					}
					set_name = Colour_Orange
					
					else = {
						set_name = "green_planet_name"#color the planet name green
						#from IBP_l_english.yml
						#green_planet_name:0 "§G[This.GetName]§!"
						#§G: color everything after §G green
						#§!: stop coloring
						#[This.GetName] insert name (GetName) of the current scoped object (This) (This = planet) 
					}
				}
			}
			if = {
				limit = {
					OR = {
						has_planet_flag = green_planet
						has_planet_flag = selected_planet#deselect planet
					}
					OR = {#remove color if the planet has
						AND = {#tier 1 capital
							OR = {
								has_building = building_capital_1
								has_building = building_machine_capital_1
							}
							OR = {#and less than 10 popsor or if you can't build next tier
								num_pops < 10
								Owner = { NOT = {has_technology = tech_colonial_centralization } }
							}
						}
						AND = {#tier 2 capital 
							OR = {
								has_building = building_capital_2
								has_building = building_machine_capital_2
							}
							OR = {#and less than 15 pops or if you can't build next tier
								num_pops < 15
								is_capital = no
								Owner = { NOT = {has_technology = tech_galactic_administration } }
							}
							
						}
						#or tier 3 capital
						has_building = building_capital_3
						has_building = building_machine_capital_3
						#or hub capital		
						has_building = building_hab_capital
						has_building_construction = building_hab_capital	
						#or building next tier
						has_building_construction = building_capital_1
						has_building_construction = building_machine_capital_1
						has_building_construction = building_capital_2
						has_building_construction = building_machine_capital_2
						has_building_construction = building_capital_3
						has_building_construction = building_machine_capital_3
					}
				}
				get_data = yes#get name from the database. Name will be stored in output00 
				set_name = "[event_target:output00.GetName]"#change name back to white
				remove_planet_flag = selected_planet#deselect planet 
				remove_planet_flag = green_planet#remove marker
				save_event_target_as = input00#save planet as event target input00 (necessary for delete_data)
				delete_data = yes#delete stored name in database
			}
		}
	}
}


# This = Planet
# From = Tile
planet_event = {
	id = green.3
	hide_window = yes
	is_triggered_only = yes
	trigger = {
		OWNER = {
			is_ai = no
		}
		OR = {
			has_planet_flag = green_planet
			has_planet_flag = selected_planet#deselect planet
		}
		From = {
			OR = {
				has_building_construction = building_capital_1
				has_building_construction = building_machine_capital_1
				has_building_construction = building_capital_2
				has_building_construction = building_machine_capital_2
				has_building_construction = building_capital_3
				has_building_construction = building_machine_capital_3
			}
		}
	}
	immediate = {
		get_data = yes#get name from the database. Name will be stored in output00 
		set_name = "[event_target:output00.GetName]"#change name back to white
		remove_planet_flag = selected_planet#deselect planet 
		remove_planet_flag = green_planet#remove marker
		save_event_target_as = input00#save planet as event target input00 (necessary for delete_data)
		delete_data = yes#delete stored name in database
	}
}



# country_event = {#Deinstallation
	# id = green.10
	# hide_window = yes
	# is_triggered_only = yes
	# trigger = {}
	# immediate = {
		# every_owned_planet = {
			# limit = {
				# has_planet_flag = green_planet
			# }
			# OWNER = { 
				# PREV = {
					# set_variable = { which = data_column value = PREV }
				# }
			# }
			# get_data = yes
			# set_name = "[event_target:output00.GetName]"
			# remove_planet_flag = selected_planet#deselect planet
			# remove_planet_flag = green_planet
			# save_event_target_as = input00
			# delete_data = yes
		# }
	# }
# }
