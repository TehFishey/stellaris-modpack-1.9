# Industrial Armies
vbph_army = {
	defensive = yes
	damage = 0.8
	health = 0.8
	morale = 2
	icon_frame = 5
	
	potential = {
		always = no
	}
}