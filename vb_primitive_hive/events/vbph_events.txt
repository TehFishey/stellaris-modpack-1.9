#######################################
#Primitive Hive Minds by ViolentBeetle#
#######################################

namespace = vbph

@tier1researchreward = 6
@tier1researchmin = 60
@tier1researchmax = 150
@tier2researchreward = 12
@tier2researchmin = 90
@tier2researchmax = 250
@tier3researchreward = 18
@tier3researchmin = 120
@tier3researchmax = 350
@tier4researchreward = 24
@tier4researchmin = 150
@tier4researchmax = 500
@tier5researchreward = 48
@tier5researchmin = 300
@tier5researchmax = 1000

#Test event

country_event = {
	id = vbph.9999
	
	hide_window = yes
	
	is_triggered_only = yes
	
	immediate = {
		set_global_flag = vbph_testing
	}
}

###############
#  ON ACTION  #
###############

#On survey
ship_event = {
	id = vbph.100
	title = vbph.100.name
	desc = {
		trigger = {
			from = {
				owner = { has_country_flag = vbph_id_stage }
			}
		}
		text = vbph.100.id
	}
	desc = {
		trigger = {
			from = {
				owner = { has_country_flag = vbph_ego_stage }
			}
		}
		text = vbph.100.ego
	}
	desc = {
		trigger = {
			from = {
				owner = { has_country_flag = vbph_superego_stage }
			}
		}
		text = vbph.100.superego
	}
	
	is_triggered_only = yes
	
	picture = GFX_evt_alien_cavemen
	location = FROM
	
	trigger = {
		FROM = {
			exists = owner
			owner = { is_country_type = vbph_country }
		}
	}
	
	immediate = {
		owner = { 
			save_event_target_as = ship_owner 
			set_country_flag = encountered_first_primitive 
		}
		FROM = {
			owner = {
				establish_contact = { who = event_target:ship_owner }
				establish_communications = event_target:ship_owner
				save_event_target_as = primitive_civ
			}
		}
	}
	
	option = {
		name = INTERESTING
		hidden_effect = {
			owner = {
				country_event = {
					id = story.6
					days = 30
					scopes = {
						fromfrom = from.owner
					}
				}
			}
		}
	}
}

# Hive planet found by trading Star Charts
planet_event = {
	id = vbph.102
	is_triggered_only = yes
	hide_window = yes
	
	trigger = {
		exists = owner
		NOT = { exists = fromfrom }
		owner = { is_country_type = vbph_country }
	}
	
	immediate = {
		from = {
			establish_communications_no_message = root.owner
		}
	}
}


#Scheduling creation of special systems. Just in case, create Sol on second day, as probability is written under this assumption.
event = {
	id = vbph.150
	
	hide_window = yes
	
	is_triggered_only = yes
	
	trigger = {
		host_has_dlc = "Utopia"
		# NOT = { 
			# any_system = { has_star_flag = sol }
		# }
	}
	
	immediate = {
		set_global_flag = vbph_spawn_scheduled
		random_system = {
			limit = { NOT = { exists = owner } }
			random_system_planet = { 
				planet_event = { id = vbph.151 days = 2 }
			}
		}
	}
}

#spawner
planet_event = {
	id = vbph.151
	
	hide_window = yes
	
	is_triggered_only = yes
	
	trigger = {
		host_has_dlc = "Utopia"
		NOT = { 
			any_system = { has_star_flag = sol }
		}
	}
	
	immediate = {
		random_list = {
			50 = { }
			50 = { spawn_system = { initializer = "vbph_sol" } }
		}
	}
}

#####################
#NATURAL PROGRESSION#
#####################

#Id->Ego
country_event = {
	id = vbph.200
	
	hide_window = yes
	
	mean_time_to_happen = {
		years = 80
	}
	
	trigger = {
		is_country_type = vbph_country
		has_country_flag = vbph_id_stage
		NOT = { has_country_flag = vbph_recently_advanced }
	}
	
	immediate = {
		remove_country_flag = vbph_id_stage
		set_country_flag = vbph_ego_stage
		set_primitive_age = vbph_ego_stage
		
		IF = {
			limit = { capital_scope = { has_planet_flag = vbph_earth } }
			set_timed_country_flag = { flag = vbph_recently_advanced days = 7200 }
			change_country_flag = {
				icon = {
					category = "human"
					file = "flag_human_8.dds"
				}
				background = {
					category = "backgrounds"
					file = "triangle_split.dds"
				}
				colors = {
					"teal"
					"blue"
					"null"
					"null"
				}
			}
			else = {
				change_country_flag = {
					icon = {
						category = "zoological"
						file = "flag_zoological_20.dds"
					}
					background = {
						category = "backgrounds"
						file = "triangle_split.dds"
					}
					colors = {
						"teal"
						"blue"
						"null"
						"null"
					}
				}
			}
		}
		
		change_government = {
			civics = {
				civic = vbph_civic_ego_one
				civic = vbph_civic_ego_two
			}
		}
		
		capital_scope = {
			random_tile = {
				limit = { has_building = vbph_spawning_pool_1 }
				set_building = vbph_spawning_pool_2
			}
			while = {
				count = 3
				random_tile = {
					limit = { has_blocker = no has_pop = no has_building = no }
					create_pop = { species = root }
					set_building = vbph_working_grounds
					set_deposit = d_mineral_deposit
				}
			}
			random_tile = {
				limit = { has_blocker = no has_pop = no has_building = no }
				set_blocker = tb_failing_infrastructure
			}
			remove_all_armies = yes
			every_owned_pop = {
				PREV = {
					create_army = {
						name = "Hive Guardians"
						owner = root
						species = root
						type = "vbph_army"
					}
				}
			}
			
			observation_outpost_owner = {
				country_event = { id = vbph.201 }
			}
		}
	}
}

#Id -> ego notification
country_event = {
	id = vbph.201
	title = vbph.201.name
	desc = vbph.201.desc
	
	picture = GFX_evt_alien_cavemen
	location = from
	
	is_triggered_only = yes
	
	option = {
		name = OK
	}
}

#Ego->Superego
country_event = {
	id = vbph.210
	
	hide_window = yes
	
	mean_time_to_happen = {
		years = 80
	}
	
	trigger = {
		is_country_type = vbph_country
		has_country_flag = vbph_ego_stage
		NOT = { has_country_flag = vbph_recently_advanced }
	}
	
	immediate = {
		remove_country_flag = vbph_ego_stage
		set_country_flag = vbph_superego_stage
		set_primitive_age = vbph_superego_stage
		
		set_timed_country_flag = { flag = vbph_recently_advanced days = 7200 }
		
		IF = {
			limit = { capital_scope = { has_planet_flag = vbph_earth } }
			change_country_flag = {
				icon = {
					category = "human"
					file = "flag_human_8.dds"
				}
				background = {
					category = "backgrounds"
					file = "triangle_split.dds"
				}
				colors = {
					"grey"
					"brown"
					"null"
					"null"
				}
			}
			else = {
				change_country_flag = {
					icon = {
						category = "zoological"
						file = "flag_zoological_20.dds"
					}
					background = {
						category = "backgrounds"
						file = "triangle_split.dds"
					}
					colors = {
						"grey"
						"brown"
						"null"
						"null"
					}
				}
			}
		}
		
		change_government = {
			civics = {
				civic = vbph_civic_superego_one
				civic = vbph_civic_superego_two
			}
		}
		
		capital_scope = {
			random_tile = {
				limit = { has_building = vbph_spawning_pool_2 }
				set_building = vbph_spawning_pool_3
			}
			random_tile = {
				limit = { has_blocker = no has_pop = no has_building = no }
				create_pop = { species = root }
				set_building = vbph_working_grounds
				set_deposit = d_mineral_deposit
			}
			
			random_tile = {
				limit = { has_blocker = no has_pop = no has_building = no }
				create_pop = { species = root }
				set_building = vbph_feeding_grounds
				set_deposit = d_farmland_deposit
			}
			random_tile = {
				limit = { has_blocker = no has_pop = no has_building = no }
				set_blocker = tb_failing_infrastructure
			}
			remove_all_armies = yes
			every_owned_pop = {
				PREV = {
					create_army = {
						name = "Hive Guardians"
						owner = root
						species = root
						type = "vbph_army"
					}
				}
			}
			
			observation_outpost_owner = {
				country_event = { id = vbph.211 }
			}
		}
	}
}

#Id -> ego notification
country_event = {
	id = vbph.211
	title = vbph.211.name
	desc = vbph.211.desc
	
	picture = GFX_evt_alien_city
	location = from
	
	is_triggered_only = yes
	
	option = {
		name = OK
	}
}

#Superego -> Space Age
country_event = {
	id = vbph.220
	
	hide_window = yes
	
	mean_time_to_happen = {
		years = 80
	}
	
	trigger = {
		OR = {
			exists = from
			AND = {
				is_country_type = vbph_country
				has_country_flag = vbph_superego_stage
				NOT = { has_country_flag = vbph_recently_advanced }
				years_passed > 25
			}
		}
	}
	
	immediate = {
		remove_country_flag = vbph_superego_stage
		set_country_flag = primitives_can_into_space
		set_country_type = default
		remove_country_flag = day_0
		
		set_timed_country_flag = { flag = vbph_recently_advanced days = 7200 }
		
		IF = {
			limit = { capital_scope = { has_planet_flag = vbph_earth } }
			change_country_flag = {
				icon = {
					category = "human"
					file = "flag_human_8.dds"
				}
				background = {
					category = "backgrounds"
					file = "triangle_split.dds"
				}
				colors = {
					"green"
					"dark_blue"
					"null"
					"null"
				}
			}
			else = {
				change_country_flag = random
			}
		}
		
		change_government = {
			civics = random
		}
		
		capital_scope = {
			random_tile = {
				limit = { 
					OR = {
						has_building = vbph_spawning_pool_1
						has_building = vbph_spawning_pool_2 
						has_building = vbph_spawning_pool_3 
					}
				}
				set_building = "building_capital_2"
				set_deposit = d_energy_deposit
			}
			while = {
				count = 2
				random_tile = {
					limit = { has_building = vbph_working_grounds }
					set_building = building_mining_network_1
				}
				random_tile = {
					limit = { has_building = vbph_feeding_grounds }
					set_building = building_hydroponics_farm_1
				}
			}
			random_tile = {
				limit = { has_blocker = no has_pop = no has_building = no }
				set_blocker = tb_collapsed_burrows
			}
			remove_all_armies = yes
			every_owned_pop = {
				PREV = {
					create_army = {
						owner = root
						species = root
						type = "defense_army"
					}
				}
			}
			
			observation_outpost_owner = {
				country_event = { id = vbph.221 }
			}
			observation_outpost = { dismantle = yes }
		}
		
		add_minerals = 1000 # enough for a spaceport and then some
		add_energy = 500
		add_influence = 300
		
		# set_name = random
	}
}

#Entering space age
country_event = {
	id = vbph.221
	title = vbph.221.name
	desc = vbph.221.desc
	
	picture = GFX_evt_alien_city
	location = from
	
	is_triggered_only = yes
	
	option = {
		name = OK
	}
}

#Space station, because why not?

country_event = {
	id = vbph.230
	
	hide_window = yes
	
	trigger = {
		has_country_flag = vbph_superego_stage
	}
	
	mean_time_to_happen = {
		years = 25
	}
	
	immediate = {
		capital_scope = {
			create_fleet = {
				name = "NAME_Space_Station"
				effect = {
					set_owner = ROOT
					create_ship = {
						name = "NAME_Space_Station"
						design = "NAME_Space_Station"
					}
					set_location = PREV
				}
			}
			observation_outpost_owner = {
				country_event = { id = vbph.231 }
			}
		}
	}
}

country_event = {
	id = vbph.231
	title = vbph.231.name
	desc = vbph.231.desc
	
	picture = GFX_evt_alien_city
	location = from
	
	is_triggered_only = yes
	
	option = {
		name = OK
	}
}

#Sudden death
country_event = {
	id = vbph.240
	
	hide_window = yes
	
	mean_time_to_happen = {
		years = 800
		
		modifier = {
			factor = 0.5
			has_country_flag = vbph_ego_stage
		}
		
		modifier = {
			factor = 0.25
			has_country_flag = vbph_superego_stage
		}
		
		modifier = {
			factor = 0.75
			capital_scope = {
				has_observation_outpost = yes
				observation_outpost = {
					has_mission = vbph_subdue
					has_mission = vbph_break_apart_0
					has_mission = vbph_break_apart_1
				}
			}
		}
	}
	
	trigger = {
		is_country_type = vbph_country
	}
	
	immediate = {
		capital_scope = {
			destroy_colony = { keep_buildings = yes }
			random_tile = {
				limit = { 
					OR = {
						has_building = vbph_spawning_pool_1
						has_building = vbph_spawning_pool_2 
						has_building = vbph_spawning_pool_3 
					}
				}
				remove_building = yes
			}
			observation_outpost_owner = { country_event = { id = vbph.241 } }
		}
	}
}

country_event = {
	id = vbph.241
	title = vbph.241.name
	desc = vbph.241.desc
	
	picture = GFX_evt_alien_nature
	location = from
	
	is_triggered_only = yes
	
	option = {
		name = OK
	}
}

############################
#OBSERVATION OUTPOST EVENTS#
############################

#Will be here eventually

##########
#PROJECTS#
##########

#Successful uplift
country_event = {
	id = vbph.400
	title = vbph.400.name
	desc = vbph.400.desc
	
	picture = GFX_evt_satellite_in_orbit
	location = from
	
	is_triggered_only = yes
	
	immediate = {
		FROM = {
			remove_country_flag = vbph_id_stage
			remove_country_flag = vbph_ego_stage
			remove_country_flag = vbph_superego_stage
			observation_outpost = { dismantle = yes }
			owner = {
				add_opinion_modifier = { who = root modifier = opinion_enlightened_us }
				if = {
					limit = {
						root = { is_country_type = default }
					}
					if = {
						limit = {
							root = { has_valid_civic = "civic_inwards_perfection" }
						}
						set_subject_of = {
							who = root
							subject_type = tributary
						}
						else = {
							set_subject_of = {
								who = root
								subject_type = protectorate
							}						
						}
					}				
				}
				if = {
					limit = {
						root = { is_country_type = awakened_fallen_empire }
					}
					if = {
						limit = { 
							root = { 
								has_ethic = ethic_fanatic_xenophobe
							}
						}
						set_subject_of = {
							who = root
							subject_type = thrall
						}		
					}	
					if = {
						limit = { 
							root = { 
								has_ethic = ethic_fanatic_xenophile
							}
						}
						set_subject_of = {
							who = root
							subject_type = signatory
						}		
					}			
					if = {
						limit = { 
							root = { 
								has_ethic = ethic_fanatic_spiritualist
							}
						}
						set_subject_of = {
							who = root
							subject_type = dominion
						}		
					}		
					if = {
						limit = { 
							root = { 
								has_ethic = ethic_fanatic_materialist
							}
						}
						set_subject_of = {
							who = root
							subject_type = satellite
						}		
					}				
				}
				country_event = { id = vbph.220 }
			}
		}
	}
	
	option = {
		name = EXCELLENT
	}
}

#Successful subduing
country_event = {
	id = vbph.410
	title = vbph.410.name
	desc = vbph.410.desc
	
	picture = GFX_evt_unspeakable_horror
	location = from
	
	is_triggered_only = yes
	
	immediate = {
		FROM = {
			set_owner = root
			observation_outpost = { dismantle = yes }
		}
	}
	
	option = {
		name = vbph.410.a
		custom_tooltip = vbph.410.a.tooltip
	}
}

#Successfully breaking them apart
country_event = {
	id = vbph.420
	title = vbph.420.name
	desc = vbph.420.desc
	
	picture = GFX_evt_alien_cavemen
	location = from
	
	is_triggered_only = yes
	
	immediate = {
		FROM = {
			random_owned_pop = {
				create_species = {
					is_mod = yes
					name = this
					plural = this
					class = this
					portrait = this
					traits = this
					homeworld = prev
					sapient = no
				}
				change_species = last_created
				modify_species = {
					species = this
					remove_trait = trait_hive_mind
				}
			}
			last_created_species = { 
				save_event_target_as = pre_saps 
			}
			destroy_colony = { keep_buildings = yes }
			planet_event = { id = vbph.610 days = 1 }
		}
	}
	
	option = {
		name = EXCELLENT
		custom_tooltip = vbph.420.a.tooltip
	}
}

#Successfully deassimilating
country_event = {
	id = vbph.430
	title = vbph.430.name
	desc = vbph.430.desc
	
	picture = GFX_evt_alien_cavemen
	location = from
	
	is_triggered_only = yes
	
	immediate = {
		FROM = {
			random_owned_pop = {
				create_species = {
					name = THIS
					class = THIS
					portrait = THIS
					traits = THIS
					homeworld = PREV
				}
				change_species = last_created
				modify_species = {
					species = this
					remove_trait = trait_hive_mind
				}
			}
			set_owner = root
			every_owned_pop = {
				change_species = last_created
			}
			observation_outpost = { dismantle = yes }
		}
	}
	
	option = {
		name = vbph.430.a
		custom_tooltip = vbph.430.a.tooltip
	}
}

############################
#MANIPULATION COMPLICATIONS#
############################

#Hive mind experiencing understanding
planet_event = {
	id = vbph.500
	
	hide_window = yes
	
	mean_time_to_happen = {
		months = 120
	}
	
	trigger = {
		has_owner = yes
		owner = { is_country_type = vbph_country }
		has_observation_outpost = yes
		observation_outpost = { 
			OR = {
				has_mission = vbph_enlightenment_0
				has_mission = vbph_enlightenment_1
				has_mission = vbph_enlightenment_2
			}
			mission_progress > 0.2
		}
		observation_outpost_owner = { has_authority = auth_hive_mind }
		NOT = {
			owner = {
				has_relation_flag = {
					who = root.observation_outpost_owner
					flag = vbph_enlighenment_progress
				}
			}
		}
	}
	
	immediate = {
		observation_outpost_owner = { country_event = { id = vbph.501 } }
	}
}

country_event = {
	id = vbph.501
	title = vbph.501.name
	desc = vbph.501.desc
	
	picture = GFX_evt_alien_cavemen
	location = from
	
	is_triggered_only = yes
	
	immediate = {
		FROM = {
			observation_outpost = { add_mission_progress = 0.2 }
			owner = {
				set_relation_flag = {
					who = ROOT
					flag = vbph_enlighenment_progress
				}
			}
		}
	}
	
	option = {
		name = vbph.501.a
		
		tooltip = {
			FROM = {
				observation_outpost = { add_mission_progress = 0.2 }
			}
		}
	}
}

#Machine empires experience difficulty
planet_event = {
	id = vbph.510
	
	hide_window = yes
	
	mean_time_to_happen = {
		months = 120
	}
	
	trigger = {
		has_owner = yes
		owner = { is_country_type = vbph_country }
		has_observation_outpost = yes
		observation_outpost = { 
			OR = {
				has_mission = vbph_enlightenment_0
				has_mission = vbph_enlightenment_1
				has_mission = vbph_enlightenment_2
			}
			mission_progress > 0.2
		}
		observation_outpost_owner = { has_authority = auth_machine_intelligence }
		NOT = {
			owner = {
				has_relation_flag = {
					who = root.observation_outpost_owner
					flag = vbph_enlighenment_progress
				}
			}
		}
	}
	
	immediate = {
		observation_outpost_owner = { country_event = { id = vbph.511 } }
	}
}

country_event = {
	id = vbph.511
	title = vbph.511.name
	desc = vbph.511.desc
	
	picture = GFX_evt_synth_organic_relations
	location = from
	
	is_triggered_only = yes
	
	immediate = {
		FROM = {
			observation_outpost = { add_mission_progress = -0.4 }
			owner = {
				set_relation_flag = {
					who = ROOT
					flag = vbph_enlighenment_progress
				}
			}
		}
	}
	
	option = {
		name = UNFORTUNATE
		
		tooltip = {
			FROM = {
				observation_outpost = { add_mission_progress = -0.4 }
			}
		}
	}
}

#Normies experience difficulty
planet_event = {
	id = vbph.520
	
	hide_window = yes
	
	mean_time_to_happen = {
		months = 120
	}
	
	trigger = {
		has_owner = yes
		owner = { is_country_type = vbph_country }
		has_observation_outpost = yes
		observation_outpost = { 
			OR = {
				has_mission = vbph_enlightenment_0
				has_mission = vbph_enlightenment_1
				has_mission = vbph_enlightenment_2
			}
			mission_progress > 0.2
		}
		observation_outpost_owner = { 
			NOR = {
				has_authority = auth_hive_mind 
				has_authority = auth_machine_intelligence
			}
		}
		NOT = {
			owner = {
				has_relation_flag = {
					who = root.observation_outpost_owner
					flag = vbph_enlighenment_progress
				}
			}
		}
	}
	
	immediate = {
		observation_outpost_owner = { country_event = { id = vbph.521 } }
	}
}

country_event = {
	id = vbph.521
	title = vbph.521.name
	desc = vbph.521.desc
	
	picture = GFX_evt_alien_cavemen
	location = from
	
	is_triggered_only = yes
	
	immediate = {
		FROM = {
			observation_outpost = { add_mission_progress = -0.2 }
			owner = {
				set_relation_flag = {
					who = ROOT
					flag = vbph_enlighenment_progress
				}
			}
		}
	}
	
	option = {
		name = UNFORTUNATE
		
		tooltip = {
			FROM = {
				observation_outpost = { add_mission_progress = -0.2 }
			}
		}
	}
}

#Difficulty subduing
planet_event = {
	id = vbph.530
	
	hide_window = yes
	
	mean_time_to_happen = {
		months = 480
		
		modifier = {
			factor = 0.25
			has_owner = yes
			owner = { has_country_flag = vbph_ego_stage }
		}
		
		modifier = {
			factor = 0.125
			has_owner = yes
			owner = { has_country_flag = vbph_superego_stage }
		}
	}
	
	trigger = {
		has_owner = yes
		owner = { is_country_type = vbph_country }
		has_observation_outpost = yes
		observation_outpost = { 
			has_mission = vbph_subdue
			mission_progress > 0.2
		}
		NOT = {
			owner = {
				has_relation_flag = {
					who = root.observation_outpost_owner
					flag = vbph_break_progress
				}
			}
		}
	}
	
	immediate = {
		observation_outpost_owner = { country_event = { id = vbph.531 } }
	}
}

country_event = {
	id = vbph.531
	title = vbph.531.name
	desc = vbph.531.desc
	
	picture = GFX_evt_alien_nature
	location = from
	
	is_triggered_only = yes
	
	immediate = {
		FROM = {
			observation_outpost = { add_mission_progress = -0.2 }
			owner = {
				set_relation_flag = {
					who = ROOT
					flag = vbph_break_progress
				}
			}
		}
	}
	
	option = {
		name = vbph.531.a
		
		tooltip = {
			FROM = {
				observation_outpost = { add_mission_progress = -0.2 }
			}
		}
	}
}


#Difficulty breaking
planet_event = {
	id = vbph.540
	
	hide_window = yes
	
	mean_time_to_happen = {
		months = 480
		
		modifier = {
			factor = 0.5
			has_owner = yes
			owner = { has_country_flag = vbph_ego_stage }
		}
		
		modifier = {
			factor = 0.25
			has_owner = yes
			owner = { has_country_flag = vbph_superego_stage }
		}
	}
	
	trigger = {
		has_owner = yes
		owner = { is_country_type = vbph_country }
		has_observation_outpost = yes
		observation_outpost = { 
			OR = {
				has_mission = vbph_break_apart_0
				has_mission = vbph_break_apart_1
			}
			mission_progress > 0.2
		}
		NOT = {
			owner = {
				has_relation_flag = {
					who = root.observation_outpost_owner
					flag = vbph_break_progress
				}
			}
		}
	}
	
	immediate = {
		observation_outpost_owner = { country_event = { id = vbph.541 } }
	}
}

country_event = {
	id = vbph.541
	title = vbph.541.name
	desc = vbph.541.desc
	
	picture = GFX_evt_genetic_modification
	location = from
	
	is_triggered_only = yes
	
	immediate = {
		FROM = {
			observation_outpost = { add_mission_progress = -0.2 }
			owner = {
				set_relation_flag = {
					who = ROOT
					flag = vbph_break_progress
				}
			}
			random_owned_pop = { kill_pop = yes }
		}
	}
	
	option = {
		name = vbph.541.a
		
		tooltip = {
			FROM = {
				observation_outpost = { add_mission_progress = -0.2 }
			}
		}
	}
}

###################
#CONQUEST HANDLING#
###################

#Hidden event. Determining fate of the hive
country_event = {
	id = vbph.600
	
	hide_window = yes
	
	is_triggered_only = yes
	
	trigger = {
		is_country_type = vbph_country
	}
	
	immediate = {
		FROMFROM = {
			save_event_target_as = hive_planet
		}
		FROM = {
			random_list = {
				1 = { # Destruction
					country_event = { id = vbph.601 }
				}
				1 = { #Breakdown
					country_event = { id = vbph.602 }
					modifier = {
						factor = 0
						OR = {
							has_authority = auth_machine_intelligence
							has_civic = civic_fanatic_purifiers
						}
					}
				}
				1 = { #Annexation
					country_event = { id = vbph.603 }
					modifier = {
						factor = 0
						NOR = {
							has_authority = auth_hive_mind
							has_ascension_perk = ap_evolutionary_mastery
						}
					}
				}
			}
			random_list = {
				2 = {  
					FROMFROM = {
						random_tile = {
							limit = {
								OR = {
									has_building = vbph_spawning_pool_1
									has_building = vbph_spawning_pool_2
									has_building = vbph_spawning_pool_3
								}
							}
							remove_building = yes
						}
						set_planet_flag = vbph_spawning_pool_lost
					}
					
				}
				1 = {
					modifier = {
						factor = 2
						has_ascension_perk = ap_evolutionary_mastery
					}
					modifier = {
						factor = 4
						has_authority = auth_hive_mind
					}
					modifier = {
						factor = 0
						has_authority = auth_machine_intelligence
					}
				}
			}
		}
	}
}

#Destruction
country_event = {
	id = vbph.601
	title = vbph.601.name
	desc = vbph.601.desc
	# desc = {
		# text = vbph.601.desc.poollost
		# trigger = {
			# event_target:hive_planet = { has_planet_flag = vbph_spawning_pool_lost }
		# }
	# }
	
	picture = GFX_evt_ground_combat
	location = event_target:hive_planet
	
	is_triggered_only = yes
	
	immediate = {
		event_target:hive_planet = {
			destroy_colony = { keep_buildings = yes }
			observation_outpost = { dismantle = yes }
		}
	}
	
	option = {
		name = vbph.601.a
		
		trigger = {
			NOR = {
				has_authority = auth_hive_mind
				# has_authority = auth_machine_intelligence
			}
		}
	}
	
	option = {
		name = vbph.601.a.hive
		
		trigger = {
			has_authority = auth_hive_mind
		}
	}
	
	# option = {
		# name = vbph.601.a.machine
		
		# trigger = {
			# has_authority = auth_machine_intelligence
		# }
	# }
}

#Breakdown
country_event = {
	id = vbph.602
	title = vbph.602.name
	desc = vbph.602.desc
	# desc = {
		# text = vbph.602.desc.poollost
		# trigger = {
			# event_target:hive_planet = { has_planet_flag = vbph_spawning_pool_lost }
		# }
	# }
	
	picture = GFX_evt_ground_combat
	location = event_target:hive_planet
	
	is_triggered_only = yes
	
	immediate = {
		FROM = {
			random_owned_pop = {
				create_species = {
					is_mod = yes
					name = this
					plural = this
					class = this
					portrait = this
					traits = this
					homeworld = event_target:hive_planet
					sapient = no
				}
				change_species = last_created
				modify_species = {
					species = this
					remove_trait = trait_hive_mind
				}
			}
			last_created_species = { 
				save_event_target_as = pre_saps 
			}
		}
		event_target:hive_planet = {
			destroy_colony = { keep_buildings = yes }
			planet_event = { id = vbph.610 days = 1 }
		}
	}
	
	option = {
		name = vbph.602.a
		
		trigger = {
			NOR = {
				has_authority = auth_hive_mind
				has_authority = auth_machine_intelligence
			}
		}
	}
	
	option = {
		name = vbph.602.a.hive
		
		trigger = {
			has_authority = auth_hive_mind
		}
	}
	
	option = {
		name = vbph.602.a.machine
		
		trigger = {
			has_authority = auth_machine_intelligence
		}
	}
}

country_event = {
	id = vbph.603
	title = vbph.603.name
	desc = vbph.603.desc
	# desc = {
		# text = vbph.603.desc.poollost
		# trigger = {
			# event_target:hive_planet = { has_planet_flag = vbph_spawning_pool_lost }
		# }
	# }
	
	picture = GFX_evt_ground_combat
	location = event_target:hive_planet
	
	is_triggered_only = yes
	
	immediate = {
		event_target:hive_planet = {
			set_owner = root
			observation_outpost = { dismantle = yes }
		}
	}
	
	option = {
		name = EXCELLENT
		custom_tooltip = vbph.603.a.tooltip
		
		trigger = {
			NOT = { has_authority = auth_hive_mind }
		}
	}
	
	option = {
		name = vbph.603.a.hive
		custom_tooltip = vbph.603.a.tooltip
		
		trigger = {
			has_authority = auth_hive_mind
		}
	}
}

#Filling planet with pops
planet_event = {
	id = vbph.610
	
	hide_window = yes
	
	is_triggered_only = yes
	
	immediate = {
		while = {
			count = 4
			best_tile_for_pop = {
				create_pop = { species = event_target:pre_saps }
			}
		}
	}
}

#################################
#AI HELPER and other maintenance#
#################################

#On observation complete, trigger the flip
ship_event = {
	id = vbph.901
	
	hide_window = yes
	
	is_triggered_only = yes 
	
	trigger = {
		FROM = {
			exists = owner
			owner = { is_country_type = vbph_country }
		}
		# always = no
	}
	
	immediate = {
		FROM = {  
			planet_event = { id = vbph.902 days = 1 } 
		}
	}
}

#Planet actually flips the mission after it's complete
planet_event = {
	id = vbph.902
	
	hide_window = yes
	
	is_triggered_only = yes
	
	trigger = {
		exists = owner
		owner = { is_country_type = vbph_country }
		has_observation_outpost = yes
		# observation_outpost = { has_mission = passive_observation } #Will return later, after scopes are fixed
	}
	
	immediate = {
		observation_outpost = { set_mission = vbph_observation }
	}
}