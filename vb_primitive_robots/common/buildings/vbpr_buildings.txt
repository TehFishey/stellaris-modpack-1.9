# Primitive Factory
vbpr_primitive_power_plant = {
	is_listed = no
	icon = "building_visitor_center"

	cost = {}
	
	produced_resources = {
		energy = 1
	}
	
	required_resources = {}
	
	upgrades = {
		building_power_plant_1
	}	

	ai_replace = yes
	event_building = yes	
}