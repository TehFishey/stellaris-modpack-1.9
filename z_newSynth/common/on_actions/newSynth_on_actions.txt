on_game_start_country = {
	events = {
		irobot.1
	}
}

# Triggers when the game starts
on_game_start = {
	events = {
		extsynths.1 		#Set flag for possible mod dependancies
	}
}

#cybertronic leader traits
on_leader_spawned = {
	events = {
		irobot.2
	}
}

# Trigger to change all robots built as irobots to irobots
# Disabled.  irobots and robots are now distinct

# A pop has been built
# This = pop
#on_buildable_pop_created = {
#	events = {
#		jagdtiger_synths.2
#	}
#}
