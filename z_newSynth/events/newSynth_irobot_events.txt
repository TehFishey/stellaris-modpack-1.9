namespace=irobot

#irobot init
country_event = {
	id = irobot.1
	hide_window = yes
	title = "OK"
	desc = "OK"
	picture = GFX_evt_alien_nature

	is_triggered_only = yes
	
	trigger = {
		owner_species = { 
			OR = {
				has_trait = trait_irobotic_1 
				has_trait = trait_irobot
			}
		}
		OR = {
			NOT = { has_country_flag = synthetic_empire }
			NOT = { has_technology = tech_synthetic_workers }
			NOT = { has_technology = tech_synthetic_leaders }
		}
	}

	immediate = {
		if = {
			limit = {
				OR = {
					has_trait = trait_irobotic_1 
					has_trait = trait_irobot 
				}
				OR = {
					NOT = { has_country_flag = synthetic_empire }
					NOT = { has_technology = tech_synthetic_workers }
					NOT = { has_technology = tech_synthetic_leaders }
				}
			}

			#give starting techs for irobots
			#give_technology = { tech = tech_administrative_ai 	message = no }
            #give_technology = { tech = tech_sentient_ai 	message = no }
            #give_technology = { tech = tech_combat_computers_1 	message = no }
            #give_technology = { tech = tech_combat_computers_2 	message = no }
            #give_technology = { tech = tech_combat_computers_3 	message = no }
            #give_technology = { tech = tech_self_aware_logic 	message = no }
			#give_technology = { tech = tech_powered_exoskeletons 	message = no }
			#give_technology = { tech = tech_robotic_workers 	message = no }
			#give_technology = { tech = tech_droid_workers 	message = no } 
			#give_technology = { tech = tech_synthetic_workers 	message = no } 
			give_technology = { tech = tech_synthetic_leaders 	message = no } 
			give_technology = { tech = tech_synthetic_thought_patterns 	message = no } 
			#give_technology = { tech = tech_will_to_power 	message = no } 
			
			set_country_flag = is_irobot
			
			# robots will never join rebellion
			set_country_flag = synthetic_age
			set_country_flag = synthetic_empire
			set_country_flag = robots_pacified
			set_country_flag = ai_accord
			set_policy = {
				policy = robot_pop_policy
				option = robot_pops_allowed
				cooldown = no
			}
			set_policy = {
				policy = artificial_intelligence_policy
				option = ai_full_rights
				cooldown = no
			}
			set_policy = {
				policy = slavery
				option = slavery_not_allowed
				cooldown = no
			}

			random_owned_planet = {
				create_species = {
					#is_mod = yes
					name = this
					plural = this
					class = this
					portrait = this
					traits = {
						trait = trait_mechanical
						trait = trait_irobotic_1
					}
					traits = this
					homeworld = this
					new_pop_resource_requirement = {
						type = robot_food
						value = 24.0
					}
					pops_auto_growth = 1.0
					pops_can_be_colonizers = yes
					pops_can_migrate = yes
					pops_can_reproduce = no
					pops_can_join_factions = yes
					pop_maintenance = 1
					can_generate_leaders = yes
					pops_can_be_slaves = yes
					can_be_modified = yes
					pops_have_happiness = yes
					#namelist = robot_test
					immortal = yes
				}
			}

			modify_species = {
				species = last_created_species
				remove_trait = trait_irobot
				change_scoped_species = no
			}
			
			change_dominant_species = { species = last_created_species }
			
			every_owned_pop = {
				change_species = last_created_species
				tile = {
					if = {
						limit = { has_building = building_basic_farm }
						set_building = building_basic_power_plant
					}
					if = {
						limit = { has_building = building_hydroponics_farm_1 }
						set_building = building_power_plant_1
					}
					if = {
						limit = { has_building = building_hydroponics_farm_2 }
						set_building = building_power_plant_2
					}
					if = {
						limit = { has_building = building_hydroponics_farm_3 }
						set_building = building_power_plant_3
					}
					if = {
						limit = { has_building = building_hydroponics_farm_4 }
						set_building = building_power_plant_4
					}		
					if = {
						limit = { has_building = building_hydroponics_farm_5 }
						set_building = building_power_plant_5
					}
					if = {
						limit = { has_building = building_hab_agri_bay }
						set_building = building_hab_dark_matter_plant
						clear_deposits = yes
					}
				}
			}
			every_owned_leader = {
				change_species = last_created_species
				if = {
					limit = { leader_class = admiral }
					add_trait = leader_trait_admiral_irobotic
					add_ruler_trait = leader_trait_ruler_irobotic
				}
				if = {
					limit = { leader_class = general }
					add_trait = leader_trait_general_irobotic
					add_ruler_trait = leader_trait_ruler_irobotic
				}
				if = {
					limit = { leader_class = governor }
					add_trait = leader_trait_governor_irobotic
					add_ruler_trait = leader_trait_ruler_irobotic
				}
				if = {
					limit = { leader_class = scientist }
					add_trait = leader_trait_scientist_irobotic
					add_ruler_trait = leader_trait_ruler_irobotic
				}
				if = {
					limit = { leader_class = ruler }
					add_trait = leader_trait_ruler_irobotic
				}
				if = {
					limit = {
						OR = {
							has_trait = leader_trait_resilient
							has_trait = leader_trait_eager
						}
					}
					remove_trait = leader_trait_resilient
					remove_trait = leader_trait_eager
					add_trait = leader_trait_adaptable
				}
				if = {
					limit = {
						has_trait = leader_trait_iron_fist
					}
					remove_trait = leader_trait_iron_fist
					add_trait = leader_trait_intellectual
				}
				if = {
					limit = {
						has_trait = leader_trait_agrarian_upbringing
					}
					remove_trait = leader_trait_agrarian_upbringing
					add_trait = leader_trait_architectural_interest
				}
			}
			every_pool_leader = {
				change_species = last_created_species
				if = {
					limit = { leader_class = admiral }
					add_trait = leader_trait_admiral_irobotic
					add_ruler_trait = leader_trait_ruler_irobotic
				}
				if = {
					limit = { leader_class = general }
					add_trait = leader_trait_general_irobotic
					add_ruler_trait = leader_trait_ruler_irobotic
				}
				if = {
					limit = { leader_class = governor }
					add_trait = leader_trait_governor_irobotic
					add_ruler_trait = leader_trait_ruler_irobotic
				}
				if = {
					limit = { leader_class = scientist }
					add_trait = leader_trait_scientist_irobotic
					add_ruler_trait = leader_trait_ruler_irobotic
				}
				if = {
					limit = { leader_class = ruler }
					add_trait = leader_trait_ruler_irobotic
				}
				if = {
					limit = {
						OR = {
							has_trait = leader_trait_resilient
							has_trait = leader_trait_eager
						}
					}
					remove_trait = leader_trait_resilient
					remove_trait = leader_trait_eager
					add_trait = leader_trait_adaptable
				}
				if = {
					limit = {
						has_trait = leader_trait_iron_fist
					}
					remove_trait = leader_trait_iron_fist
					add_trait = leader_trait_intellectual
				}
				if = {
					limit = {
						has_trait = leader_trait_agrarian_upbringing
					}
					remove_trait = leader_trait_agrarian_upbringing
					add_trait = leader_trait_architectural_interest
				}
			}

			every_owned_planet = {
				every_tile = {
					limit = { 
						has_resource = {
							type = food
							amount > 0
						}
					}
					
					clear_deposits = yes
					set_deposit = d_rich_energy_deposit
				}
			}
			last_created_species = {
				set_citizenship_type = {
					country = root
					type = citizenship_full
					cooldown = yes
				}
			}
		}
	}
}

country_event = {
	id = irobot.2
	is_triggered_only = yes
	hide_window = yes

	trigger = {
		from = {
			species = { has_trait = trait_irobotic_1 }
		}
	}

	immediate = {
		from = {
			if = {
				limit = { leader_class = admiral }
				add_trait = leader_trait_admiral_irobotic
				add_ruler_trait = leader_trait_ruler_irobotic
			}
			if = {
				limit = { leader_class = general }
				add_trait = leader_trait_general_irobotic
				add_ruler_trait = leader_trait_ruler_irobotic
			}
			if = {
				limit = { leader_class = governor }
				add_trait = leader_trait_governor_irobotic
				add_ruler_trait = leader_trait_ruler_irobotic
			}
			if = {
				limit = { leader_class = scientist }
				add_trait = leader_trait_scientist_irobotic
				add_ruler_trait = leader_trait_ruler_irobotic
			}
			if = {
				limit = { leader_class = ruler }
				add_trait = leader_trait_ruler_irobotic
			}
			if = {
				limit = {
					OR = {
						has_trait = leader_trait_resilient
						has_trait = leader_trait_eager
					}
				}
				remove_trait = leader_trait_resilient
				remove_trait = leader_trait_eager
				add_trait = leader_trait_adaptable
			}
			if = {
				limit = {
					has_trait = leader_trait_iron_fist
				}
				remove_trait = leader_trait_iron_fist
				add_trait = leader_trait_intellectual
			}
			if = {
				limit = {
					has_trait = leader_trait_agrarian_upbringing
				}
				remove_trait = leader_trait_agrarian_upbringing
				add_trait = leader_trait_architectural_interest
			}
		}
	}
}

#DEPRECATED
#pop_event = {
#	id = jagdtiger_synths.2
#	hide_window = yes
#	is_triggered_only = yes
#
#	trigger = {
#		planet = {
#			has_owner = yes
#			owner = { has_trait = trait_irobotic_1 }
#		}
#	}
#	
#	immediate = {
#		planet = {
#			owner = {
#				ROOT = {
#					change_species = PREV
#				}
#			}
#		}
#	}
#}

#pop_event = {
#	id = jagdtiger_synths.3
#	hide_window = yes
#	is_triggered_only = no
#
#	trigger = {
#		has_trait = trait_irobotic_1
#		planet = {
#			has_owner = yes
#			owner = { 
#				has_trait = trait_irobotic_1 
#				NOT = { is_same_species = ROOT }
#			}
#		}
#	}
#	
#	mean_time_to_happen = { months = 1 }
#
#	immediate = {
#		planet = {
#			owner = {
#				ROOT = {
#					change_species = PREV
#				}
#			}
#		}
#	}
#}

#country_event = {
#	id = jagdtiger_synths.4
#	hide_window = yes
#	is_triggered_only = no
#
#	trigger = {
#		has_technology = tech_synthetic_workers
#		has_technology = tech_synthetic_leaders
#	}
#	
#	mean_time_to_happen = { months = 1 }
#
#	immediate = {
#		random_owned_leader = {
#			limit = { 
#				OR = {
#					has_trait = leader_trait_admiral_synthetic 
#					has_trait = leader_trait_general_synthetic 
#					has_trait = leader_trait_governor_synthetic 
#					has_trait = leader_trait_ruler_synthetic 
#					has_trait = leader_trait_scientist_synthetic 
#				}
#			}
#			set_age = 1
#		}
#		random_pool_leader = {
#			limit = { 
#				OR = {
#					has_trait = leader_trait_admiral_synthetic 
#					has_trait = leader_trait_general_synthetic 
#					has_trait = leader_trait_governor_synthetic 
#					has_trait = leader_trait_ruler_synthetic 
#					has_trait = leader_trait_scientist_synthetic 
#				}
#			}
#			set_age = 1
#		}
#	}
#}

# clear the old obsolete modifier
#pop_event = {
#	id = jagdtiger_synths.7
#	hide_window = yes
#	is_triggered_only = no
#
#	trigger = {
#		has_modifier = pop_assimilation_to_synth
#	}
#	
#	mean_time_to_happen = { months = 1 }
#
#	immediate = {
#		remove_modifier = pop_assimilation_to_synth
#	}
#}

# was scheduled to be borged, and their time is up
#pop_event = {
#	id = jagdtiger_synths.10
#	hide_window = yes
#	is_triggered_only = no
#
#	trigger = {
#		NOT = { has_trait = trait_irobotic_1 }
#		has_citizenship_type = { type = citizenship_assimilation }
#		planet = {
#			has_owner = yes
#			owner = { 
#				OR = {
#					is_species_class = "ROBOT"
#					is_species_class = "JSYNTH2"
#					is_species_class = "MACHINE"
#				}
#				has_trait = trait_irobotic_1 
#				has_country_flag = synthetic_age
#				NOT = { is_same_species = ROOT }
#			}
#		}
#	}
#	
#	mean_time_to_happen = { months = 60 }
#
#	immediate = {
#		planet = {
#			owner = {
#				ROOT = {
#					change_species = PREV
#					remove_modifier = pop_assimilation_to_synth
#				}
#			}
#		}
#	}
#}

# patch in the ability to build themselves in older save games
#country_event = {
#	id = jagdtiger_synths.11
#	hide_window = yes
#	is_triggered_only = no
#	fire_only_once = yes
#
#	trigger = {
#		has_technology = tech_synthetic_workers
#		has_technology = tech_synthetic_leaders
#		has_trait = trait_irobotic_1
#		NOR = {
#			has_country_flag = synthetic_age
#			has_authority = auth_machine_intelligence
#		}
#	}
#	
#	mean_time_to_happen = { months = 1 }
#
#	immediate = {
#		set_country_flag = synthetic_age
#	}
#}
